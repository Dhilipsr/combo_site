<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta name="insight-app-sec-validation" content="91e16055-044f-4be7-8eb2-e54684fdbd93">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    @yield('seo')
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/7.0.8/swiper-bundle.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <!--
    <link rel="stylesheet" href="css/main3.css"> -->
    <link rel="stylesheet" href="{{asset('css/responsive3.css')}}">
    @yield('css')
    <script src="{{asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script> <!-- modernizr -->
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=P0cJVG2CwNXQdG2bnWUhpw&gtm_preview=env-1&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PKQTQMS');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PKQTQMS&gtm_auth=P0cJVG2CwNXQdG2bnWUhpw&gtm_preview=env-1&gtm_cookies_win=x"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
		<![endif]-->

    @include('partials.header')

    {{-- content starts here --}}
    @yield('content')
    {{-- content ends here --}}

    @include('partials.footer')


    @include('partials.leaveSiteModal')

    <script type="text/javascript" src="{{asset('js/vendor/jquery-1.11.2.min.js')}}"></script> <!-- jQuery -->
    <script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script> <!-- bootstrap -->
    <script type="text/javascript" src="{{asset('js/TweenMax.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/7.0.8/swiper-bundle.min.js"></script>
    <!-- <script src="js/plugins.js"></script> -->
    <script type="text/javascript" src="{{asset('js/jquery.validate.min.js')}}"></script> <!-- main -->
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script> <!-- main -->
    <script>
        $(document).ready(function() {

            fetch_customer_data();

            function fetch_customer_data(query = '',page = '') {
                console.log(page);
                
                @if(Route::currentRouteName() == 'customizeHealthTools')
                    checkPage = ''
                @else
                    checkPage = '{{last(request()->segments())}}';
                @endif
                if(
                    checkPage != 'login' && checkPage != 'register' && checkPage != 'my-account' && checkPage != "guidelines" && checkPage != "trend-report" && page == ''  ){
                    page = '{{last(request()->segments())}}';
                    pageTwo = '{{last(request()->segments(0))}}';
                    console.log(pageTwo);
                    if(page == 'healthtools'){
                        @if(isset($_GET['section']))
                            page = "{{$_GET['section']}}";
                        @endif
                        switch(page) {
                        case "1":
                            page = 1;
                            break;
                        case "2":
                            // code block
                            page = 2;
                            break;
                        case "3":
                            // code block
                            page = 3;
                            break;
                        case "4":
                            // code block
                            page = 4;
                            break;
                        default:
                            // code block
                        }
                    }else{
                        switch(page) {
                        case "AC":
                            page = 1;
                            break;
                        case "PC":
                            // code block
                            page = 2;
                            break;
                        case "HS":
                            // code block
                            page = 3;
                            break;
                        case "LTC":
                            // code block
                            page = 4;
                            break;
                        case "screen-patients":
                            // code block
                            page = 1;
                            break;
                        case "define-an-episode":
                            // code block
                            page = 1;
                            break;
                        case "coordinate-care":
                            // code block
                            page = 1;
                            break;
                        case "raise-the-bar":
                            // code block
                            page = 2;
                            break;
                        case "hs-care-pathways":
                            // code block
                            page = 3;
                            break;
                        case "hs-ehr-plugin":
                            // code block
                            page = 3;
                            break;
                        case "ltc-care-pathways":
                            // code block
                            page = 4;
                            break;
                        case "ltc-ehr-plugin":
                            // code block
                            page = 4;
                            break;
                        default:
                            page = '';
                            break;
                            // code block
                        }
                    }

                }
                $.ajax({
                    url: "{{ route('live_search.action') }}",
                    method: 'GET',
                    data: {
                        query: query,
                        page: page,
                    },
                    dataType: 'json',
                    success: function(data) {
                        console.log(data)
                        $('#healthtools_ajax_results').html(data.table_data);
                        // $('#ebw_ajax_results').html(data.table_data_two);
                        // $('#guidelines_ajax_results').html(data.table_data_three);
                        $('#total_records').text(data.total_data);
                    }
                })
            }

            $(document).on('keyup', '#search', function() {
                var query = $(this).val();
                var page = $(this).attr('data-page');
                // console.log(query);
                // console.log(page);
                fetch_customer_data(query,page);
            });
        });
        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
          });
        // $(document).ready(function() {
        //     $('#org_type').on('change', function() {
        //         var $form = $(this).closest('form');
        //         $('#org_type').val($(this).val());
        //         $form.find('input[type=submit]').click();
        //     });
        //     $('#lang_sort').change(function(e){
        //         var $form = $(this).closest('form');
        //         $('#lang_sort').val($(this).val());
        //         $form.find('input[type=submit]').click();
        //     });
        // });
    </script>


    @yield('js')
    
    
</body>

</html>