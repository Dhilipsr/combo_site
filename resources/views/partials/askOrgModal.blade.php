<!-- Header Ends -->
<div id="selectOrganizationModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row guidePop full-sz ">
                <div class="col-md-12">
                    <h3 class="">Please Select an Organization Type to View the Health Tools </h3>
                    <ul class="product-link">
                        <li><a href="{{route('healthtools',['section' => 1])}}" class="button line-white"> <strong>LHN</strong> Ambulatory Care</a>
                            <a href="{{route('healthtools',['section' => 2])}}" class="button line-white"> <strong>LHN</strong> Primary Care</a>
                            <a href="{{route('healthtools',['section' => 3])}}" class="button line-white"> <strong>ACE</strong> Health Systems</a>
                            <a href="{{route('healthtools',['section' => 4])}}" class="button line-white"> <strong>ACE</strong> Long-Term Care</a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>