<div class="htFilter">
    <div class="container">
        <form method="GET" action="{{route('healthtoolfilter')}}">
            <div class="htFilterRow">

                <div class="htFiltHead  ">
                    <select
                        class="select2 form-field orgTp "
                        placeholder="Organization Type"
                        name="section" id="org_type"
                        onchange="this.form.submit()"
                     >
                        <option></option>
                        <option value="1"
                        @if($section == 1)
                            selected="selected"
                        @endif
                        >LHN Ambulatory Care</option>
                        <option value="2"
                        @if($section == 2)
                            selected="selected"
                        @endif
                        >LHN Primary Care</option>
                        <option value="3"
                        @if($section == 3)
                            selected="selected"
                        @endif
                        >ACE Health Systems</option>
                        <option value="4"
                        @if($section == 4)
                            selected="selected"
                        @endif
                        >ACE Long-Term Care</option>
                    </select>
                </div>
                <div class="htFiltHead htFiltHead1">
                    <span class="htFiltHdSpan" data-id="1">Target Audience <img src="{{asset('img/arrow.png')}}"
                            alt="img" /></span>
                </div>
                <div class="htFiltHead htFiltHead2">
                    <span class="htFiltHdSpan" data-id="2">Patient Type <img src="{{asset('img/arrow.png')}}" alt="img" /></span>
                </div>
                <!-- Added 24 April -->
                <div class="htFiltHead  ">
                    <select class="select2 select2Title form-field languageSel" name="lang"
                    id="lang_sort"
                    placeholder="Language"
                    onchange="this.form.submit()"
                    >
                        <option ></option>
                        <option value="1"
                        <?php
                            if( isset($_GET['lang']) && $_GET['lang'] == 1){
                                echo 'selected="selected"';
                            }
                        ?>
                        >English </option>
                        <option value="2"
                        <?php
                            if( isset($_GET['lang']) && $_GET['lang'] == 2){
                                echo 'selected="selected"';
                            }
                        ?>
                        > Spanish</option>
                    </select>
                </div>
                <!-- // Added 24 April -->

                <div class="search-tool sb-search" id="sb-search">

                    {{-- <form> --}}
                        <input class="sb-search-input" placeholder="Enter your search term..." type="text" value=""
                            name="search" id="search">
                        <button class="sb-search-submit"  value=""></button>
                        <span class="sb-icon-search">
                            <!-- <img src="img/search.png" alt=""> --></span>
                    {{-- </form> --}}

                    <!--  <a href="" class=""><img src="img/search.png" alt=""></a> -->
                </div>


            </div>
            <div class="filtIntBox">
                <div class="filtInt">
                    <ul class="htFiltBody htFiltBody1">
                        @foreach( $targetaudiences as $targetaudience )
                        <li>
                            <div class="check-box">
                                <input type="checkbox" id="targetaudience_{{$targetaudience->id}}" name="targetaudience[]" value="{{$targetaudience->id}}"
                                <?php
                                    if(isset($_GET['filter'])){
                                        if ($_GET['filter'] == "clear") {

                                        }else{
                                            echo "checked='checked'";
                                        }
                                    }else{
                                        if(isset($targetaudiences_array)){
                                            foreach( $targetaudiences_array as $valueToCheck){
                                                if($valueToCheck == $targetaudience->id){
                                                    echo "checked='checked'";
                                                }
                                            }
                                        }
                                    }
                                ?>
                                >
                                <label for="targetaudience_{{$targetaudience->id}}">{{$targetaudience->title}}</label>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    <ul class="htFiltBody htFiltBody2">
                        @foreach( $patienttypes as $patienttype )
                        <li>
                            <div class="check-box">
                                <input type="checkbox" id="patienttype_{{$patienttype->id}}" name="patienttype[]" value="{{$patienttype->id}}"
                                <?php
                                        if(isset($_GET['filter'])){
                                            if ($_GET['filter'] == "clear") {

                                            }else{
                                                echo "checked='checked'";
                                            }
                                        }else{
                                             if(isset($patienttypes_array)){
                                                foreach( $patienttypes_array as $valueToCheck){
                                                    if($valueToCheck == $patienttype->id){
                                                        echo "checked='checked'";
                                                    }
                                                }
                                            }
                                        }
                                ?>
                                >
                                <label for="patienttype_{{$patienttype->id}}">
                                    {{$patienttype->type}}
                                </label>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="filtBtns">
                    <button class="filtBtn" id="filter">Filter</button>
                    @if(isset($_GET['section']))
                        <a href="{{route('healthtools')."?section=".$_GET['section']."&&filter=clear"}}">Clear Filter</a>
                    @else
                        <a href="{{route('healthtools')."?filter=clear"}}">Clear Filter</a>
                    @endif
                </div>
            </div>
        </form>
        <p class="filtResult">
            @if(isset($search) && $search)
                 {{$healthtoolsCount}} results found for "{{$search}}"
            @else
                Your Results {{$healthtoolsCount}}
            @endif
        </p>
    </div>
</div>