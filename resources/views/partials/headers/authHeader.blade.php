@if(Route::currentRouteName() == 'login' || Route::currentRouteName() == 'register')
<!-- Header Starts -->
<header>
    <div class="small-header">
        <div class="top-container">
            <ul class="topmenu">
                <li><a
                    @if(Route::currentRouteName() == 'ambulatoryCare')
                        class="active"
                    @endif
                    href="https://liverhealthnow.com/AC"><strong> LHN</strong> Ambulatory Care</a> </li>
                <li><a
                    @if(Route::currentRouteName() == 'primaryCare')
                        class="active"
                    @endif
                    href="https://liverhealthnow.com/PC"><strong>LHN</strong> Primary Care </a> </li>
                <li><a
                    @if(Route::currentRouteName() == 'healthSystem')
                        class="active"
                    @endif
                    href="https://accessclickengage.liverhealthnow.com/HS"><strong>ACE</strong> Health Systems </a></li>
                <li><a
                    @if(Route::currentRouteName() == 'longTermCare')
                        class="active"
                    @endif
                    href="https://accessclickengage.liverhealthnow.com/LTC"><strong>ACE</strong> Long-Term Care</a></li>
                <li>This site is intended for US health care professsionals only.</li>

            </ul>
            <ul>
                @if(Auth::check())
                    <li><a href="https://liverhealthnow.com/my-account">My Account</a></li>
                    <li><a href="{{route('logout')}}">Log Out</a></li>
                @else
                    <li><a href="https://liverhealthnow.com/login">Log In</a></li>
                    <li><a href="https://liverhealthnow.com/register">Register</a></li>
                @endif
                <li><a class="srchBtn"><img src="{{asset('img/search.png')}}" alt="img" /></a></li>
            </ul>
        </div>
    </div>
    <div class="main-header">
        <div class="container">
            <div class="main-header__in">
                <div class="logo">
                    <!-- <a href="index.html"><img src="img/logo.png" alt="img" /></a> -->
                </div>
                <ul class="navigation">
                    <li><a href="#" data-toggle="modal" data-target="#selectOrganizationModal">Health Tools</a></li>
                    <li><a href="https://liverhealthnow.com/guidelines"> Guidelines and Links </a></li>
                    <li><a href="https://liverhealthnow.com/trends-report"> Trends Report </a></li>

                    @if(Auth::check())
                        <li><a href="https://liverhealthnow.com/my-account">My Account</a></li>
                        <li class="fxHdr"><a href="{{route('logout')}}">Log Out</a></li>
                    @else
                        <li class="fxHdr fxHdr1"><a href="https://liverhealthnow.com/login">Log In</a></li>
                        <li class="fxHdr"><a href="https://liverhealthnow.com/register">Register</a></li>
                    @endif

                    <li class="fxHdr"><a class="srchBtn"><img src="{{asset('img/search-clr.png')}}" alt="img" /></a></li>
                </ul>
                <div class="mob-menu-box">
                    <div class="mob-srchBtn"><a class="srchBtn"><img src="{{asset('img/mobSearchIcon.png')}}" alt="img" /></a>
                    </div>
                    <div class="menu-icon"><span></span></div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="mob-menu-up">
    <div class="mob-menu">
        <div class="mob-close-box"><span class="mob-close"><img src="{{asset('img/close-white.png')}}" alt="img" /></span>
        </div>
        <ul class="navigation">

            @if(Auth::check())
                <li><a href="https://liverhealthnow.com/my-account">My Account</a></li>
                <li><a href="{{route('logout')}}">Log Out</a></li>
            @else
                <li><a href="https://liverhealthnow.com/login">Log In</a></li>
                <li><a href="https://liverhealthnow.com/register">Register</a></li>
            @endif

            <li><a href="{{route('healthtools',['section' => '2'])}}">Health Tools</a></li>
            <li><a
                @if(Route::currentRouteName() == 'longTermGuidelines')
                    class="active"
                @endif
                href="https://liverhealthnow.com/guidelines"> Guidelines and Links </a></li>
            <li><a
                @if(Route::currentRouteName() == 'longTermTrendReport')
                    class="active"
                @endif
                href="https://liverhealthnow.com/trends-report"> Trends Report </a></li>
                <li><p>This site is intended for US health care
                professionals only.</p></li>

        </ul>
    </div>
</div>
<div class="searchContainer">
    <div class="srchBox">
        <div class="container">
            <div class="srch">
                <input type="text" placeholder="Search" id="search" data-page="">
                <span class="srchClose"></span>
            </div>
             <div class="srch__container">
                <div class="srch__cont">
                    <div class="srch__container__in">
                        <h2>Health Tools</h2>
                        <ul id="healthtools_ajax_results">
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Header Ends -->
<div id="selectOrganizationModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row guidePop full-sz ">
                <div class="col-md-12">
                    <h3 class="">Please Select an Organization Type to View the Health Tools </h3>
                    <ul class="product-link">
                        <li>
                            <!--<a href="{{route('healthtools',['section' => 1])}}" class="button line-white"> <strong>LHN</strong> Ambulatory Care</a>-->
                            <!--<a href="{{route('healthtools',['section' => 2])}}" class="button line-white"> <strong>LHN</strong> Primary Care</a>-->
                            <!--<a href="{{route('healthtools',['section' => 3])}}" class="button line-white"> <strong>ACE</strong> Health Systems</a>-->
                            <!--<a href="{{route('healthtools',['section' => 4])}}" class="button line-white"> <strong>ACE</strong> Long-Term Care</a>-->
                            <a href=" https://liverhealthnow.com/AC" class="button line-white"> <strong>LHN</strong> Ambulatory Care</a>
                            <a href=" https://liverhealthnow.com/PC" class="button line-white"> <strong>LHN</strong> Primary Care</a>
                            <a href=" https://accessclickengage.liverhealthnow.com/HS" class="button line-white"> <strong>ACE</strong> Health Systems</a>
                            <a href=" https://accessclickengage.liverhealthnow.com/LTC/" class="button line-white"> <strong>ACE</strong> Long-Term Care</a>
                           
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif