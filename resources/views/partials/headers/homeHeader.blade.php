@if(Route::currentRouteName() == 'home' || Route::currentRouteName() == 'landing'|| Route::currentRouteName() == 'healthtools' ||
Route::currentRouteName() == 'healthtoolfilter' || Route::currentRouteName() == 'customizeHealthTools' ||
Route::currentRouteName() == 'myaccount' || Route::currentRouteName() == 'password.reset' || Route::currentRouteName() == 'thankyou' ||
Route::currentRouteName() == 'share-fav-tool' || Route::currentRouteName() == 'trendReportDetail' || Route::currentRouteName() == 'longTermGuidelines'
)
<!-- Header Starts -->
<header>
    <div class="small-header">
        <div class="top-container">
            <ul class="topmenu">
                <li><a
                    @if(Route::currentRouteName() == 'ambulatoryCare')
                        class="active"
                    @endif
                    href="https://liverhealthnow.com/AC"><strong> LHN</strong> Ambulatory Care</a>
                </li>
                <li><a
                    @if(Route::currentRouteName() == 'primaryCare')
                        class="active"
                    @endif
                    href="https://liverhealthnow.com/PC"><strong>LHN</strong> Primary Care </a>
                </li>
                <li><a
                    @if(Route::currentRouteName() == 'healthSystem')
                        class="active"
                    @endif
                    href="https://accessclickengage.liverhealthnow.com/HS"><strong>ACE</strong> Health Systems </a></li>
                <li><a
                    @if(Route::currentRouteName() == 'longTermCare')
                        class="active"
                    @endif
                    href="https://accessclickengage.liverhealthnow.com/LTC"><strong>ACE</strong> Long-Term Care</a></li>
                <li>This site is intended for US health care professsionals only.</li>
            </ul>
            <ul>

                @if(Auth::check())
                    <li><a href="https://liverhealthnow.com/my-account">My Account</a></li>
                    <li><a href="{{route('logout')}}">Log Out</a></li>
                @else
                    <li><a href="https://liverhealthnow.com/login">Log In</a></li>
                    <li><a href="https://liverhealthnow.com/register">Register</a></li>
                @endif
                <li><a class="srchBtn"><img src="{{asset('img/search.png')}}" alt="img" /></a></li>
            </ul>
        </div>
    </div>
    <div class="main-header
        @if(Route::currentRouteName() == 'landing')
        index-menu-bar
        @endif
        "
    >
        <div class="container">
            <div class="main-header__in">
                <div class="logo">
                    <!-- <a href="index.html"><img src="img/logo.png" alt="img" /></a> -->
                </div>
                <ul class="navigation">
                        @if( isset($_GET['section']) )
                            <li><a href="{{route('healthtools',['section'=> $_GET['section']])}}">Health Tools</a></li>
                        @else
                            <li><a href="#" data-toggle="modal" data-target="#selectOrganizationModal">Health Tools</a></li>
                        @endif
                        <li><a
                            @if(Route::currentRouteName() == 'longTermGuidelines')
                                class="active"
                            @endif
                            href="{{route('longTermGuidelines')}}"> Guidelines and Links </a></li>
                        <li><a
                            @if(Route::currentRouteName() == 'longTermTrendReport')
                                class="active"
                            @endif
                            href="{{route('longTermTrendReport')}}"> Trends Report </a></li>

                        <li class="fxHdr"><a href="{{route('logout')}}">Log Out</a></li>

                        <li class="fxHdr"><a class="srchBtn"><img src="{{asset('img/search-clr.png')}}" alt="img" /></a></li>
                    </ul>
                <div class="mob-menu-box">
                    <div class="mob-srchBtn"><a class="srchBtn"><img src="{{asset('img/mobSearchIcon.png')}}" alt="img" /></a>
                    </div>
                    <div class="menu-icon"><span></span></div>

                </div>

            </div>
        </div>
    </div>
</header>
<div class="mob-menu-up">
    <div class="mob-menu">
        <div class="mob-close-box"><span class="mob-close"><img src="{{asset('img/close-white.png')}}" alt="img" /></span>
        </div>
        <ul class="navigation">
            @if(Auth::check())
                <li><a href="https://liverhealthnow.com/my-account">My Account</a></li>
                <li><a href="{{route('logout')}}">Log Out</a></li>
            @else
                <li><a href="https://liverhealthnow.com/login">Log In</a></li>
                <li><a href="https://liverhealthnow.com/register">Register</a></li>
            @endif
             <li>
                <a
                    @if(Route::currentRouteName() == 'longTermTrendReport' || Route::currentRouteName() == 'longTermGuidelines')
                        href="#" data-toggle="modal" data-target="#selectOrganizationModal"
                    @elseif(Route::currentRouteName() == 'healthtools' && !isset($_GET['section']))
                        href="#" data-toggle="modal" data-target="#selectOrganizationModal"
                    @elseif(Route::currentRouteName() == 'healthtools')
                        href="{{route('healthtools',['section' => $_GET['section']])}}"
                    @else
                        href="#"
                    @endif
                >
                Health Tools
                </a>
            </li>

            <li><a
                @if(Route::currentRouteName() == 'longTermGuidelines')
                class="active"
                @endif
                href="{{route('longTermGuidelines')}}"> Guidelines and Links</a></li>
            <li>
                <a href="{{route('longTermTrendReport')}}"
                @if(Route::currentRouteName() == 'longTermTrendReport')
                class="active"
                @endif
                > Trends Report</a>
            </li>
            <li><a href="http://go.aventriahealth.com/ACE-LHNComboContactUs_ContactForm.html">Contact Us</a></li>
            <li><p>This site is intended for US health care
                professionals only.</p></li>

        </ul>
    </div>
</div>
<div class="searchContainer">
    <div class="srchBox">
        <div class="container">
            <div class="srch">
                <input type="text" placeholder="Search" id="search" data-page="">
                <span class="srchClose"></span>
            </div>
            <div class="srch__container">
                <div class="srch__cont">
                    <div class="srch__container__in">
                        <h2>Health Tools</h2>
                        <ul id="healthtools_ajax_results">
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- Header Ends -->

{{-- top header starts --}}
    @if(Route::currentRouteName() == 'landing' )
    <section class="top-header">
        <div class="container">
            <h1 class="mainhead">Manage Patients With Chronic Liver Disease,
                Cirrhosis, and Overt Hepatic Encephalopathy</h1>
        </div>
    </section>
    @endif
{{-- top header ends --}}
@include('partials.askOrgModal')
@endif