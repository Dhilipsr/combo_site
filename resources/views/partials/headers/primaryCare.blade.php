@if(Route::currentRouteName() == 'primaryCare' || Route::currentRouteName() == 'raiseBar' ||
Route::currentRouteName() == 'wrestlingTheMonster'
)
<header>
    <div class="small-header">
        <div class="top-container">
            <ul class="topmenu">
                <li><a href="https://liverhealthnow.com/AC"><strong> LHN</strong> Ambulatory Care</a>
                </li>
                <li class="active"><a href="https://liverhealthnow.com/PC"><strong>LHN</strong> Primary Care </a>
                </li>
                <li><a href="https://accessclickengage.liverhealthnow.com/HS"><strong>ACE</strong> Health Systems </a></li>
                <li ><a href="https://accessclickengage.liverhealthnow.com/LTC"><strong>ACE</strong> Long-Term Care</a></li>
                <li>This site is intended for US health care professsionals only.</li>

            </ul>
            <ul>
                @if(Auth::check())
                    <li><a href="https://liverhealthnow.com/my-account">My Account</a></li>
                    <li><a href="{{route('logout')}}">Log Out</a></li>
                @else
                    <li><a href="https://liverhealthnow.com/login">Log In</a></li>
                    <li><a href="https://liverhealthnow.com/register">Register</a></li>

                @endif
                <li><a class="srchBtn"><img src="{{asset('img/search.png')}}" alt="img" /></a></li>
            </ul>
        </div>
    </div>
    <div class="main-header">
        <div class="container">
            <div class="main-header__in">
                <div class="logolh"><a href="{{route('primaryCare')}}"><img src="{{ asset('img/lh-pc-logo.png') }}" alt="img" /></a></div>
                <ul class="navigation">
                    <li><a href="https://liverhealthnow.com/healthtools?section=2">Health Tools</a></li>
                    <li><a href="https://liverhealthnow.com/PC/raise-the-bar"
                        @if(Route::currentRouteName() == 'raiseBar')
                        class="active"
                        @endif
                        >Raise the Bar</a></li>
                    <li><a
                        @if(Route::currentRouteName() == 'longTermGuidelines')
                            class="active"
                        @endif
                        href="https://liverhealthnow.com/guidelines"> Guidelines and Links </a></li>
                    <li><a
                        @if(Route::currentRouteName() == 'longTermTrendReport')
                            class="active"
                        @endif
                        href="https://liverhealthnow.com/trends-report"> Trends Report </a></li>

                    @if(Auth::check())
                        {{-- <li><a href="https://liverhealthnow.com/my-account">Account</a></li> --}}
                        <li class="fxHdr"><a href="{{route('logout')}}">Log Out</a></li>
                    @else
                        <li class="fxHdr fxHdr1"><a href="https://liverhealthnow.com/login">Log In</a></li>
                        <li class="fxHdr"><a href="https://liverhealthnow.com/register">Register</a></li>

                    @endif


                    <li class="fxHdr"><a class="srchBtn"><img src="{{asset('img/search-clr.png')}}" alt="img" /></a></li>
                </ul>
                <div class="mob-menu-box">
                    <div class="mob-srchBtn"><a class="srchBtn"><img src="{{asset('img/mobSearchIcon.png')}}" alt="img" /></a>
                    </div>
                    <div class="menu-icon"><span></span></div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="mob-menu-up">
    <div class="mob-menu">
        <div class="mob-close-box"><span class="mob-close"><img src="{{asset('img/close-white.png')}}" alt="img" /></span>
        </div>
        <ul class="navigation">
            @if(Auth::check())
                <li><a href="https://liverhealthnow.com/my-account">My Account</a></li>
                <li><a href="{{route('logout')}}">Log Out</a></li>
            @else
                <li><a href="https://liverhealthnow.com/login">Log In</a></li>
                <li><a href="https://liverhealthnow.com/register">Register</a></li>

            @endif
            <li><a href="{{route('healthtools',['section' => '2'])}}">Health Tools</a></li>
            <li><a
                @if(Route::currentRouteName() == 'raiseBar')
                class="active"
                @endif
                href="https://liverhealthnow.com/PC/raise-the-bar">Raise the Bar</a></li>
            <li><a
                @if(Route::currentRouteName() == 'longTermGuidelines')
                class="active"
                @endif
                href="https://liverhealthnow.com/guidelines"> Guidelines and Links </a></li>
            <li><a
                @if(Route::currentRouteName() == 'longTermTrendReport')
                class="active"
                @endif
                href="https://liverhealthnow.com/trends-report"> Trends Report </a></li>
            <li><a href="http://go.aventriahealth.com/ACE-LHNComboContactUs_ContactForm.html">Contact Us</a></li>
            <li><p>This site is intended for US health care
                professionals only.</p></li>
        </ul>
    </div>
</div>
<div class="searchContainer">
    <div class="srchBox">
        <div class="container">
            <div class="srch">
                <input type="text" placeholder="Search" id="search" data-page="2">
                <span class="srchClose"></span>
            </div>
             <div class="srch__container">
                <div class="srch__cont">
                    <div class="srch__container__in">
                        <h2>Health Tools</h2>
                        <ul id="healthtools_ajax_results">
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
@endif