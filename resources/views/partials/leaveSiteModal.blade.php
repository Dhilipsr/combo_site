<div id="guideModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-box">
        <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
        <div class="row guidePop full-sz">
            <div class="col-md-12">
                <h3 >The website you are about to visit is not affiliated with Salix Pharmaceuticals or its affiliated entities and is not responsible for the content, format, maintenance, or policies of the website you are about to visit. Salix Pharmaceuticals or its affiliated entities do not monitor non-affiliated websites for accuracy. This link does not imply endorsement or support of any program, products, or services associated with the website.</h3>
                <ul>
                    <li><a href="#" data-dismiss="modal"><img src="{{asset('img/arrow-white-left.png')}}" alt="img" /> &nbsp;&nbsp; Cancel</a>
                    </li>
                    <li><a  target="_blank" id="leaveSiteAnchor">Continue &nbsp;&nbsp; <img src="{{asset('img/arrow-white-right.png')}}" alt="img" /></a>
                    </li>
                </ul>
            </div>
            <!-- <div class="col-md-12 text-center">
                <div class="check-box">
                    <input type="checkbox" id="chk1">
                    <label for="chk1">Don’t show me this message again.</label>
                </div>
            </div> -->
        </div>
    </div>
</div>