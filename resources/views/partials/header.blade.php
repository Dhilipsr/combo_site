@include('partials.headers.homeHeader')

{{-- Header for Auth --}}
@include('partials.headers.authHeader')
{{-- Header for Auth --}}

{{-- Header for landing one ambulatorycare --}}
@include('partials.headers.ambulatoryCare')
{{-- Header for landing one ambulatorycare --}}


{{-- Header for landing two primary care --}}
@include('partials.headers.primaryCare')
{{-- Header for landing two primary care --}}

{{-- Header for landing three health system--}}
@include('partials.headers.healthSystem')
{{-- Header for landing three health system--}}

{{-- Header for Long Term Care --}}
@include('partials.headers.LongTermCare')
{{-- Header for Long Term Care --}}