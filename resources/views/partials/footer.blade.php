<!-- Footer Starts -->
    <div class="footer-note">
        <p>Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals
            for
            informational purposes only.
            Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>
    </div>

    <footer>
        <div class="container">
            <div class="salix-logo"><img src="{{asset('img/salix-logo.png')}}" alt="img"></div>
            <p>Salix Pharmaceuticals: 400 Somerset Corporate Blvd., Bridgewater, NJ 08807</p>
            <div class="copy">
                <p>© 2021 Salix Pharmaceuticals or its affiliates.</p>
                <p>HED.0116.USA.21</p>
            </div>


        </div>
        <!--  <div class="footer-slip"></div> -->


        <div class="policys">
            <ul class="subfoo-link">
                <li><a href="https://www.research.net/r/LiverHealthNow_AccessClickEngage" target="_blank">GINA™</a></li>
                <li><a href="https://www.bauschhealth.com/privacy" target="_blank">Privacy Policy</a></li>
                <li><a href="http://go.aventriahealth.com/ACE-LHNComboContactUs_ContactForm.html" target="_blank">Contact Us</a></li>
            </ul>
            <ul class="foo-link-2">

                <li>California residents: <a
                        href="http://go.aventriahealth.com/SalixLHNCAoptout_SalixACECAOptoutLandingPage.html"
                        target="_blank">
                        Do not sell my personal information</a></li>
            </ul>
        </div>
    </footer>
    <!-- Footer Ends -->