<div id="regModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="img/close-white.png" alt="img" /></div>
            <div class="row">
                <div class="col-md-12">
                    <h2>LICENSE AGREEMENT</h2>
                    <p>This Agreement ("Agreement") is made between Salix U.S. LLC ("Owner") and all registered users of
                        LiverHealthNow™/Access.Click.Engage.™ (LHN/ACE) and is effective as of May 1, 2019.</p>
                    <p>WHEREAS, Owner owns or has acquired rights to the LHN/ACE E-tools (defined below) and desires to
                        grant Organization the License (defined below); </p>
                    <p>WHEREAS, Organization desires to accept from Owner the License. </p>
                    <p>NOW THEREFORE, in consideration of the mutual covenants and promises herein contained, Owner and
                        Organization agree as follows:</p>
                    <p>DELIVERY OF LHN/ACE E-TOOLS; ACCEPTANCE OF AGREEMENT</p>
                    <p>Upon complete execution of this Agreement, Owner shall use commercially reasonable efforts to
                        deliver electronically to Organization the LHN/ACE tools that the parties mutually agree upon
                        from time to time ("LHN/ACE E-tools"). Organization's License to any LHN/ACE E-tools is subject
                        to the following terms and conditions and all applicable laws. By accepting this License,
                        Organization accepts and agrees to, without limitation or qualification, these terms and
                        conditions and acknowledges that any other agreements between Organization and Owner are
                        superseded and of no force or effect with respect to the LHN/ACE E-tools. Owner acknowledges
                        that its offer, and Organization acknowledges that its acceptance, of the License is not
                        conditioned upon (i) Organization's use, formulary status or purchasing commitment of any of
                        Owner's products or (ii) Owner's recommendation or promotion of Organization's products or
                        services.</p>
                    <p>SCOPE OF USE</p>
                    <p>Owner grants Organization a non-exclusive license of the LHN/ACE E-tools solely for
                        Organization's informational and educational communications within Organization's network
                        and its patient population ("License"). All rights, title and interest in the LHN/ACE
                        E-tools, including without limitation, any copyright, shall remain with Owner. Organization
                        may distribute, modify, transmit, reuse, repost, or use the LHN/ACE E-tools including the
                        text and images, solely within the United States; provided, however, that Organization shall
                        not distribute, modify, transmit, reuse, repost, use, rent, lease, sell or sublicense the
                        LHN/ACE E-tools, or any part thereof, to any unaffiliated entity (unless such entity has
                        been duly authorized by Organization to deliver the LHN/ACE E-tools within Organization's
                        network and/or to Organization's patient population and has agreed to deliver the LHN/ACE
                        E-tools subject to the terms and conditions of this Agreement), regardless of the existence
                        or absence of any fee or other remuneration, or for any unlawful use. Organization
                        understands that Owner makes no representation that the LHN/ACE E-tools are appropriate or
                        available for use in locations outside of the United States, and access to the LHN/ACE
                        E-tools from territories where the content of the LHN/ACE E tools may be illegal or
                        inappropriate is prohibited.</p>
                    <p>NO MEDICAL ADVICE</p>
                    <p>Organization acknowledges that the LHN/ACE E-tools do not provide medical advice.
                        Organization further acknowledges that Owner is not engaged in rendering medical or
                        similar professional services or advice, and the information provided in the LHN/ACE
                        E-tools is not intended to replace medical advice offered by a health care provider. If
                        Organization desires or needs such services or advice, Organization agrees to promptly
                        consult a physician or professional healthcare provider.</p>
                    <p>COPYRIGHT PROTECTION</p>
                    <p>Organization should assume that everything Organization sees or reads in the
                        LHN/ACE E-tools is copyrighted, unless otherwise noted, and may not be used
                        except as provided in this Agreement or in the text of the LHN/ACE E-tools
                        without the written permission of Owner.</p>
                    <p>TRADEMARKS</p>
                    <p>The trademarks, tradenames, logos, and service marks (collectively,
                        "Trademarks") displayed in the LHN/ACE E-tools are registered and
                        unregistered Trademarks of Owner and others. Nothing contained in the
                        LHN/ACE E-tools should be construed as granting, by implication,
                        estoppel, or otherwise, any license or right to use any Trademark
                        displayed in the LHN/ACE E-tools without the written permission of Owner
                        or such third party that may own the Trademarks displayed in the LHN/ACE
                        E-tools. Organization's use of the Trademarks displayed in the LHN/ACE
                        E-tools, or any other content in the LHN/ACE E-tools, except as provided
                        herein, is strictly prohibited. Organization also acknowledges that
                        Owner may aggressively enforce its intellectual property rights to the
                        fullest extent of the law, including, but not limited to, the seeking of
                        criminal prosecution.</p>
                    <p>EXCLUSION OF WARRANTY</p>
                    <p>While Owner uses reasonable efforts to include accurate and up-to-date information in the LHN/ACE
                        E-tools, Owner makes no warranties or representations as to its accuracy. Owner assumes no
                        liability
                        or responsibility for any errors or omissions in the content of, or claims related to or arising
                        from the LHN/ACE E-tools and/or content thereof. OWNER DOES NOT REPRESENT OR WARRANT THAT ANY
                        ERRORS
                        IN THE LHN/ACE E-TOOLS WILL BE CORRECTED.</p>
                    <p>Without limiting the foregoing, the LHN/ACE E-tools are provided to Organization "AS IS" WITHOUT
                        WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, WHETHER BY STATUTE, COMMON LAW, CUSTOM, USAGE
                        OR
                        OTHERWISE, INCLUDING, BUT NOT LIMITED TO, ANY WARRANTY OF DESIGN, MERCHANTABILITY, FITNESS FOR A
                        PARTICULAR PURPOSE OR NONINFRINGEMENT. Owner acknowledges that some jurisdictions may not allow
                        the
                        exclusion of implied warranties, so some of the above exclusions may not apply to Organization.
                        Organization shall be responsible for consulting local laws for any restrictions or limitations
                        regarding the exclusion of implied warranties.</p>
                    <p>LIMITATION OF LIABILITY</p>
                    <p>ORGANIZATION'S USE OF THE LHN/ACE E-TOOLS, INCLUDING, BUT NOT LIMITED TO, ANY REVISIONS THERETO,
                        IS
                        COMPLETELY AT ORGANIZATION'S RISK. NEITHER OWNER NOR ANY OTHER PARTY INVOLVED IN CREATING,
                        PRODUCING, OR DELIVERING THE LHN/ACE E-TOOLS SHALL BE LIABLE FOR ANY DIRECT, INCIDENTAL,
                        CONSEQUENTIAL, INDIRECT, OR PUNITIVE DAMAGES ARISING OUT OF ORGANIZATION'S ACCESS TO, OR USE OF,
                        THE
                        LHN/ACE E-TOOLS, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR IF SUCH DAMAGES ARE
                        FORESEEABLE. OWNER ASSUMES NO RESPONSIBILITY, AND SHALL NOT BE LIABLE FOR, ANY DAMAGES TO, OR
                        VIRUSES THAT MAY INFECT, ORGANIZATION'S COMPUTER EQUIPMENT OR OTHER PROPERTY ON ACCOUNT OF
                        ORGANIZATION'S ACCESS TO OR USE OF THE LHN/ACE E-TOOLS OR ORGANIZATION'S DOWNLOADING OF ANY
                        MATERIALS, DATA, TEXT, OR IMAGES FROM THE LHN/ACE E-TOOLS.</p>
                    <p>INDEMNIFICATION</p>
                    <p>Organization shall defend, indemnify and hold harmless Owner and its employees, agents, insurers,
                        officers, directors, affiliates and subsidiaries from and against all liabilities, claims,
                        actions
                        or suits (including, but not limited to, court costs, reasonable attorneys' fees, awards and
                        settlements) to the extent the same arise out of or in connection with Organization's use of the
                        LHN/ACE E-tools, including, but not limited to, any revisions thereto, or Organization's
                        negligence,
                        willful misconduct or breach of this Agreement, including, but not limited to, a violation of
                        applicable law, rule, regulation or ordinance in Organization's performance hereunder.</p>
                    <p>INJUNCTIVE RELIEF</p>
                    <p>Organization acknowledges that the unauthorized use, transfer or disclosure of the LHN/ACE
                        E-tools or
                        copies thereof will (i) substantially diminish the value to Owner of the trade secrets and other
                        proprietary interests that are the subject of this Agreement; (ii) render Owner's remedy at law
                        for
                        such unauthorized use, disclosure or transfer inadequate; and (iii) cause irreparable injury in
                        a
                        short period of time. If Organization breaches any of its obligations with respect to the use of
                        the
                        LHN/ACE E-tools, Owner shall be entitled to equitable relief to protect its interests therein,
                        including, but not limited to, preliminary and permanent injunctive relief.</p>
                    <p>GOVERNING LAW</p>
                    <p>This Agreement and its performance shall be governed by the laws of the State of New Jersey,
                        without
                        regard to its conflict of laws provisions. Organization consents and submits to the exclusive
                        jurisdiction of the state and federal courts located in the State of New Jersey, in all
                        questions
                        and controversies arising out of Organization's use of the LHN/ACE E-tools and this Agreement.
                    </p>
                    <p>MODIFICATION OF AGREEMENT</p>
                    <p>Owner may, at any time, revise the terms and conditions of this Agreement. If any terms and
                        conditions contained in this Agreement are changed, Owner shall exercise commercially reasonable
                        efforts to provide Organization with such revisions and Organization agrees to be bound
                        accordingly.
                    </p>
                    <p>ASSIGNMENT</p>
                    <p>Organization shall not assign, delegate or transfer this Agreement or any of the rights or
                        obligations hereunder without the prior written consent of Owner. Any agreement made in breach
                        of this provision is null and void and of no legal force and effect, and Owner will have, in
                        addition to all other rights and remedies it may have hereunder, the right to terminate this
                        Agreement immediately. Notwithstanding the foregoing, Organization may assign this Agreement to
                        an affiliate or a subsidiary or a successor to that area of its business to which this Agreement
                        is related, subject to providing prior written notice to Owner.</p>
                    <p>Owner may, in its sole discretion, assign, delegate or transfer this Agreement or any of the
                        rights or obligations hereunder and shall use commercially reasonable efforts to notify
                        Organization accordingly.</p>
                    <p>The terms of this Agreement shall be binding upon and inure to the benefit of Organization, Owner
                        and the parties' respective successors and permitted assigns, if any.</p>
                    <p>ERM;
                        TERMINATION</p>
                    <p>This Agreement shall remain in full force and effect from the Effective Date for a period of one
                        (1) year and shall renew automatically for subsequent one-year terms thereafter, unless
                        terminated earlier in accordance with the terms of this Agreement. </p>
                    <p>Owner may terminate this Agreement upon thirty (30) days' written notice to Organization. Either
                        party may terminate this Agreement immediately in the event FDA or any other regulatory
                        authority, or any law, rule, or regulation prohibits the use of the LHN/ACE E-tools or if by
                        their intervention Owner may not feasibly provide the LHN/ACE E-tools.</p>
                    <p>SEVERABILITY</p>
                    <p>If any part of this Agreement shall be determined to be invalid or unenforceable by a
                        court of competent jurisdiction or by any other legally constituted body having jurisdiction to
                        make such determination, the remainder of this Agreement shall remain in full force and effect;
                        provided,
                        however, that the part of the Agreement thus invalidated or declared unenforceable is not
                        essential to the intended purposes of this Agreement.</p>
                    <p>WAIVER</p>
                    <p>The failure of either party to demand strict performance of any term or condition of this
                        Agreement shall not constitute a waiver
                        thereof or in any way limit or prevent subsequent strict enforcement of
                        such term or condition.</p>
                </div>
            </div>
        </div>
    </div>