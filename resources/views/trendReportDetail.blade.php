@section('title')
Trend Report
@endsection
@section('seo')
<title>LHN/ACE  - Trends Report</title>
<meta name="description" content="">
<meta name="keywords" content="">
@endsection
@extends('layouts.layout')
@section('content')
<!-- Main Container Starts -->
<div class="mainContainer">
    <!-- Start Here -->
    <div class="banner-container">
        <div class="banner-container__top  plain-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title">Liver Health Annual Trends Report
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="section text-container">
        <div class="container">
            <div class="row">
                <div class="col-12">


                    <div id="publitas-embed-xenx0bbs6dg"></div>
                    <script data-cfasync="false" data-height="undefined"
                        data-publication="https://cat02.cc-tools.online/liver-health-annual-trends-report-first-edition/"
                        data-publication-aspect-ratio="0.7725274725274726" data-responsive="true"
                        data-width="undefined" data-wrapper-id="publitas-embed-xenx0bbs6dg" publitas-embed
                        src="https://view.publitas.com/embed.js" type="text/javascript"></script>

                </div>
            </div>
        </div>
    </div>


</div>
<!-- Main Container Ends -->
@endsection

@section('js')
@endsection
