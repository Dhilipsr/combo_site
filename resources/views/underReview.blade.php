

@section('title')
Membership Under Approval Process
@endsection
@section('seo')
<title>LHN/ACE - Thank You</title>
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Membership under approval process</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact-msg">
    <div class="default-box ">
        <h2>Thank you for registering!</h2>
        <p>Your membership is in the approval process. We will alert you when you have access.</p>
    </div>
</div>
{{-- @include('partials.agreement') --}}
{{-- Ends Here --}}
@endsection
