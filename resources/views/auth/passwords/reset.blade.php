@section('seo')
<title>LHN/ACE - Reset Password</title>
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<div class="banner-container">
        <div class="banner-container__top  plain-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title">Reset Password</h2>

                    </div>
                </div>
            </div>
        </div>
</div>
<div class="login-box section">
    <div class="default-box">
        <div class="container pad">
            <h2>Fill in the information below to reset your password on this website.<br> This site
                is intended for US health care professionals only.</h2>
        </div>
    </div>
    <div class="container pad">
        <div class="row">
            <div class="col-md-12">
                <p class="ind-txt float-right"><span>*</span> Required Field</p>
            </div>
        </div>
        <form method="POST" action="{{ route('password.update') }}">
            <div class="row">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="col-md-6 form-box @error('password') error-box @enderror">
                    <label class="req">New Password</label>
                    <input type="password" class="form-field"  name="password">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @if ($errors->has('password'))
                            {{ $errors->first('password') }}
                        @endif
                    </div>
                </div>
                <div class="col-md-6 form-box">
                    <label class="req">Confirm Password</label>
                    <input type="password" id="password-confirm" class="form-field" name="password_confirmation">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" /> This field is required
                    </div>
                </div>
                <div class="col-md-6 form-box @error('email') error-box @enderror">
                    <label class="req">Your Email</label>
                    <input type="email" class="form-field"  name="email" value="{{ old('email') }}">
                    <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                        @error('email')
                            {{ $message }}
                        @enderror
                    </div>
                </div>

                <div class="col-md-12 form-box">
                    <button id="login" class="button d-blu ">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
{{-- <div class="reg-form section">

    <div class="container">


    </div>
</div> --}}
{{-- @include('partials.agreement') --}}
{{-- Ends Here --}}
@endsection