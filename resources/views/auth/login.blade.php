@extends('layouts.layout')
@section('seo')
<title>LHN/ACE - Log In</title>
<meta name="description" content="Log In">
<meta name="keywords" content="Log In">
@endsection

@section('css')
@endsection

@section('content')
 <!-- Main Container Starts -->
    <div class="mainContainer">
        <!-- Start Here -->
        <div class="banner-container">
            <div class="banner-container__top  plain-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title"> Log In

                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="login-box section">
            <div class="default-box">
                <div class="container pad">
                    <h2>Welcome back! Enter your email address and password below<br> for access to your account.</h2>
                </div>
            </div>
            <div class="container pad">
                <div class="row">
                    <div class="col-md-12">
                        <p class="ind-txt float-right"><span>*</span> Required field</p>
                    </div>
                </div>
                <form method="POST" action="{{route('login')}}">
                    <div class="row">
                         @csrf
                         <input type="hidden" name="last_url"
                         value="{{old('last_url') ? old('last_url') : Session::get('page')}}"
                         >
                        <div class="col-md-6 form-box @error('email') error-box @enderror">

                            <label class="req">Enter Email Address</label>

                            <input type="email" class="form-field" name="email" id="email" value=" "
                                {{-- required="required" --}}
                            >
                            @error('email')
                                <div class="error">
                                    <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                                    {{ $message }}
                                </div>
                            @enderror

                        </div>
                        <div class="col-md-6 form-box @error('password') error-box @enderror">
                            <label class="req">Password</label>

                            <input type="password" class="form-field" name="password" id="password"
                            {{-- required="required" --}}
                            >
                            @error('password')
                            <div class="error">
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-md-12">
                            <div class="check-box">
                                <input type="checkbox" id="chk1" name="remember">
                                <label for="chk1">Keep me signed in</label>
                            </div>
                        </div>
                        <div class="col-md-12 form-box">
                            <button id="login" class="button d-blu ">Log In</button>
                        </div>
                        <div class="col-md-12 forgot">
                            <a data-toggle="modal" data-target="#myModal">Forgot your password?</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="default-box login-register">
                <div class="form-box">
                    <h2>Don't have an account?</h2>
                    <p> Sign up now to get health tools, guidelines, and links.</p>
                </div>
                <div class="form-box w-auto">
                    <a href="{{route('register')}}" class="button d-blu ">Register</a>
                </div>
            </div>
        </div>
        <!-- Modal Starts here-->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-box">
                <div class="model-close" data-dismiss="modal">
                    <img src="{{asset('img/close-white.png')}}" alt="img" />
                </div>
                <div class="default-box">
                    <h3>Password Recovery</h3>
                    <p>Please enter your email address. <br>
                        You will receive a link via email to reset your password.</p>
                </div>
                <form method="POST" action="{{ route('password.email') }}">
                    <div class="row justify-content-center mt-3">
                        <div class="col-md-6 form-box error-box">
                            <p class="ind-txt float-right"><span>*</span> Required field</p>
                            <!-- <label class="req">Enter Email Address</label> -->
                            @csrf
                            <input type="email" class="form-field" name="email"
                                {{-- required --}}
                                placeholder="Enter Email Address">
                            <div class="error">
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                                 This field is required
                            </div>

                            <div class="form-box mt-4">
                                <button class="button">Request New Password</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Modal Ends Here -->
    </div>
    <!-- Main Container Ends -->
    @if(Session::has('under_review'))
    <div id="underReviewModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="img/close-white.png" alt="img" /></div>
            <div class="row   full-sz ">
                <div class="col-md-12">
                    <div class="default-box">
                    <h3 class="">Not Approved Yet </h3>

                    <p>We’re sorry. Your account has not been approved yet.  <br>
                        Confirmation of your registration may take up to 48 hours. <br>
                        You will be notified when your account has been approved via the email address provided during registration.</p>
                        <br><br>
                        <p><strong>If it has been more than 48 hours and you have not received a confirmation email, <br>
                        please contact us at <a href="mailto: support@liverhealthnow.com"> support@liverhealthnow.com</a>.
                        </strong></p>
                    </div>
                </div>
                <!-- <div class="col-md-12 text-center">
                    <div class="check-box">
                        <input type="checkbox" id="chk1">
                        <label for="chk1">Don’t show me this message again.</label>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    @endif
@endsection

@section('js')
<script type="text/javascript">
@if(Session::has('under_review'))
    $('#underReviewModal').modal('show');
@endif
</script>
@endsection