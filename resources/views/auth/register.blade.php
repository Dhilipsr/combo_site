@extends('layouts.layout')
@section('seo')
<title>LHN/ACE - Register</title>
<meta name="description" content="Register">
<meta name="keywords" content="Register">
@endsection

@section('css')
@endsection

@section('content')
 <!-- Main Container Starts -->
<div class="mainContainer">
        <!-- Start Here -->
        <div class="banner-container">
            <div class="banner-container__top  plain-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title"> Registration </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="reg-form section">
            <div class="default-box">
                <div class="container pad">
                    <h2>Fill in the information below to request access to the resources on this website. This site is
                        intended for US
                        health care professionals only.</h2>
                </div>
            </div>

            <div class="container pad">

                <div class="row">
                    <div class="col-md-12">
                        <p class="ind-txt float-right"><span>*</span> Required field</p>
                    </div>
                </div>
                <form method="POST" action="{{route('register')}}">
                    <div class="row">
                        @csrf
                        <div class="col-md-6 form-box @error('password') error-box @enderror">

                            <label class="req">Password</label>
                            <input type="password" class="form-field" name="password">
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('password')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box">
                            <label class="req">Confirm Password</label>
                            <input type="password" class="form-field" name="password_confirmation">
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" /> This field is required
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('email') error-box @enderror">

                            <label class="req">Email Address</label>
                            <input type="email" class="form-field" name="email" value="{{ old('email') }}">

                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('email')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('contact') error-box @enderror">

                            <label>Phone Number</label>
                            <input type="tel" class="form-field allownumericwithoutdecimal" name="contact"  value="{{ old('contact') }}">
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('contact')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="col-md-6 form-box @error('name') error-box @enderror">

                            <label class="req">First Name</label>
                            <input type="text" class="form-field" name="name" value="{{ old('name') }}">

                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('name')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('lname') error-box @enderror">

                            <label class="req">Last Name</label>
                            <input type="text" class="form-field" name="lname" value="{{ old('lname') }}">

                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('lname')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('organization') error-box @enderror">

                            <label class="req">Organization</label>
                            <input type="text" class="form-field" name="organization" value="{{ old('organization') }}">

                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('organization')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                     @php
                    $organizationtypes = App\Organizationtype::where(['status' => 1])->get();
                    $sources = App\Source::where(['status' => 1])->get();
                    $states = App\State::orderBy('state')->get();
                    @endphp
                        <div class="col-md-6 form-box @error('organization_id') error-box @enderror">

                            <label class="req">Organization Type</label>
                            <select  class="select2 form-field select2-clr" name="organization_id">
                                <option></option>
                                @foreach( $organizationtypes as $organizationtype )
                                <option value="{{$organizationtype->id}}">{{$organizationtype->type}}</option>
                                @endforeach
                            </select>
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('organization_id')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('job_title') error-box @enderror">

                            <label class="req">Job Title</label>
                            <input type="text" class="form-field" name="job_title" value="{{ old('job_title') }}">
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('job_title')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('npi_number') error-box @enderror">

                            <label >NPI Number</label>
                            <input type="text" class="form-field" name="npi_number" value="{{ old('npi_number') }}">
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('npi_number')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6 form-box @error('city') error-box @enderror">

                            <label class="req">City</label>
                            <input type="text" class="form-field" name="city">
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('city')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('organization_id') error-box @enderror">

                            <label class="req">State</label>
                            <select class="select2 form-field select2-clr" name="state_id">
                                <option></option>
                                @foreach( $states as $state )
                                <option value="{{$state->id}}">{{$state->state}}</option>
                                @endforeach
                            </select>
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('state_id')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 form-box @error('source_id') error-box @enderror">

                            <label>How did you hear about us?</label>
                            <select class="select2 form-field select2-clr" name="source_id" onchange='checkAbout(this.value);'>
                                <option></option>
                                {{-- <option>Through my Salix representative</option>
                                <option> Through a colleague</option>
                                <option> Through a web search</option>
                                <option> I received a code</option>
                                <option> Other</option> --}}
                                @foreach( $sources as $source )
                                <option value="{{$source->id}}">{{$source->sources}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 form-box @error('code') error-box @enderror" id="refcode"
                            @if ($errors->first('code'))
                            @else
                            style="display:none;"
                            @endif
                        >
                                <label class="req">Add Code Here</label>
                                <input type="text" class="form-field" name="code">
                                <div class="error"><img src="{{asset('img/error-arrow.png')}}" alt="error" />
                                    @error('code')
                                        {{ $message }}
                                    @enderror
                                </div>
                        </div>
                        <div class="col-md-6 form-box @error('salix_manager_name') error-box @enderror">

                            <label>Name of Salix Representative</label>
                            <input type="text" class="form-field" name="salix_manager_name">
                            <div class="error">
                                <img
                                    src="{{asset('img/error-arrow.png')}}" alt="error" />
                                @error('salix_manager_name')
                                {{ $message }}
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="check-box">
                                <input type="checkbox" id="chk1" name="update">
                                <label for="chk1">I agree to receive periodic updates and communications via email from
                                    this
                                    website.</label>
                            </div>

                            <div class="check-box  @error('agreement_check') error-box @enderror">

                                <input type="checkbox" id="chk2" name="agreement_check">
                                <label for="chk2" class="req">I accept the
                                    <a href="#" data-toggle="modal" data-target="#regModal">License Agreement.</a>
                                </label>
                                @error('agreement_check')
                                    <div class="error">
                                        <img
                                            src="{{asset('img/error-arrow.png')}}" alt="error" />

                                        {{ $message }}

                                    </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="g-recaptcha" data-sitekey="6Lfo6IYcAAAAAC9cYJWuvS8JQIjCpXB6uWcuYNTr"></div>
                            <div class="error"
                            style="display: block;"
                            >
                                @if($errors->has('g-recaptcha-response'))
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                                    {{ $errors->first('g-recaptcha-response') }}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12 form-box w-auto mb-0 mt-3 ml-auto mr-auto">
                            <button class="button  d-blu ">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--  {{-- <div class="contact-msg" style="display: none;">
        <div class="default-box ">
            <h2>Thank you for applying to become a member of LIVER<span>HEALTH</span>NOW!</h2>
            <p>You should expect an email within the next 48 hours with information about how to access the resources on this site.</p>
        </div>
    </div> --}} -->
        <!--
        {{-- Ends Here --}}
         -->
    </div>
<!-- Main Container Ends -->
@include('partials.regModal')
@endsection



@section('js')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>

  $(".select2").select2({
    allowClear:true,
			matcher: function(term, text) {
				return text.toUpperCase().indexOf(term.toUpperCase())==0;
			}
  })
  
  function checkAbout(val){
     var element=document.getElementById('refcode');
     if(val=='6')
       element.style.display='block';
     else  
       element.style.display='none';
  }

</script>

@endsection