@section('seo')
<title>LHN/ACE  - My Favorites</title>
<meta name="description" content="Favorite tools">
<meta name="keywords" content="Favorite tools">
@endsection
@extends('layouts.layout')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Start Here -->

    <div class="banner-container">
        <div class="banner-container__top  plain-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title"> {{ $username }}'s Favorite Tools  </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="container  section">

        <div class="refrences_sec favorites pad">
            {{-- <h5><i class="icon-img"><img src="img/heart-2.svg"></i>{{ Auth::user()->name }} Favorites </h5>
            <p>Save your favorite health tools here for easy access in the future. You can also share all your favorites
                with colleagues.</p> --}}
                <div class="container">
                    <ol>
                        @foreach ($favTools as $tool)
                        <li>
                            <a target="_blank"  href="{{ $tool->link }}">{{ optional($tool->healthtool)->title }}</a>
                        </li>
                        @endforeach
                    </ol>
                </div>
        </div>

    </section>
    {{-- success --}}


    @endsection

@section('extra-js')

    {{-- <script type="text/javascript">



        $(document).ready(function() {
            $('#old_password').keyup(function() {
                var old_password = $(this).val();
                // ajax call
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('password.check') }}",
                    type: "post",
                    data: {
                        'old_password': old_password
                    },
                    success: function(response) {
                        var status = response.status;
                        console.log(status)
                        if (status == 1) {
                            $("#new_password").removeAttr("disabled");
                            $("#confirm_password").removeAttr("disabled");
                        } else if (status == 0) {
                            $("#new_password").attr("disabled", true);
                            $("#confirm_password").attr("disabled", true);
                        }
                    }
                });
            });
            $('#confirm_password').keyup(function() {
                var new_password = $('#new_password').val();
                if ($(this).val() != new_password) {
                    $('#c_password').addClass('error-box');
                    $('#c_password_error_msg').text('Passwords do not match.!');
                    $('#update_password').attr('disabled', true);
                } else {
                    $("#update_password").removeAttr("disabled");
                    $('#c_password').removeClass('error-box');
                    $('#c_password_error_msg').text(' ');
                }
            });
        });

        // ajax image upload
        $(document).on('change', '#file', function() {
            var name = document.getElementById("file").files[0].name;
            var form_data = new FormData();
            var ext = name.split('.').pop().toLowerCase();
            if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                alert("Invalid Image File");
            }
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file").files[0]);
            var f = document.getElementById("file").files[0];
            var fsize = f.size || f.fileSize;
            if (fsize > 2000000) {
                alert("Image File Size is very big");
            } else {
                form_data.append("file", document.getElementById('file').files[0]);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('profilepicture.upload') }}",
                    method: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data) {
                        // console.log(data.message);
                        if (data.message == 1) {
                            // image changed
                            location.reload();
                        }
                    }
                });
            }
        });
        // ajax image upload ends
        function getFileData(myFile) {
            var file = myFile.files[0];
            var filename = file.name;
            console.log(filename);
            $('.upload-form-sec__2__name').html(filename);
            $('.upload-form-sec__1').hide();
            $('.upload-form-sec__2').show();
        }

        function fileRemove() {
            $('.upload-form-sec__2__name').html('');
            $('.upload-form-sec__1').show();
            $('.upload-form-sec__2').hide();
        }

        $(document).on('click', '.deleteFavTool', function(e) {
            var id = $(this).attr('data-fav-id');
            var elem = this;
            console.log(id);
            $.ajax({
                url: "{{ route('deleteFromFav') }}",
                method: "get",
                data: {
                    'id': id
                },
                success: function(data) {
                    // console.log(data.message);
                    if (data.status == 200) {
                        let li = elem.parentNode;
                        li.parentNode.removeChild(li);
                    }
                }
            });
        });

    </script> --}}
@endsection
