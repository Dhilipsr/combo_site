@extends('layouts.layout')
@section('seo')
<title>LHN Primary Care - {{$raise_bar->meta_title}}</title>
<meta name="description" content="{{$raise_bar->meta_description}}">
<meta name="keywords" content="{{$raise_bar->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<div class="main_section">
    <div class="container">
        <div class="main-title section">
            <h2 class="title">{{ $raise_bar->title }} </h2>
        </div>

        <div class="text-container ">

            {!! $raise_bar->content !!}

        </div>

        <!--<a href="https://cc1edu.talentlms.com/unit/view/id:2054" target="_blank">-->
        <a href="https://cc1edu.talentlms.com/shared/start/key:LZQIDNHR" target="_blank">
            <div class="video-container">
                <img src="{{ asset('img/video-img.png') }}" alt="">
            </div>
        </a>


    </div>
</div>
@endsection

@section('js')
@endsection