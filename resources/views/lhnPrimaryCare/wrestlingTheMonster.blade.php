@extends('layouts.layout')
@section('seo')
<title>LHN Primary Care - {{$raise_bar->meta_title}}</title>
<meta name="description" content="{{$raise_bar->meta_description}}">
<meta name="keywords" content="{{$raise_bar->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<div class="main_section">
    <div class="container">
        <div class="main-title section">
            <h2 class="title text-center">{{$raise_bar->title}}</h2>
        </div>



        <div class="text-container default-box">

            {!!$raise_bar->content!!}

        </div>

        <div class="video-container">
            {{-- <img src="img/video-img-2.jpg" alt=""> --}}
            <iframe width="100%" height="500" src="https://www.youtube.com/embed/l37Nz1bBQ5M" title="YouTube video player" frameborder="0"
             allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

    </div>
</div>
@endsection

@section('js')
@endsection