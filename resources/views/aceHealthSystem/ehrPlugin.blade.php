@extends('layouts.layout')
@section('seo')
<title>ACE Health Systems - {{$health_system_ehrplugin->meta_title}}</title>
<meta name="description" content="{{$health_system_ehrplugin->meta_description}}">
<meta name="keywords" content="{{$health_system_ehrplugin->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
    <!-- Main Container Starts -->
    <div class="mainContainer">
        <!-- Start Here -->
        <div class="banner-container">
            <div class="banner-container__top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title">{{ $health_system_ehrplugin->page_title }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section comm-txt">
            <div class="container">
                <div class="row  ">
                    <div class="col-lg-10  col-12 m-auto">
                        {!! $health_system_ehrplugin->description !!}
                    </div>
                </div>
            </div>
        </div>


        <div class="ehr-container section colour-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4  col-12">
                        <div class="ehr-box">
                            <h2>{{ $health_system_ehrplugin->plugin_one_title }} <span>Plugin</span> </h2>
                            <div class="ehr-img">
                                <img src="{{ asset('storage/'.$health_system_ehrplugin->plugin_image_one ) }}" alt="img" />
                            </div>
                            <a href="{{ $health_system_ehrplugin->plugin_one_link }}" target="_blank" class="button" target="_blank">View Live Sample</a>
                        </div>
                    </div>
                    <div class="col-lg-4  col-12">
                        <div class="ehr-box">
                            <h2>{{ $health_system_ehrplugin->plugin_two_title }} <span>Plugin</span> </h2>
                            <div class="ehr-img">
                                <img src="{{ asset('storage/'.$health_system_ehrplugin->plugin_image_two ) }}" alt="img" />
                            </div><a href="{{ $health_system_ehrplugin->plugin_two_link }}" target="_blank" class="button" target="_blank">View Live Sample</a>
                        </div>
                    </div>
                    <div class="col-lg-4  col-12">
                        <div class="ehr-box">
                            <h2>{{ $health_system_ehrplugin->plugin_three_title }} <span>Plugin</span> </h2>
                            <div class="ehr-img">
                                <img src="{{ asset('storage/'.$health_system_ehrplugin->plugin_image_three ) }}" alt="img" />
                            </div><a href="{{ $health_system_ehrplugin->plugin_three_link }}" target="_blank" class="button" target="_blank">View Live Sample</a>
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <a href="#" class="model-btn button m-width" data-toggle="modal" data-target="#guideModal">Order
                            Now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="section ehr-contact">
            <div class="container">
                <div class="row ">
                     <div class="col-lg-8 col-12 m-auto">
                        <a href="{{ $health_system_ehrplugin->title_two_link }}" class="link">{{ $health_system_ehrplugin->title_two }}</a>
                        <h3>{{ $health_system_ehrplugin->gina_text }}</h3>
                        <a href="https://www.research.net/r/LiverHealthNow_AccessClickEngage"
                        target="_blank"
                        class="button line-blk">GINA™</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Container Ends -->
     <div id="guideModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box-wht">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-rd.png')}}" alt="img" /></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="center-container">
                                    <h6 class="model-title">{{ $popup_content->title }}</h6>
                                   {!! $popup_content->content !!}
                                </div>
                            </div>

                            <div class="col-12 ">
                                <p class="form-title">Please select which ACE plugin(s) you are interested in:</p>
                                <form action="{{route('storePluginOrder')}}" method="post" class="form-validate" id="orderPlugin" novalidate="novalidate">
                                    @csrf
                                    <div class="radio-group">
                                        <div class="radio">
                                            <input id="radio-1" value="1" name="plugin_type" type="radio" checked>
                                            <label for="radio-1" class="radio-label">ACE Health Systems</label>
                                        </div>
                                        <div class="radio">
                                            <input id="radio-2" value="2" name="plugin_type" type="radio">
                                            <label for="radio-2" class="radio-label">ACE Long-Term Care
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input id="radio-3" value="3" name="plugin_type" type="radio">
                                            <label for="radio-3" class="radio-label">All Plugins
                                            </label>
                                        </div>
                                    </div>

                                    @if (Auth::user()->need_assessment == 1)
                                    <section class="field-section">
                                        <div class=" noted">
                                            <label><span>*</span>Indicates required field</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="req">Customer's name</label>
                                            <input id="name-from" name="name" type="text" class="form-field"
                                                required=""
                                                @if (Auth::user()->need_assessment == 1)
                                                value=""
                                                @endif
                                                >
                                            <div class="invalid-feedback">Please enter your name</div>
                                        </div>
                                        <div class="form-group">
                                            <label class="req">Customer's email</label>
                                            <input id="email-to" name="email" type="email" class="form-field"
                                                required="">
                                            <div class="invalid-feedback">Please enter your recipients email</div>
                                        </div>

                                        <div class="form-group">
                                            <label class="req">Customer's professional title</label>
                                            <input id="title-to" name="professional_title" type="text" class="form-field"
                                                required="">
                                            <div class="invalid-feedback">Please enter your professional title</div>
                                        </div>

                                        <div class="form-group">
                                            <label class="req">The name of the customer's organization</label>
                                            <input id="organization-to" name="organization_name" type="text"
                                                class="form-field" required="">
                                            <div class="invalid-feedback">Please enter your organization name
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="req">Customer's organization’s city</label>
                                            <input id="city-to" name="organization_city" type="text" class="form-field" required="">
                                            <div class="invalid-feedback">Please enter your organization city
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="req">Customer's organization’s state</label>
                                            <input id="state-to" name="organization_state" type="text" class="form-field"
                                                required="">
                                            <div class="invalid-feedback">Please enter your organization’s state
                                            </div>
                                        </div>
                                    </section>
                                    @endif

                                    <div class="form-action">
                                        <label class="container-checkbox" style="display: block;">
                                            @if(Auth::user()->need_assessment == 1)
                                                My customer wants a logo on their health tools.
                                            @else
                                                Yes, I want a logo on the health tools.
                                            @endif
                                            <input type="checkbox" name="custom_log">
                                            <span class="checkmark"></span>
                                        </label>

                                        <button class="model-btn button" type="submit">Submit</button>
                                        <!--   <a href="" class="model-btn button" data-toggle="modal"
                                                data-target="#guideModal">Submit</a> -->
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {{-- @if(Session::has('manager_success')) --}}
    <div id="manager_success" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row   full-sz ">
                <div class="col-md-12">
                    <div class="default-box">
                    <h4 class="white-txt">We have received your order</h4>

                    <p class="text">You will receive your customer's plugin via email<br>
                        within 1 week (5 business days).</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @endif --}}

    {{-- @if(Session::has('customer_success')) --}}
    <div id="customer_success" class="modal fade" role="dialog">
        <div class="modal-dialog modal-box">
            <div class="model-close" data-dismiss="modal"><img src="{{asset('img/close-white.png')}}" alt="img" /></div>
            <div class="row   full-sz ">
                <div class="col-md-12">
                    <div class="default-box">
                    <h4 class="white-txt">We have received your order.</h4>

                    <p class="text">
                        You will receive your plugin via email
                        within 1 week (5 business days).<br>
                        If you are interested in learning about EHR best practices on how to<br>
                        integrate the ACE plugin into existing EHR system, go to the<br>
                        GINA<sup>TM</sup> link at the bottom of the plugin page.
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @endif --}}
@endsection

@section('js')
<script>
    @if(Session::has('manager_success'))
        $('#manager_success').modal('show');
    @endif
    @if(Session::has('customer_success'))
        $('#customer_success').modal('show');
    @endif
    @if(Auth::user()->need_assessment == 1)
        $("#orderPlugin").validate({
            rules: {
                name: "required",
                email: "required",
                professional_title: "required",
                organization_name: "required",
                organization_city: "required",
                organization_state: "required",

            },
            messages: {
                name: "This field is required",
                email: "This field is required",
                professional_title: "This field is required",
                organization_name: "This field is required",
                organization_city: "This field is required",
                organization_state: "This field is required"
            }
        });
    @endif
</script>
@endsection