@extends('layouts.layout')

@section('seo')
<title>LHN/ACE - {{$content->meta_title}}</title>
<meta name="description" content="{{$content->meta_description}}">
<meta name="keywords" content="{{$content->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<!-- Banner Section -->
<div class="banner-container__main">
    <div class="h-banner-img img-full">
        <img src="{{('storage/'.$content->banner_image)}}" alt="img" />
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="h-banner-cont">
                    <ul class="banner-list-point">
                        {!! $content->content !!}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Banner Section Over-->

<!-- References Section -->
<div class="bend-lgt-yellow banner-container__bottom ">
    <div class="container">
        {!! $content->content_two !!}
        @if(!Auth::check())
            <a href="{{route('register')}}" class="button d-blu">Register Now</a>
        @endif
    </div>
</div>
<!-- References Section Over-->

<div class="section ">
    <!-- container class added -->
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <section class=" main-top-sec">
                    <div class="logo-section">
                        <img src="{{asset('img/lhn-logo.png')}}">
                    </div>
                    <section class="main-pg-logo-sec">
                        <a href="https://liverhealthnow.com/AC" class="button d-blu">{!! $content->lhn_btn_one !!}</a>

                        <p>{{ $content->lhn_content_one }}</p>
                    </section>
                    <section class="main-pg-logo-sec">
                        <a href="https://liverhealthnow.com/PC" class="button d-blu">{!! $content->lhn_btn_two !!}</a>

                        <p>{{ $content->lhn_content_two }}</p>
                    </section>

                </section>
            </div>

            <div class="col-md-6">
                <section class=" main-top-sec bdr-none ">
                    <div class="logo-section">
                        <img src="{{asset('img/ace-logo.png')}}">
                    </div>
                    <section class="main-pg-logo-sec">
                        <a href="https://accessclickengage.liverhealthnow.com/HS" class="button d-blu">{!! $content->ace_link_one !!}</a>

                        <p>{{ $content->ace_content_one }}</p>
                    </section>
                    <section class="main-pg-logo-sec">
                        <a href="https://accessclickengage.liverhealthnow.com/LTC" class="button d-blu">{!! $content->ace_link_two !!}</a>
                        <p>{{ $content->ace_content_two }}</p>
                    </section>
                </section>
            </div>
        </div>


    </div>
</div>
<div class="section pt-0">
    <!-- container class added -->
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="htBoxIn m-auto">
                    <div class="htImg"><img src="img/mag.jpg" alt="img"></div>
                    <div class="htCont">
                        <div class="htCont__in">

                            <p>{{$content->description_one}} <a href="{{route('longTermTrendReport')}}" target="_blank">Liver Health
                                    Annual Trends Report.</a> </p>
                        </div>

                    </div>
                </div>
            </div>

            {{-- <div class="col-md-6">
                <article class="center-section">
                    <h6 class="big">
                        Looking for support in treating patients with overt hepatic encephalopathy?
                        <a href="#"> Click here.</a>

                    </h6>
                </article>
            </div> --}}
        </div>


    </div>
</div>
<div class=" section  pt-0">
    <!-- container class added -->
    <div class="container">

        <div class="ref-hint">
            <p>{{$content->note}}
            </p>
        </div>


    </div>
</div>
@endsection

@section('js')
@endsection