@extends('layouts.layout')
@section('seo')
<title>LHN/ACE - Health Tools</title>
<meta name="description" content="Health Tools">
<meta name="keywords" content="Health Tools">
@endsection

@section('css')
@endsection

@section('content')
<!-- Main Container Starts -->
<div class="mainContainer">
    <!-- Start Here -->
    <div class="banner-container">
        <div class="banner-container__top plain-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title">Health Tools</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-30">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-10 col-12">
                    <h3>Find the appropriate health tools for your provider teams, patients, and their caregivers
                        quickly and easily by using the filters below.</h3>

                </div>
            </div>
        </div>
    </div>

    @include('partials.healthtoolfilter')

    <div class="htContainer">
        <div class="htContainerIn">
            @foreach( $healthtools as $healthtool )
            <div class="htBox ">
                <div class="htBoxIn">
                    <div class="htImg">
                        @if($healthtool->video_url)
                            <a href="{{$healthtool->video_url}}" target="_blank" target="_blank" ><img src="{{asset('img/play.png')}}" alt=""></a>
                        @endif
                        <img src="{{asset('storage/'.$healthtool->image)}}" alt="img" />
                    </div>
                    <div class="htCont">
                        <div class="htCont__in">
                            <h2>{{$healthtool->title}}</h2>
                            <p>{!! $healthtool->description !!}</p>
                        </div>
                        @if($healthtool->video_url)
                            <a  href="{{$healthtool->video_url}}"
                            target="_blank"
                            class="button click_check">
                            View
                            </a>
                        @else
                            <a  href="{{route('customizeHealthTools', ['healthtool_id' => $healthtool->id ] )}}"
                                class="button click_check"
                                data-label = "{{$healthtool->title}}"
                                onClick="ga('send', 'event', 'View/Customized', 'Click', '{{$healthtool->title}}');">
                                View/Customize
                            </a>

                        @endif
                    </div>
                </div>
            </div>
            @endforeach

            <div class="clearfix"></div>
        </div>
        <div class="paginat">

            {{ $healthtools->appends([
                'section' => Request::get('section'),
                'targetaudience' => Request::get('targetaudience'),
                'patienttype' => Request::get('patienttype') ,
                'searchbar' => Request::get('searchbar') ,
                'sort' => Request::get('sort'),
                'lang' => Request::get('lang'),
            ])->links('vendor.pagination.default') }}

        </div>
    </div>


    <div class="section ehr-contact">
        <div class="container">
            <div class="row ">
                <div class="col-lg-8 col-12 m-auto">


                    <h3>GINA™ (Guided INtegration Assistant) can help you
                        integrate the plugin into your EHR system.</h3>
                    <a href="https://www.research.net/r/LiverHealthNow_AccessClickEngage"
                    target="_blank"
                    class="button line-blk">GINA™</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Container Ends -->

<!-- Footer Starts -->
<div class="footer-note">
    <p>Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for
        informational purposes only.
        Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>
</div>
@endsection

@section('js')
<script src="{{asset('js/classie.js')}}"></script>
    <script src="{{asset('js/uisearch.js')}}"></script>
    <script>
        new UISearch(document.getElementById('sb-search'));
    </script>
@endsection