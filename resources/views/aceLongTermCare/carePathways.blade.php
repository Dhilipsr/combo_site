@extends('layouts.layout')
@section('seo')
<title>ACE Long-Term Care - {{$care_pathway->meta_title}}</title>
<meta name="description" content="{{$care_pathway->meta_description}}">
<meta name="keywords" content="{{$care_pathway->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<div class="mainContainer">
        <!-- Start Here -->
        <div class="banner-container">
            <div class="banner-container__top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title">{{ $care_pathway->page_title }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section comm-txt">
        <div class="container">
            <div class="row  ">
                <div class=" col-12">
                    {!! $care_pathway->description !!}
                </div>
            </div>
        </div>
    </div>

    <div class="section pt-0 ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="points-list">
                        <ul>
                            @foreach($pathways as $pathway)
                                <li>
                                    <h4>{{$pathway->title}}</h4>
                                    <a href="{{$pathway->link}}" target="_blank">{{$pathway->link_title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section  pt-0">
        <div class="container">
            <div class="row  justify-content-center">
                <div class="col-lg-9 col-12">
                    <div class="points-list bg-none">
                        <ul>
                            <li><a href="{{ $care_pathway->link_one }}"  >{{ $care_pathway->link_one_title }}</a></li>

                            <li><a href="{{ $care_pathway->link_two }}"  >{{ $care_pathway->link_two_title }}</a></li>

                            <li><a href="{{ $care_pathway->link_three }}"  >{{ $care_pathway->link_three_title }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection