@extends('layouts.layout')
@section('seo')
<title>LHN/ACE - {{$guideline->meta_title}}</title>
<meta name="description" content="{{$guideline->meta_description}}">
<meta name="keywords" content="{{$guideline->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<div class="mainContainer">
    <!-- Start Here -->
    <div class="banner-container">
        <div class="banner-container__top  plain-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title">{{ $guideline->title }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="section dgrey-txt  text-left">
        <div class="container">
            <div class="row  ">
                <div class=" col-12">
                    {!! $guideline->content !!}
                </div>



            </div>
        </div>
    </div>

    <div class="section pt-0 ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="yellow-list">
                        <ul>
                            <li>
                                <h4> <a href="#" data-toggle="modal" data-target="#guideModal"
                                    data-url = "https://www.aasld.org/publications/practice-guidelines" class="guidelineURL"
                                    >American Association for the Study of Liver Diseases (AASLD) Practice Guidelines
                                </h4></a>
                                <a href=" #" data-toggle="modal" data-target="#guideModal"
                                data-url = "https://aasldpubs.onlinelibrary.wiley.com/doi/10.1002/hep.20703" class="guidelineURL"
                                >Acute Liver Failure,
                                        Management </a>

                                    <a href="#" data-toggle="modal" data-target="#guideModal"
                                    data-url = "https://www.aasld.org/membership/member-resources/special-interest-groups/alcohol-associated-liver-disease" class="guidelineURL"
                                    > Alcohol-associated
                                        Liver
                                        Disease</a>
                                    <a href="#" data-toggle="modal" data-target="#guideModal"
                                    data-url = "https://www.aasld.org/sites/default/files/2019-06/141020_Guideline_Ascites_4UFb_2015.pdf" class="guidelineURL"
                                    >Ascites Due to
                                        Cirrhosis,
                                        Management
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#guideModal"
                                    data-url = "https://www.aasld.org/sites/default/files/2019-06/141022_AASLD_Guideline_Encephalopathy_4UFd_2015.pdf" class="guidelineURL"
                                    >Hepatic Encephalopathy
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#guideModal"
                                    data-url = "https://aasldpubs.onlinelibrary.wiley.com/doi/pdf/10.1002/hep.29367" class="guidelineURL"
                                    >Nonalcoholic Fatty
                                        Liver
                                        Disease, Diagnosis and Management
                                    </a>
                            </li>


                        </ul>
                    </div>

                    <div class="yellow-list">
                        <ul>
                            <li>
                                <h4> <a href="#" data-toggle="modal" data-target="#guideModal"
                                    data-url = "https://liverfoundation.org/for-patients/resources/webinars/#hepatic-encephalopathy" class="guidelineURL"
                                    >American Liver Foundation (ALF) Patient Profile and Support Videos
                                </h4></a>

                            </li>


                        </ul>
                    </div>

                    <div class=" yellow-list">
                        <ul>
                            <li>
                                <h4> <a href="#" data-toggle="modal" data-target="#guideModal"
                                    data-url = "https://aasldpubs.onlinelibrary.wiley.com/doi/10.1002/hep.30489" class="guidelineURL"
                                    >Development of Quality Measures in Cirrhosis by the Practice
                                        Metrics Committee
                                        of the American Association for the Study of Liver Diseases

                                </h4></a>

                            </li>


                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).on('click', '.guidelineURL', function() {
        var url = $(this).attr('data-url');
        console.log(url);
        $('#leaveSiteAnchor').attr('href', url);
        // console.log(page);

    });
</script>
@endsection