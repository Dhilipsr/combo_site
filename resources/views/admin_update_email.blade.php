Hi Admin,<br>

A new user has registered to Liverhealthnow.com with details as mentioned below.<br>

Name = {{ $name }} <br>
Last Name = {{ $lname }} <br>
Email = {{ $email }} <br>
Salix Manager Name = {{ $manager }} <br>
Organization = {{ $organization }} <br>