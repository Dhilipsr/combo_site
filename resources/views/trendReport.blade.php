@extends('layouts.layout')
@section('seo')
<title>LHN/ACE - {{$report_data->meta_title}}</title>
<meta name="description" content="{{$report_data->meta_description}}">
<meta name="keywords" content="{{$report_data->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<!-- Main Container Starts -->
    <div class="mainContainer">
        <!-- Start Here -->
        <div class="banner-container">
            <div class="banner-container__top  plain-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title">{{ $report_data->title }}
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section text-container">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        {!! $report_data->description !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="section pt-0 ">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="yellow-list">

                            <section class="htBoxIn">
                                <div class="htImg">
                                    <img src="{{ asset('storage/'.$report_data->image) }}" alt="">
                                </div>

                                <aside class="htCont">
                                    <div class="htCont__in">
                                        <h2> {{ $report_data->title_two }}</h2>
                                        <p> {{ $report_data->description_two }}</p>

                                        <a href="{{route('trendReportDetail')}}" class="button d-blu">View</a>
                                    </div>
                                </aside>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Container Ends -->
@endsection

@section('js')
@endsection