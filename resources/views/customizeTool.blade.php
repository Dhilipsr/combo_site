@extends('layouts.layout')
@section('seo')
<title>LHN/ACE - {{$healthTool->title}}</title>
<meta name="description" content="Customize Health Tools">
<meta name="keywords" content="Customize Health Tools">
@endsection

@section('css')
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="banner-container">
    <div class="banner-container__top  plain-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title">Customize Your Tool</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=" customize-pg">
    <div class="container">
        <div class="row">
            <div class="w50">
                <div class="customize-img-sec">
                    <img src="{{asset('storage/'.$healthTool->customize_image)}}">
                </div>
            </div>
            <div class="w50 ">
                <div class="customize-sec">
                    <form class="form">
                        <div class="main-area">
                            <section class="side-space">
                                <div class="check-box">
                                    <input type="checkbox" id="chk1" name="update">
                                    <label for="chk1">I do not want to customize my tool<br>
                                        (Go directly to <strong>Preview tool</strong> button below)</label>
                                </div>
                            </section>
                            <hr>
                            <section class="side-space">
                                <div class="title"><img src="{{asset('img/icon-01.svg')}}"> Customize Your Health Tool</div>
                                <div class="upload-sec">
                                    <label>Max upload size is 2 mb</label>
                                    <div class="file-upload-wrapper" data-text="Upload your organization’s logo">
                                        <input
                                        name="file-upload-field" type="file"
                                        class="file-upload-field" value="" id="file">
                                    </div>
                                </div>
                                <div class=" form-box">
                                    <div class="check-box center-width">
                                        <input type="checkbox" id="orgImage" name="update">
                                        <label for="orgImage">Go to my profile settings to grab my
                                            organization's pre-loaded logo. </label>
                                    </div>
                                    <!-- <button>Create Link</button> -->
                                </div>
                                <div class="wait-pop d-none" id="wait-pop">
                                    <h4>PLEASE WAIT...</h4>
                                    <p>Link is generating. This takes about 15 seconds.</p>
                                </div>
                                <div class="riview-sec">
                                    <label>Click on the URL to review your customized health tool.</label>
                                    <div class="file-copy-wrapper">
                                        <input name="" type="text" class="  " value="" id="myInput">
                                    </div>
                                    <button class="copy-url">Copy URL</button>
                                </div>
                                <div class="form-box mr-top-01 preview">
                                    <a href="#" class="button" id="previewTool" target="_blank">Preview tool</a>
                                    <label>(Your preview will open in another window)</label> <button
                                        class="favrite-btn"><i class="icon-img"><img src="{{asset('img/heart.svg')}}"></i>Add
                                        tool to favorites
                                        on your profile page
                                    </button>
                                </div>
                                <a href="{{ url()->previous() }}" class="back">Back to previous page</a>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    let link ;
    console.log(link)
    var intermediateLink;
    var organization = "{{ Auth::user()->organization }}" ;
    const toBase64 = file =>
                new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => resolve(reader.result);
                    reader.onerror = error => reject(error);
                });

    $(document).on('click', '#chk1', function(e)
    {
        $('#create_link').text('Creating link...');
        if ($(this).is(":checked")) {
            $('#file').parent(".file-upload-wrapper").attr("data-text",'Upload your organization’s logo');
            $('#orgImage').prop('checked',false);
            $('#myInput').val('');
            $('#wait-pop').removeClass('d-none');
            let page_id = "{{ $healthTool->id }}" ;


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type : 'POST',
                url : '{{route("noLogoUpload")}}',
                data : {
                    page_id: page_id,
                    'organization': organization,
                },
                success: function(result){
                    console.log(result);
                    link = result;
                    $('#myInput').val(link);
                    $("#previewTool").attr("href", link);
                    intermediateLink = link;
                    $("#create_link").text('Create link');
                    $('#wait-pop').addClass('d-none');
                }
            });

        }else{
                $('#myInput').val('');
                $("#previewTool").attr("href", '#');
                $("#loading").hide();

        }
        // console.log(link);
    });

    $(document).on('change', '#file', function()
    {
        if( $('#chk1').is(':checked') == true )
        {
            $('#chk1').prop('checked',false);
        }
        if( $('#orgImage').is(':checked') == true )
        {
            $('#orgImage').prop('checked',false);
        }
        $('#create_link').text('Creating link...');


        var name = document.getElementById("file").files[0].name;
        let file_exists = $('#file').val();
        var form_data = new FormData();
        var ext = name.split('.').pop().toLowerCase();

        if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1)
        {
            alert("Invalid Image File");
            $('#create_link').text('Create link');
            return false;
        }
        else if(file_exists.length == 0)
        {
            $('#create_link').text('Create link');
            return false;
        }

        var f = document.getElementById("file").files[0];
        var fsize = f.size||f.fileSize;
        if(fsize > 2000000)
        {
            alert("Image File Size is very big");
            $('#create_link').text('Create link');
            return false;
        }
        else
        {
            $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
            $('#myInput').val('');
            function Main() {
                fileUpload = document.getElementById('file').files[0];
                const file = fileUpload;
                return toBase64(file);
            }
            $('#wait-pop').removeClass('d-none');
            Main().then(function (response) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                console.log(response);

                $.ajax({
                    url:"{{route('logo.upload')}}",
                    method:"POST",
                    data: {'file' : response,'healthtool_id' : '{{$healthTool->id}}','organization': organization,},
                    // data: form_data,
                    success:function(data)
                    {
                        $('#create_link').text('Create link');
                        console.log(data);
                        if (data.status == 200) {
                            // image changed
                            $('#myInput').val(data.url);
                            intermediateLink = data.url;
                            link = data.url;
                            $("#previewTool").attr("href", link);
                            $('#wait-pop').addClass('d-none');
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        }
    });
  // ajax image upload ends
    $(document).on('click', '#create_link', function(e)
    {
        e.preventDefault();

        $('#myInput').val(link);
        $("#previewTool").attr("href", link);
    });

    $(document).on('click', '#orgImage', function(e){

        e.preventDefault();
        var checkAvatar = ('{{ Auth::user()->avatar }}');
        if(checkAvatar == 'users/default.png' || checkAvatar == ''){
            alert('Please upload a logo to your profile page to use this feature.')
        }else{
            $('#wait-pop').removeClass('d-none');
            $('#file').parent(".file-upload-wrapper").attr("data-text", 'Upload your organization’s logo');
            $('#chk1').prop('checked',false);
            $('#myInput').val('');
            $('#create_link').text('Creating link...');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url:"{{route('logo.upload.two')}}",
                method:"POST",
                data: {'orgImage' : 1, 'healthtool_id': "{{$healthTool->id }}" },
                success:function(data)
                {
                    // console.log(data);
                    if (data.status == 200) {
                        $('#create_link').text('Create link');
                        intermediateLink = data.url;
                        link = data.url;
                        $('#myInput').val(intermediateLink);
                        $("#previewTool").attr("href", link);
                        $('#wait-pop').addClass('d-none');
                    }
                },
                error:function(jqXHR, exception)
                {
                    console.log("HIi");
                    $('#create_link').text('Create link');
                    alert("jqXHR.message");
                }
            });
        }

        // });

    });



  // copy url starts
    $('.copy-url').click(function (event) {
        if($("#myInput").val() == ''){
            event.preventDefault();
            alert("Please select an option first to create a link");
        }else{
            copyT();
            $(this).html("<i><img src='{{asset('img/tick.svg')}}'></i> Copied");
            event.preventDefault();
        }

    });


  function copyT() {
      /* Get the text field */
      var copyText = document.getElementById("myInput");

      /* Select the text field */
      copyText.select();
      copyText.setSelectionRange(0, 99999); /*For mobile devices*/

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
  }
  // copy url ends

  // fav button starts
    $('.favrite-btn').click(function (event) {
        event.preventDefault();
        console.log(link)
        if(link && link != undefined)
        {
            let user_id = '{{ Auth::user()->id }}';
            let healthtool_id = '{{ last(request()->segments()) }}';
            console.log(healthtool_id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url:"{{route('postToFav')}}",
                method:"POST",
                data: {'user_id' : user_id , 'healthtool_id' : healthtool_id, 'link' : link,'new_link' : 1 },
                success:function(data)
                {
                    console.log(data);
                    if (data.status == 200)
                    {
                        // image changed
                        $('.favrite-btn').html("<i class='icon-img'><img src='{{asset('img/tick.svg')}}'></i> Added to your profile page");
                    }
                }
            });

        }
        else
        {
            event.preventDefault();
            alert('Please select an option first to create a link');
        }


    });
  // fav button ends
</script>
@endsection