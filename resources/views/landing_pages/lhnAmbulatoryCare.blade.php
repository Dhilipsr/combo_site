@extends('layouts.layout')
@section('seo')
<title>LHN Ambulatory Care - {{$section_data->meta_title}}</title>
<meta name="description" content="{{$section_data->meta_description}}">
<meta name="keywords" content="{{$section_data->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<!-- Banner Section -->
<div class="banner-container__bottom bend-yellow">
    <div class="container">
        <div class="row justify-content-center">

            <div class="award__banner">

                <img src="img/ribben-w.png" alt="">
                <p>{!! $section_data->title !!}
                </p>

            </div>
        </div>
    </div>
</div>
<section class="slider_sec">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            @foreach ($lhn_primary_carebanner as $key => $banners )
                <div class="swiper-slide">
                    <img src="{{ asset('storage/'.$banners->image) }}" class="ele img-0{{++$key}}">
                    <article class="slider-points point-3">
                        {!! $banners->content !!}
                    </article>
                </div>
                {{-- <a href="#">Register Now</a> --}}
            @endforeach

            {{-- <div class="swiper-slide ">
                <img src="img/slider-02.png" class="ele img-03">
                <article class="slider-points point-3">
                    <h5>
                        <span class="big-2">Chronic Liver Disease</span> <br>
                        can lead to<span class="big-5 yel_colr hep"> <b
                                class="super">Cirrhosis<sup>2</sup></b></span>
                    </h5>
                </article>
            </div>

            <div class="swiper-slide ">
                <img src="img/slider-03.png" class="ele img-03">
                <article class="slider-points point-3">
                    <h5>
                        Up to <span class="big-3">80%</span> of patients with <span
                            class="big-4">Cirrhosis</span><br>will eventually develop some form of<br>
                        <span class="big-5 yel_colr hep">Hepatic Encephalopathy <b
                                class="super">(HE)<sup>*3</sup></b></span>
                    </h5>
                    <p>*Ranging from minimal to overt.</p>
                </article>
            </div>

            <div class="swiper-slide">
                <img src="img/slider-04.png" class="ele img-04">
                <article class="slider-points ">
                    <h5>In 2018, <span class="big-3">37%</span> of hospitalized patients with<br><span
                            class="big-2 yel_colr">overt hepatic encephalopathy (OHE)</span><br>
                        were <span class="big-1 yel_colr">readmitted </span>within 30 <b
                            class="super">days<sup>4</sup></b>
                    </h5>

                    <p>*HE cases are identified by ICD-10 codes K70.41 (alcoholic hepatic failure with coma), K71.11
                        (toxic liver disease with
                        hepatic necrosis, with coma), K72.01 (acute and subacute hepatic failure with coma), K72.11
                        (chronic hepatic failure with
                        coma), K72.90 (hepatic failure, unspecified without coma), and K72.91 (hepatic failure,
                        unspecified with coma). Unless
                        otherwise specified, hospital case data include primary and secondary diagnoses. Hospital
                        case data for 2018 were
                        collected from October 1, 2017, to September 30, 2018.
                    </p>
                </article>

            </div>

            <div class="swiper-slide ">
                <img src="img/slider-05.png" class="ele img-05">
                <article class="slider-points">
                    <h5>
                        Patients who have been hospitalized with <span class="big-2 yel_colr">OHE</span> have <br
                            class="br-none">
                        a
                        <span class="big-4 ">42%</span> probability of <span class="big-4 yel_colr">survival</span>
                        at 1
                        <b class="super">year<sup>*5</sup></b>
                    </h5>
                    <p>*This is from a retrospective chart review of medical records from 1990-1993.</p>
                </article>
            </div> --}}

            {{-- <div class="swiper-slide">
                <img src="img/slider-06.png" class="ele img-06">
                <article class="slider-points">
                    <h5>Here’s how <br> you can <span class="big-3">Impact Care</span> for patients with<br>
                        <span class="big-5 yel_colr">
                            Chronic Liver Disease</span>
                    </h5>
                    <a href="#">Register Now</a>
                </article>
            </div> --}}
        </div>
        <!-- <div class="swiper-pagination swiper-pagination-white"></div> -->
    </div>
</section>

<!-- Banner Section Over-->
  <!-- References Section -->
    <section class="refrences_container">
        <div class="refrences_sec">
            <h5>References </h5>
        </div>
        <div class="slider">
            <div class="container">
                {!! $section_data->reference !!}
            </div>
        </div>
    </section>
    <!-- References Section Over-->

    <!-- Main Section -->
    <div class="section  pb-0">
        <div class="container">
            {!! $section_data->title_two !!}
            <div class="default-box">
                {!! $section_data->description_one !!}
            </div>
        </div>
    </div>
    <!-- Main Section Over-->
    <div class=" section  pt-0">
        <!-- container class added -->
        <div class="container">
            <div class="htBoxIn m-auto">
                <div class="htImg"><img src="{{ asset('storage/' .  $section_data->image) }}" alt="img"></div>
                <div class="htCont">
                    <div class="htCont__in">
                       {!! $section_data->content !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
        var swiper = new Swiper(".swiper-container", {
            spaceBetween: 30,
            effect: "fade",
            autoplay: true,
            fadeEffect: {
                crossFade: true
            },
            speed: 3000,
        });
    </script>
@endsection
