@extends('layouts.layout')
@section('seo')
<title>ACE Long-Term Care - {{$ace_long_termcare->meta_title}}</title>
<meta name="description" content="{{$ace_long_termcare->meta_description}}">
<meta name="keywords" content="{{$ace_long_termcare->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<div class="mainContainer">
        <!-- Start Here -->
        <div class="banner-container">
            <div class="banner-container__top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!--   <h2 class="title">Help Improve Care for OHE Patients</h2> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-container__main  alt-banner">
                <div class="h-banner-img img-full">
                    <img src="{{ asset('storage/'.$ace_long_termcare->banner_image) }}" alt="img" />
                </div>
                <div class="container">
                    <div class="row">

                        <div class="h-banner-cont">
                            <h2 class="title">{{ $ace_long_termcare->banner_description }}
                            </h2>

                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>

    <div class=" section">
        <!-- container class added -->
        <div class="container">

            <div class="default-box">
                <div class="abt-det">

                    <p class="large-box">{!!  $ace_long_termcare->description_one !!}</p>

                    @if(!Auth::check())
                    <a href="{{route('register')}}" class="button d-blu">Register Now</a>
                    @endif
                </div>
            </div>
            <!-- abt-award added -->


        </div>
    </div>
    <div class=" section pt-0">
        <!-- container class added -->
        <div class="container">


            <div class="default-box">
                <div class="abt-det">
                    <h4>{{ $ace_long_termcare->title}}</h4>
                    <p class="small-box">{{ $ace_long_termcare->sub_title}}</p>
                </div>
            </div>
            <!-- abt-award added -->
        </div>
    </div>


    <div class="section pt-0">
        <!-- container class added -->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="htBoxIn m-auto">
                        <div class="htImg"><img src="{{ asset('storage/'.$ace_long_termcare->image) }}" alt="img"></div>
                        <div class="htCont">
                            <div class="htCont__in">
                                {!! $ace_long_termcare->content_one !!}
                            </div>

                        </div>
                    </div>
                </div>

                {{-- <div class="col-md-6">
                    <article class="center-section">
                        <h6 class="big">
                            <a href="#"> Click here</a> to order a Cirrhosis Management for Long-term Care Order Set
                            Kit.

                        </h6>
                    </article>
                </div> --}}
            </div>


        </div>
    </div>




    <div class=" section  pt-0">
        <!-- container class added -->
        <div class="container">

            <div class="ref-hint">
                {{-- <p><strong> Reference: 1.</strong> Johnson EA, Spier BJ, Leff JA, Lucey MR, Said A. Optimizing the care
                    of patients with
                    cirrhosis and gastrointestinal hemorrhage:
                    a quality improvement study.<i> Aliment Pharmacol Ther.</i> 2011;34:76–82.
                </p> --}}
                {!! $ace_long_termcare->content_two !!}
            </div>


        </div>
    </div>
@endsection

@section('js')
@endsection
