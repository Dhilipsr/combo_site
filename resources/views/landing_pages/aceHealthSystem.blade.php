@extends('layouts.layout')
@section('seo')
<title>ACE Health Systems - {{$health_system->meta_title}}</title>
<meta name="description" content="{{$health_system->meta_description}}">
<meta name="keywords" content="{{$health_system->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
<div class="mainContainer">
        <!-- Start Here -->
        <div class="banner-container">
            <div class="banner-container__top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title">{{ $health_system->page_title }}</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-container__main">
                <div class="h-banner-img img-full">
                    @php
                    //   print_r($health_system->banner_image);
                    //   die();
                    // $images = json_decode($health_system->banner_image);

                    @endphp
                    <img src="{{ asset('storage/'.$health_system->banner_image) }}" alt="img" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="h-banner-cont">
                                <p>{{ $health_system->banner_description }}</p>
                                @if(!Auth::check())
                                    <a href="{{route('register')}}" class="button">Register Now</a>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="banner-container__bottom ">
                <div class="container">
                    <div class="row justify-content-center">

                        <div class="award__banner">

                            <img src="img/ribben.png" alt="">
                            <p><strong>Digital Health Award Winner</strong> for Interactive Content and Rich Media
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="about-box section">
        <!-- container class added -->
        <div class="container">
            <div class="default-box">

                <div class="abt-det">
                    <h4>{{ $health_system->about_title }}</h4>
                    <p class="small-box">{!! $health_system->about_description !!}</p>
                </div>
            </div>
            <!-- abt-award added -->
        </div>
    </div>
    <div class="about-box section pt-0">
        <!-- container class added -->
        <div class="container">
            <div class="htBoxIn m-auto">
                <div class="htImg"><img src="{{ asset('storage/'.$health_system->image_two) }}" alt="img"></div>
                <div class="htCont">
                    <div class="htCont__in">
                       <p> {{$health_system->description_two}}
                        <a href="https://liverhealthnow.com/trends-report" target="_blank">Liver Health
                                    Annual Trends Report.</a>
                       </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection
