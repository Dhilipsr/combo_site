@extends('layouts.layout')
@section('seo')
<title>LHN Primary Care - {{$section_data->meta_title}}</title>
<meta name="description" content="{{$section_data->meta_description}}">
<meta name="keywords" content="{{$section_data->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
    <!-- Banner Section -->
    <div class="banner-container__bottom bend-blue">
        <div class="container">
            <div class="row justify-content-center">

                <div class="award__banner">

                    <img src="img/ribben-w.png" alt="">
                    <p>{!! $section_data->title !!}
                    </p>

                </div>
            </div>
        </div>
    </div>
    <section class="slider_sec" >

        <div class="swiper-container">
            <div class="swiper-wrapper">
                 @foreach ($lhnAmbulatorycare_banner as $key => $banners )
                    <div class="swiper-slide ylw-bg ">
                    <img src="{{ asset('storage/'.$banners->image) }}" class="ele
                    @if($loop->last)
                    img-06
                    @else
                    img-0{{++$key}}
                    @endif
                    ">
                    <article class="slider-points point-3">
                        {!! $banners->content !!}
                    </article>
                </div>
                {{-- <a href="#">Register Now</a> --}}
            @endforeach

                {{-- <div class="swiper-slide ylw-bg ">
                    <img src="img/slider-01a.png" class="ele img-03">
                    <article class="slider-points point-3">
                        <h5>
                            In 2018, about <span class="big-4">4.5 million</span> people in the US had<br>
                            <span class="big-5  hep"> <b class="super">Chronic Liver
                                    Disease<sup>1</sup></b></span>
                        </h5>
                    </article>
                </div>
                <div class="swiper-slide ylw-bg ">
                    <img src="img/slider-02a.png" class="ele img-03">
                    <article class="slider-points point-3">
                        <h5>
                            <span class="big-2">Chronic Liver Disease</span> <br>
                            can lead to<span class="big-5  hep"> <b class="super">Cirrhosis<sup>2</sup></b></span>
                        </h5>
                    </article>
                </div>

                <div class="swiper-slide ylw-bg ">
                    <img src="img/slider-03a.png" class="ele img-03">
                    <article class="slider-points point-3">
                        <h5>
                            <span class="big-4">Cirrhosis</span> can lead to<br>
                            <span class="big-5  hep">Hepatic Encephalopathy <b class="super">(HE)</b></span>
                            and/or other complications<sup>3</sup>
                        </h5>
                    </article>
                </div>

                <div class="swiper-slide ylw-bg">
                    <img src=" img/slider-06a.png" class="ele img-06">
                    <article class="slider-points">
                        <h5>Here’s how <br> you can <span class="big-3">Impact Care</span> for patients with<br>
                            <span class="big-5 ">
                                Chronic Liver Disease</span>
                        </h5>
                        <a href="#" class="blu-btn">Register Now</a>
                    </article>

                </div> --}}
            </div>
            <!-- <div class="swiper-pagination swiper-pagination-white"></div> -->
        </div>
    </section>

    <!-- Banner Section Over-->
    <!-- References Section -->
    <div class="banner-container__bottom " >
        <div class="container">
          {!! $section_data->description_one !!}

        </div>
    </div>
    <!-- References Section Over-->

    <!-- Main Section -->
    <div class="section pt-0 " >
        <div class="container">

            <div class="default-box">
               {!! $section_data->description_two !!}
            </div>
        </div>
    </div>
    <!-- Main Section Over-->
    <div class=" section  pt-0">
        <!-- container class added -->
        <div class="container" >

            <div class="row  ">

                <div class="col-md-6">
                    <section class="new-contect-sec">
                        <div class="top-sec">
                            <aside class="paly-ic"><img src="img/play-ic.png" alt=""></aside>
                            <a href="{{ $section_data->video_title_one_link }}"> {{ $section_data->video_title_one }}</a>
                        </div>
                        <article class="info">
                            {!! $section_data->description_four !!}
                        </article>
                    </section>

                    <section class="new-contect-sec">
                        <div class="top-sec">
                            <aside class="paly-ic"><img src="img/play-ic.png" alt=""></aside>
                            <a href="{{ $section_data->video_title_two_link }}">{{ $section_data->video_title_two }} </a>
                        </div>
                        <article class="info">
                             {!! $section_data->description_three !!}
                        </article>
                    </section>

                </div>

                <div class="col-md-6">
                    <div class="htBoxIn">
                        <div class="htImg"><img src=" {{ asset('storage/'.$section_data->image) }}" alt="img"></div>
                        <div class="htCont">
                            <div class="htCont__in">
                               {!! $section_data->description_five !!}
                            </div>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
    <!-- References Section -->
    <section class="refrences_container margin-top1 margin-bottom1">
        <div class="refrences_sec">
            <h5>References </h5>
        </div>
        <div class="slider ">
            <div class="container">
                {!! $section_data->ref !!}
            </div>
        </div>
    </section>
    <!-- References Section Over-->

@endsection

@section('js')
<script type="text/javascript">
        var swiper = new Swiper(".swiper-container", {
            spaceBetween: 30,
            effect: "fade",
            autoplay: true,
            fadeEffect: {
                crossFade: true
            },
            speed: 3000,
        });
    </script>
@endsection
