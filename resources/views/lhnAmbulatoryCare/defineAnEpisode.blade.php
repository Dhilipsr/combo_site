@extends('layouts.layout')
@section('seo')
<title>LHN Ambulatory Care - {{$define_episode->meta_title}}</title>
<meta name="description" content="{{$define_episode->meta_description}}">
<meta name="keywords" content="{{$define_episode->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
 <!-- Main Section -->
    <div class="main_section">
        <div class="container">
            <div class="main-title section">
                <h2 class="title">{{ $define_episode->title }}</h2>
            </div>
            <div class="text-container">
                {!! $define_episode->content !!}
            </div>
            <div class="blue-box">
                <div class="box-content">
                    {!! $define_episode->content_two !!}
                </div>
            </div>


            <hr>
            <div class="text-container">
                 {!! $define_episode->content_three !!}
                 @if(Auth::check())
                    <a class="blueBox--link button" href="{{route('healthtools',['section' => '1'])}}">ACCESS TOOLS </a>
                  @else
                    <p>
                        <a class="blueBox--link button" href="{{route('register')}}">REGISTER </a> to get access to the
                        <a class="plain-link" href="{{route('healthtools',['section' => '1'])}}">health tools</a> below.
                    </p>
                  @endif
            </div>

            <div class="yellow-head">
                <h4>Provider Resources</h4>
            </div>
            <div class="list-one">
                {!! $define_episode->provider_resource_content !!}
            </div>

            <div class="yellow-head">
                <h4>Patient Resources</h4>
            </div>
            <div class="list-one">
                 {!! $define_episode->patient_resource_content !!}
            </div>


        </div>
    </div>
    <!-- Main Section Over-->
    <!-- References Section -->
    <section class="refrences_container margin-top1 margin-bottom1">
        <div class="refrences_sec">
            <h5>References </h5>
        </div>
        <div class="slider inside-page-slider">
            <div class="container">
                {!! $define_episode->ref !!}
            </div>
        </div>
    </section>
    <!-- References Section Over-->
@endsection

@section('js')
@endsection