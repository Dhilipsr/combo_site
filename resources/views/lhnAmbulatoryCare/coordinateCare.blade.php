@extends('layouts.layout')
@section('seo')
<title>LHN Ambulatory Care - {{$coordinate_care->meta_title}}</title>
<meta name="description" content="{{$coordinate_care->meta_description}}">
<meta name="keywords" content="{{$coordinate_care->keywords}}">
@endsection

@section('css')
@endsection

@section('content')
    <!-- Main Section -->
    <div class="main_section">
        <div class="container">
            <div class="main-title section">
                <h2 class="title">{{ $coordinate_care->title }}</h2>
            </div>
            <div class="blue-box">
                <div class="box-content">
                    {!! $coordinate_care->description_one !!}

                </div>
            </div>

            <div class="text-container">
                {!! $coordinate_care->description_two !!}
            </div>
            <hr>
            <div class="text-container">
               {!! $coordinate_care->description_three !!}
               @if(Auth::check())
                <a class="blueBox--link button" href="{{route('healthtools',['section' => '1'])}}">ACCESS TOOLS </a>
                @else
                <p>
                    <a class="blueBox--link button" href="{{route('register')}}">REGISTER </a> to get access to the
                    <a class="plain-link" href="{{route('healthtools',['section' => '1'])}}">health tools</a> below.
                </p>
                @endif
            </div>

            <div class="yellow-head">
                <h4>{{ $coordinate_care->tool_one_title }}</h4>
            </div>
            <div class="list-one">
                {!! $coordinate_care->tool_one_description !!}
            </div>

            <div class="yellow-head">
                <h4>{{ $coordinate_care->tool_two_title }}</h4>
            </div>
            <div class="list-one">
                 {!! $coordinate_care->tool_two_description !!}
            </div>


        </div>
    </div>
    <!-- Main Section Over-->
    <!-- References Section -->
    <section class="refrences_container margin-top1 margin-bottom1">
        <div class="refrences_sec">
            <h5>References </h5>
        </div>
        <div class="slider">
            <div class="container">
                 {!! $coordinate_care->ref !!}
            </div>
        </div>
    </section>
    <!-- References Section Over-->
@endsection

@section('js')
@endsection