@section('seo')
<title>LHN/ACE - Thank you</title>
<meta name="description" content="Thank you">
<meta name="keywords" content="Thank you">
@endsection
@extends('layouts.layout')
@section('content')
<!-- Start Here -->
<div class="banner-container">
    <div class="banner-container__top  plain-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title">Registration </h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact-msg">
    <div class="default-box ">
        <h4>Thank you for applying to become a member!</h4>
        <p class="small-box">You should expect an email within the next 48 hours with information about how to access the health tools and resources on the site.</p>
    </div>
</div>
{{-- @include('partials.agreement') --}}
{{-- Ends Here --}}
@endsection
