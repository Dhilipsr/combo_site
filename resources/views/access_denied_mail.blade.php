<html>
    <head></head>
    <body>
<table width="650" style="margin: auto;">
        <tr>
            <td style="font:  15px/1.5 arial;">
                <p>After review of your registration form, you unfortunately do not meet the criteria for access to LiverHealthNow or
AccessClickEngage. If you have questions, please contact us at <a href="mailto:support@liverhealthnow.com"
                        target="_blank" style="color: #2B79AC;">support@liverhealthnow.com</a>.</p>


            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="20px">

            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">
                 <p>
                     Content contained in <a href="https://liverhealthnow.com/all" target="_blank" style="color: #2B79AC;">liverhealthnow.com/all</a> is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.
                </p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="100px">

            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="25px">
                <p>
                    Please do not reply to this email. If you have questions related to these resources,
                    contact the support team at:
                    <a href="mailto:support@liverhealthnow.com" target="_blank" style="color: #2B79AC;">support@liverhealthnow.com</a>
                </p>
            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="15px">

            </td>
        </tr>
        <tr>
            <td style="font:300  14px/1.5 arial;">
                <p><span><a
                        href="https://www.bauschhealth.com/privacy" target="_blank" style="color: #2B79AC;">PRIVACY POLICY</a></span> | <span
                        ><a
                        href="https://www.bauschhealth.com/terms" target="_blank" style="color: #2B79AC;">LEGAL NOTICE</a></span></p>


            </td>
        </tr>
        <tr>
            <td style="font:300 14px/1.5 arial" height="15px">

            </td>
        </tr>
        <tr>
            <td style="font: 12px/3 arial; ">
                <address style="font-style: normal;">Salix Pharmaceuticals: 400 Somerset Corporate Blvd.,
                    Bridgewater, NJ 08807.</address>
                <address style="font-style: normal;">© 2021 Salix Pharmaceuticals or its affiliates. HED.0124.USA.21</address>
            </td>
        </tr>
    </table>


    </body>
</html>