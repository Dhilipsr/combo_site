@extends('layouts.layout')
@section('seo')
<title>LHN/ACE - My Account</title>
<meta name="description" content="My Account">
<meta name="keywords" content="My Account">
@endsection

@section('css')
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="mainContainer">
    <!-- Start Here -->
    <div class="banner-container">
        <div class="banner-container__top  plain-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title"> My Account </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="login-box section">
        <div class="container pad">
            <div class="upload-logo">
                <div class="upload-logo__img-box">
                    <div class="upload-logo__img">
                        @if( empty(Auth::user()->avatar) )
                          <img src="{{asset('storage/users/default.png')}}">
                        @else
                            <img src="{{asset('storage/'.Auth::user()->avatar)}}">
                        @endif
                    </div>
                    <!-- <a href="#">Edit Your Logo</a> -->
                    <input type="file" name="file" id="file" accept="image/*">
                    <label for="file" class="edit-logo">Edit Your Logo</label>
                </div>
                <div class="upload-logo__content">
                    <h2>Update Profile</h2>
                    <p>Complete your profile and upload your organization’s logo.</p>
                </div>
            </div>
        </div>
        <div class="container pad">
            <div class="row">
                <div class="col-md-12">
                    <p class="ind-txt float-right"><span>*</span> Required field</p>
                </div>
            </div>
            <form method="post" action="{{route('myaccount.update')}}" enctype="multipart/form-data">
                <div class="row">
                    @csrf
                    <div class="col-md-6 form-box @error('name') error-box @enderror ">

                        <label class="req">First Name</label>
                        <input type="text" class="form-field" value="{{Auth::user()->name}} " name="name" required>

                        <div class="error">
                            @error('name'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 form-box @error('lname') error-box @enderror">

                        <label class="req">Last Name</label>
                        <input type="text" class="form-field" value="{{Auth::user()->lname}}" name="lname" required>

                        <div class="error">


                            @error('lname'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 form-box  @error('organization') error-box @enderror">

                        <label class="req">Organization</label>
                        <input type="text" class="form-field" value="{{Auth::user()->organization}}" name="organization" required>

                        <div class="error">

                            @error('organization'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 form-box ">
                        <label class="req">Upload</label>
                        <div class="upload-form-sec">
                            <div class="upload-form-sec__1">

                               <div class="file-upload-wrapper" data-text=" Organization’s logo">
                                        <input name="avatar" type="file" class="file-upload-field" value=""  id="uplImg" onchange="getFileData(this);">
                               </div>


                             {{--    <p>Organization’s logo</p>
                                <input type="file" name="avatar" accept="image/*" id="uplImg"
                                    onchange="getFileData(this);">
                                <label for="uplImg">Choose File</label> --}}
                            </div>
                            <div class="upload-form-sec__2">
                                <p class="upload-form-sec__2__name">Organization’s logo</p>
                                <span class="upload-form-sec__2__remove" onclick="fileRemove();">Remove</span>
                            </div>
                        </div>
                        <div class="error">
                            @error('avatar'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 form-box @error('email') error-box @enderror">

                        <label class="req">Enter Email Address</label>
                        <input type="email" class="form-field" value="{{Auth::user()->email}}" name="email" required>

                        <div class="error">
                            @error('email'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 form-box @error('contact') error-box @enderror">

                        <label>Phone Number</label>
                        <input type="number" class="form-field" value="{{Auth::user()->contact}}" name="contact">

                        <div class="error">

                            @error('contact'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <div class="check-box">
                            <input type="checkbox" name="update" id="chk1"
                                <?php if( Auth::user()->update == 1 ){ echo "checked='checked'"; } ?>
                            >


                            <label for="chk1">I agree to receive periodic updates and communications via email from
                                this website.</label>
                        </div>
                    </div>
                    <div class="col-md-12 form-box w-auto mb-0 ml-auto mr-auto">
                        <button class="button d-blu">Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="change-password-box">
        <div class="default-box">
            <div class="default-box">
                <h4>Change Password </h4>
            </div>
        </div>
        <div class="container pad">
            <div class="row">
                <div class="col-md-12">
                    <p class="ind-txt float-right"><span>*</span> Required field</p>
                </div>
            </div>
            <form method="POST" action="{{route('password.change')}}">
                <div class="row">
                    @csrf
                    <div class="col-md-6 form-box @error('old_password') error-box @enderror">
                        <label class="req">Old Password</label>
                        <input type="password" class="form-field" name="old_password" id="old_password">
                        <div class="error">

                            @error('old_password'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 form-box @error('new_password') error-box @enderror">
                        <label class="req">New Password</label>
                        <input type="password" class="form-field" name="new_password" disabled="disabled"
                            id="new_password">
                        <div class="error">


                            @error('new_password'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 form-box @error('password_confirmation') error-box @enderror" id="c_password">
                        <label class="req">Confirm New Password</label>
                        <input type="password" class="form-field" name="password_confirmation" disabled="disabled"
                            id="confirm_password">
                        <div class="error" id="c_password_error_msg">
                            @error('password_confirmation'){{$message}}
                                <img src="{{asset('img/error-arrow.png')}}" alt="error" />
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12 form-box w-auto mb-0 ml-auto mr-auto">
                        <button id="update_password" disabled="disabled" class="button d-blu">Update
                            Password</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <section class="container section">

        <div class="refrences_sec favorites pad">
            <h5><i class="icon-img"><img src="img/heart-2.svg"></i>My Favorites </h5>
            <p>Save your favorite customized health tools here for easy access in the future. You can also share all
                your favorites with colleagues. </p>
        </div>
        <div class="slider favorites pad">
            <div class="container">
                <ol>
                    @foreach( $favTools as $favTool )

                  @if($favTool->new_link != 1)
                        @php
                            $tool = App\Healthtool::where(['id' => $favTool->healthtool_id])->first();
                            // dd($tool);
                            if ($tool) {
                            $link = Str::slug($tool->title, '-');
                            }
                            $client_name = Str::slug(Auth::user()->organization, '-');
                        @endphp
                        @if($tool)
                        <li>
                            {{-- <a href="{{route('healthToolsDetail',['link' => $link,'client_name' => $client_name,'healthtool_id' => $favTool->healthtool_slug,'image' => $favTool->id])}}" target="_blank"> --}}
                            <a href="{{$favTool->link}}" target="_blank">
                            {{$tool->title}}</a><span class="delete deleteFavTool" data-fav-id = '{{$favTool->id}}'><img src="{{asset('img/delete.svg')}}"></span>
                        </li>
                        @endif
                    @else
                        <li> <a href="{{ $favTool->link }}" target="_blank">
                            {{optional($favTool->healthtool)->title}}</a><span class="delete deleteFavTool" data-fav-id = '{{$favTool->id}}'><img src="{{asset('img/delete.svg')}}"></span>
                        </li>
                    @endif
                @endforeach
                </ol>


            </div>
            <div class="col-md-12 w-auto shareBtn">
                <a href="{{route('share-fav-tool',['user_id' => Auth::user()->id])}}" target="_blank" class="button d-blu mb-5 ">Share My Favorites</a>
            </div>
        </div>

    </section>
    <!-- Main Section Over-->
</div>
{{-- success --}}
@if(Session::has('alert-type'))
  @if(Session::get('alert-type') == 'success')
    <div class="custalert alert alert-success alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{Session::get('message')}}</strong>
    </div>
  @elseif(Session::get('alert-type') == 'error')
    <div class="custalert alert alert-danger alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>{{Session::get('message')}}</strong>
    </div>
  @endif
@endif
{{-- success --}}
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function () {




    $('#old_password').keyup(function () {
        var old_password = $(this).val();
        // ajax call
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{route('password.check')}}",
            type: "post",
            data: {
                'old_password': old_password
            },
            success: function (response) {
                var status = response.status;

                if (status == 1) {
                    $("#new_password").removeAttr("disabled");
                    $("#confirm_password").removeAttr("disabled");
                } else if (status == 0) {
                    $("#new_password").attr("disabled", true);
                    $("#confirm_password").attr("disabled", true);
                }
            },
            error: function (jqXHR, exception) {
                console.log(exception);
            }
        });
    });
    $('#confirm_password').keyup(function () {
        var new_password = $('#new_password').val();
        if ($(this).val() != new_password) {
            $('#c_password').addClass('error-box');
            $('#c_password_error_msg').text('The passwords do not match.');
            $('#update_password').attr('disabled', true);
        } else {
            $("#update_password").removeAttr("disabled");
            $('#c_password').removeClass('error-box');
            $('#c_password_error_msg').text(' ');
        }
    });
});

// ajax image upload
$(document).on('change', '#file', function () {
    console.log('Uploading')
    var name = document.getElementById("file").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        alert("Invalid Image File");
    }
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("file").files[0]);
    var f = document.getElementById("file").files[0];
    var fsize = f.size || f.fileSize;
    if (fsize > 2000000) {
        alert("Image File Size is very big");
    } else {
        form_data.append("file", document.getElementById('file').files[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{route('profilepicture.upload')}}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                // console.log(data.message);
                if (data.message == 1) {
                    // image changed
                    location.reload();
                }
            }
        });
    }
});
$(document).on('click', '.deleteFavTool', function(e){
      var id = $(this).attr('data-fav-id');
      var elem = this;
      console.log(id);
      $.ajax({
       url:"{{route('deleteFromFav')}}",
       method:"get",
       data: {'id' : id},
       success:function(data)
       {
        // console.log(data.message);
        if (data.status == 200) {
          let li = elem.parentNode;
          li.parentNode.removeChild(li);
        }
       }
      });
    });
// ajax image upload ends
</script>
@endsection