<?php

use Illuminate\Support\Facades\Route;
use App\Htt\Controllers\PagesController;


Route::get('HS/ehr-plugin','PagesController@helthSytemEhrPlugin')->name('helthSytemEhrPlugin');
$urll  = @$_SERVER['HTTP_HOST'];
// print_r($urll);
// exit;
if($urll == "liverhealthnow.com" || $urll == "www.liverhealthnow.com"){
Route::get('all','PagesController@home')->name('landing');    
Route::get('/','PagesController@ambulatoryCare')->name('ambulatoryCare');
Route::group(['middleware' => ['checkuserstatus','auth']], function () {
// Post Login Ace Pages
Route::post('store-plugin-order','PagesController@storePluginOrder')->name('storePluginOrder');
// Long Term;
Route::get('PC/raise-the-bar','PagesController@raiseBar')->name('raiseBar');
});
Route::get('PC/wrestling-the-monster','PagesController@wrestlingTheMonster')->name('wrestlingTheMonster');
Route::get('PC','PagesController@primaryCare')->name('primaryCare');

// Landing pages starts
Route::get('AC','PagesController@ambulatoryCare')->name('ambulatoryCare');
Route::get('PC','PagesController@primaryCare')->name('primaryCare');
//Route::get('HS','PagesController@healthSystem')->name('healthSystem');
//Route::get('LTC','PagesController@longTermCare')->name('longTermCare');
// Landing pages ends


// LHN starts here
// Ambulatory Care
Route::get('AC/screen-patients','PagesController@screenPatients')->name('screenPatients');
Route::get('AC/define-an-episode','PagesController@defineAnEpisode')->name('defineAnEpisode');
Route::get('AC/coordinate-care','PagesController@coordinateCare')->name('coordinateCare');
Route::get('PC/wrestling-the-monster','PagesController@wrestlingTheMonster')->name('wrestlingTheMonster');

// LHN ends here

// common pages starts
Route::get('under-review','PagesController@underReview')->name('underReview');
// common pages ends


Route::get('/live_search', 'AjaxSearchController@index');
Route::get('/live_search/action', 'AjaxSearchController@action')->name('live_search.action');
Route::get('myfavtool/{user_id}', 'PagesController@shareMyfavorites')->name('share-fav-tool');

Route::group(['middleware' => ['checkuserstatus','auth']], function () {

Route::get('home', 'PagesController@healthtools')->name('home');
Route::get('healthtools','PagesController@viewHealthTools')->name('healthtools');
Route::get('health-tools','PagesController@viewHealthTools')->name('healthtools');
Route::get('search-results','PagesController@results')->name('results');
Route::get('health-tools/results', 'PagesController@healthtoolfilter')->name('healthtoolfilter');
Route::get('health-tools/results/search', 'PagesController@searchhealthtool')->name('searchhealthtool');
Route::get('guidelines','PagesController@longTermGuidelines')->name('longTermGuidelines');


// customization of healthtools starts
Route::get('/customize-your-tool/{healthtool_id}','PagesController@customizeTool')->name('customizeHealthTools');
Route::post('/no-logo-upload','PagesController@noLogoUpload')->name('noLogoUpload');
Route::post('logo-upload','PagesController@logoUpload')->name('logo.upload');
Route::post('logo-upload-two','PagesController@logoUploadTwo')->name('logo.upload.two');
Route::post('post-to-fav','PagesController@postToFav')->name('postToFav');
Route::get('delete-from-fav','PagesController@deleteFromFav')->name('deleteFromFav');
// customization of healthtools ends

Route::get('my-account', 'PagesController@myaccount')->name('myaccount');
Route::post('my-account/update', 'PagesController@myaccount_update')->name('myaccount.update');
Route::post('check-old-password','PagesController@checkpassword')->name('password.check');
Route::post('change-password','PagesController@changepassword')->name('password.change');
Route::post('upload-profile-picture','PagesController@profilepicture')->name('profilepicture.upload');

// Primary Care
Route::get('PC/raise-the-bar','PagesController@raiseBar')->name('raiseBar');

});
Route::get('/dumm1','PagesController@dummy2')->name('longTermCare');
Route::get('/dumm2','PagesController@dummy2')->name('healthSystem');
}else{
Route::get('/','PagesController@healthSystem')->name('healthSystem');
Route::get('under-review','PagesController@underReview')->name('underReview');
Route::get('HS','PagesController@healthSystem')->name('healthSystem');
Route::group(['middleware' => ['checkuserstatus','auth']], function () {
Route::get('LTC/care-pathways','PagesController@longTermCarePathWays')->name('longTermCarePathWays');
Route::get('LTC/ehr-plugin','PagesController@longTermEhrPlugin')->name('longTermEhrPlugin');
Route::get('HS/care-pathways','PagesController@healthSystemCarePathWays')->name('healthSystemCarePathWays');
Route::get('HS/ehr-plugin','PagesController@helthSytemEhrPlugin')->name('helthSytemEhrPlugin');
Route::get('/customize-your-tool/{healthtool_id}','PagesController@customizeTool')->name('customizeHealthTools');
});
if($urll == "accessclickengage.liverhealthnow.com" || $urll == "www.accessclickengage.liverhealthnow.com")
{
Route::get('/LTC','PagesController@longTermCare')->name('longTermCare');
}
Route::get('/sd','PagesController@sd')->name('healthSystemlthEhrPlugin');
Route::get('/dummy1','PagesController@dummy2')->name('longTermGuidelines');
Route::get('/dummy2','PagesController@dummy2')->name('healthtools');
Route::get('/dummy3','PagesController@dummy3')->name('longTermTrendReport');
Route::get('/live_search/action','AjaxSearchController@action')->name('live_search.action');
Route::get('/dummy5','PagesController@dummy3')->name('myaccount');
Route::post('store-plugin-order','PagesController@storePluginOrder')->name('storePluginOrder');
}


Route::get('trends-report','PagesController@longTermTrendReport')->name('longTermTrendReport');
Route::get('trends-report','PagesController@longTermTrendReport')->name('longTermTrendReport');
Route::get('trend-report-detail','PagesController@trendReportDetail')->name('trendReportDetail');

Auth::routes();
Route::get('/register/thank-you', 'PagesController@thankyou')->name('thankyou');

Route::get('/get-org-types', 'PagesController@getOrgTypes')->name('getOrgTypes');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'api'], function()
{
    Route::get('/get-healthtools/','PagesController@getTools');
    Route::get('/get-user','PagesController@getUser');
    Route::get('/user-plugin-orders','PagesController@userPluginOrders');
    Route::get('/manager-plugin-orders','PagesController@managerPluginOrders');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/duplicate-healthtool/{id}','CustomHealthtoolController@duplicateHealtool')->name('duplicate.health_tool');
});