<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthToolsFav extends Model
{
    protected $fillable = ['user_id','healthtool_id','link','is_fav'];

    public function healthtool()
    {
        return $this->belongsTo('App\Healthtool');
    }
}
