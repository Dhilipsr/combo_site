<?php

namespace App\Http\Resources;

use App\State;
use App\Models\User;
use App\Organizationtype;
use Illuminate\Http\Resources\Json\JsonResource;

class PluginOrderCollection extends JsonResource
{
    public function toArray($request)
    {
        $user = User::where('email',$this->email)->first();
        $orgType = null;
        $state = null;
        $plugins = null;
        if($this->plugin_type == 1){
            $plugins = 'Ambulatory Care';
        }
        if($this->plugin_type == 2){
            $plugins = 'Long Term Care';
        }
        if($this->plugin_type == 3){
            $plugins = 'All';
        }

        if($user){
            // get Org Type
            $orgType = Organizationtype::where('id',$user->organization_id)->first();
            // get State
            $state = State::where('id',$user->state_id)->first();
        }

        return [
            'timestamp' => $this->created_at,
            'user_id' =>  $this->user_id,
            'name' => $user ? $user->name : null,
            'last_name' => $user ? $user->lname : null,
            'email' => $user ? $user->email : null,
            'npi_number' => $user ? $user->npi_number : null,
            'job_title' => $user ? $user->job_title : null,
            'organization_name' => $user ? $user->organization : null,
            'city' =>  $this->organization_city,
            'state' => $this->organization_state,
            'custom_logo' => $this->custom_log == 1 ? 'Yes' : 'No',
            'plugins' => $plugins,
            'salix_manager_name' => $user ? $user->salix_manager_name : null,
            'org_type' => $orgType ? $orgType->type : null,
            'reg_city' => $user ? $user->city : null,
            'reg_state' => $state ? $state->state : null,
        ];
    }
}
