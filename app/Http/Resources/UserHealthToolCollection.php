<?php

namespace App\Http\Resources;

use App\Role;
use App\State;
use App\Source;
use App\Organizationtype;
use Illuminate\Http\Resources\Json\JsonResource;

class UserHealthToolCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $role = Role::where('id',$this->role_id)->first();
        $organization = Organizationtype::where('id',$this->organization_id)->first();
        $state = State::where('id',$this->state_id)->first();
        $source = Source::where('id',$this->source_id)->first();
        return
        [   
            'user_id' =>  $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'last_login' => $this->last_login,
            'last_login_timezone' => $this->last_login_timezone,
            'timezone' => $this->timezone,
            'role_id' => $role != '' ? $role->display_name : '',
            'avatar' => asset('storage/.'.$this->avatar),
            'name' => $this->name,
            'lname' => $this->lname,
            'email' => $this->email,
            'npi_number' => $this->npi_number,
            'job_title' => $this->job_title,
            'organization' => $this->organization,
            'organization_type' => $organization != '' ? $organization->type : '' ,
            'city' => $this->city,
            'state' => $state ? $state->state : null,
            'source' => $source ? $source->sources : null,
            'salix_manager_name' => $this->salix_manager_name,
            'update' => $this->update == 1 ? 'Yes' : 'No',
            'agreement_accepted' => $this->agreement_check == 1 ? 'Yes' : 'No',
            'code' => $this->code
        ];
    }
}
