<?php

namespace App\Http\Resources;
use App\State;
use App\Models\User;
use App\Organizationtype;
use Illuminate\Http\Resources\Json\JsonResource;

class ManagerPluginOrderCollection extends JsonResource
{
    public function toArray($request)
    {
        $user = User::where('email',$this->email)->first();
        $manager = User::where('email',$this->manager_email)->first();

        $plugins = null;
        if($this->plugin_type == 1){
            $plugins = 'Ambulatory Care';
        }
        if($this->plugin_type == 2){
            $plugins = 'Long Term Care';
        }
        if($this->plugin_type == 3){
            $plugins = 'All';
        }

        return [
            'timestamp' => $this->created_at,
            'user_id' =>  $this->user_id,
            'customer_name' => $this->name,
            'email' => $this->email,
            'customer_professional_title' => $user ? $user->job_title : null,
            'customer_organization_name' => $user ? $user->organization : null,
            'city' =>  $this->organization_city,
            'state' => $this->organization_state,
            'custom_logo' => $this->custom_log == 1 ? 'Yes' : 'No',
            'manager_first_name' => $manager ? $manager->name : null,
            'manager_last_name' => $manager ? $manager->lname : null,
            'manager_email' => $manager ? $manager->email : null,
            'plugins' => $plugins,
        ];
    }
}
