<?php

namespace App\Http\Controllers;

use Hash;
use Mail;
use Session;
use App\State;
use Validator;
use App\AcePopup;

use Carbon\Carbon;
use App\AcePathway;
use App\Healthtool;
use App\MainLanding;
use App\Models\User;

use App\Patienttype;
use App\PluginOrder;

use App\AceGuideline;
use App\AceCarePathway;
use App\HealthToolsFav;

use App\Targetaudience;
use App\AceHealthSystem;

use Carbon\CarbonPeriod;
use App\AceEhrPluginsPage;
use App\AceLongTermLanding;
use App\LhnPrimcareWresMon;
use Illuminate\Support\Str;
use App\LhnAmbScreenPatient;
use Illuminate\Http\Request;
use App\LhnAnnualTrendReport;
use App\LhnPrimaryCareBanner;
use App\AceLongTermsEhrPlugin;
use App\LhnPrimaryCareLanding;
use App\AceLongTermCarePathway;
use App\LhnAmbulatoryCoordCare;
use App\LhnPrimarycareRaisebar;

use Illuminate\Validation\Rule;
use App\AceLongTermCareDPathway;
use App\LhnAmbulatorycareBanner;
use App\AceHealthSystemEhrPlugin;
use App\LhnAmbulatoryCaresLanding;

use Illuminate\Support\Facades\DB;
use App\LhnAmbulatoryDefineEpisode;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

use App\Http\Resources\PluginOrderCollection;
use App\Http\Resources\UserHealthToolCollection;
use App\Http\Resources\ManagerPluginOrderCollection;

class PagesController extends Controller
{
    private $prettyLinkUrl;
    
    protected function setPrettyLinkUrl(string $link)
    {
        $prettyLinkUrl = $link;
    }

    protected function getPrettyLinkUrl(string $file,int $id)
    {
        $organization = Auth::user()->organization;

        // "PageTitle" => $healthtool->title,
        $curl = curl_init();
        $healthtool = Healthtool::find($id);
        // return $healthtool;
        $post = array(
            "PageUrl" => $healthtool->api_id,
            "OrganizationName" => $organization,
            "Image" => array('$content-type'=> "image/jpeg",
            '$content'=> $file
        ));
                        // return $file;
        curl_setopt_array($curl, array(
                                CURLOPT_URL => 'https://cmsautomate.azure-api.net/cmsautomate/getprettylink',
                                // CURLOPT_URL => 'https://cmsautomate.azure-api.net/cmsautomate/getprettylink',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => '',
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => 'POST',
                                CURLOPT_POSTFIELDS =>json_encode($post),
                                CURLOPT_HTTPHEADER => array(
                                    'Ocp-Apim-Subscription-Key: d186020dc07642acb4fc5493d05e4acb',
                                    'Content-Type: application/json'
                                ),
                                ));

        $response = curl_exec($curl);
        curl_close($curl);
        // return $response;
        $url = json_decode($response)->PrettyLinkUrl ;
        $this->setPrettyLinkUrl($url);

        return $url ;
    }

    public function home()
    {
        Session::put('page', str_replace(url('/'), '', url()->current()));
        $content = MainLanding::first();

        return view('welcome')->with(compact('content'));
    }

    public function register()
    {
        return view('auth.register');
    }

    public function ambulatoryCare()
    {
        Session::put('page', str_replace(url('/'), '', url()->current()));

        $lhn_primary_carebanner =LhnPrimaryCareBanner::get();
        $section_data = LhnAmbulatoryCaresLanding::first();
        return view('landing_pages.lhnAmbulatoryCare')->with(compact('lhn_primary_carebanner','section_data'));
    }

    public function primaryCare()
    {
        Session::put('page', str_replace(url('/'), '', url()->current()));
        $lhnAmbulatorycare_banner = LhnAmbulatorycareBanner::get();
        $section_data = LhnPrimaryCareLanding::first();
        return view('landing_pages.lhnPrimaryCare')->with(compact('lhnAmbulatorycare_banner','section_data'));
    }

    public function healthSystem()
    {
        Session::put('page', str_replace(url('/'), '', url()->current()));
        $health_system =AceHealthSystem::first();
        return view('landing_pages.aceHealthSystem')->with(compact('health_system'));
    }

    public function longTermCare()
    {
        Session::put('page', str_replace(url('/'), '', url()->current()));
        $ace_long_termcare = AceLongTermLanding::first();
        return view('landing_pages.aceLongTermCare')->with(compact('ace_long_termcare'));
    }

    public function longTermCarePathWays()
    {
        $care_pathway = AceLongTermCarePathway::first();
        $pathways = AceLongTermCareDPathway::get();
        return view('aceLongTermCare.carePathways')->with(compact('care_pathway','pathways'));
    }

    public function longTermEhrPlugin()
    {

        $popup_content = AcePopup::first();
        $long_term_ehrplugin = AceLongTermsEhrPlugin::first();
        return view('aceLongTermCare.ehrPlugin')->with(compact('long_term_ehrplugin','popup_content'));
    }

    public function healthSystemCarePathWays()
    {
        $care_pathway = AcePathway::first();
        $ace_care_pathways = AceCarePathway::get();
        return view('aceHealthSystem.carePathways')->with(compact(['care_pathway','ace_care_pathways']));
    }
    public function helthSytemEhrPlugin()
    {
        $popup_content = AcePopup::first();
        $health_system_ehrplugin = AceHealthSystemEhrPlugin::first();
        return view('aceHealthSystem.ehrPlugin')->with(compact('health_system_ehrplugin','popup_content'));
    }

    public function raiseBar()
    {
        $raise_bar = LhnPrimarycareRaisebar::first();
        return view('lhnPrimaryCare.raiseBar')->with(compact('raise_bar'));
    }

    public function wrestlingTheMonster()
    {
        $raise_bar = LhnPrimcareWresMon::first();
        return view('lhnPrimaryCare.wrestlingTheMonster')->with(compact('raise_bar'));
    }

    public function screenPatients()
    {
        $screen_patients = LhnAmbScreenPatient::first();

        return view('lhnAmbulatoryCare.screenPaitients')->with(compact('screen_patients'));
    }

    public function defineAnEpisode()
    {
        $define_episode = LhnAmbulatoryDefineEpisode::first();
        return view('lhnAmbulatoryCare.defineAnEpisode')->with(compact('define_episode'));
    }

    public function coordinateCare()
    {
        $coordinate_care = LhnAmbulatoryCoordCare::first();
        return view('lhnAmbulatoryCare.coordinateCare')->with(compact('coordinate_care'));
    }

    public function longTermGuidelines()
    {
        $guideline = AceGuideline::first();
        return view('guidelines')->with(compact('guideline'));
    }

    public function longTermTrendReport()
    {
        $report_data = LhnAnnualTrendReport::first();
        return view('trendReport')->with(compact('report_data'));
    }

    public function trendReportDetail(Request $request)
    {
        return view('trendReportDetail');
    }

    public function thankyou()
    {
        if (Auth::check()) {
            if (Auth::user()->status == 0 || Auth::user()->status == NULL) {
                return view('post_register_thankyou');
            }else{
                return redirect(route('healthtools'));
            }

        }else{
            return redirect(route('login'));
        }
    }

    public function underReview()
    {
        Auth::logout();
        return redirect()->route('login')->with('under_review','Under Review');
    }

    public function healthtools ()
    {
        return redirect()->back();
    }

    public function viewHealthTools(Request $request)
    {

        $healthtools = Healthtool::where(['status' => 1])->where('has_spanish', 0)
                        ->whereJsonContains('category',$request->section)
                        ->orderBy('title','ASC')->paginate(6);
        $healthtoolsCount = Healthtool::where(['status' => 1])->where('has_spanish', 0)
                        ->whereJsonContains('category',$request->section)
                        ->orderBy('title','ASC')->count();

        $targetaudiences = Targetaudience::where(['status' => 1])
                            ->whereJsonContains('sections',$request->section)
                            ->get();

        $patienttypes = Patienttype::where(['status' => 1])
                        ->whereJsonContains('sections',$request->section)
                        ->get();
        $section = $request->section;
        return view('healthtools')->with(compact([
            'healthtools',
            'healthtoolsCount',
            'targetaudiences',
            'patienttypes',
            'section'
        ]));
    }

    public function healthtoolfilter(Request $request)
    {
        // $healthtool_page_contents = Healthtoolstaticcontent::get();

        $section = $request->get('section');
        $lang = $request->get('lang');
        $search = $request->get('search');

        $targetaudiences = Targetaudience::where(['status' => 1])
                            ->whereJsonContains('sections',$section)
                            ->get();
        $targetaudiences_array = $request->get('targetaudience');

        $patienttypes = Patienttype::where(['status' => 1])
                        ->whereJsonContains('sections',$section)
                        ->get();
        $patienttypes_array = $request->get('patienttype');
        // return $patienttypes_array;

        $query = DB::table('healthtools')->where(['status' => 1])->orderBy('title','ASC')

        ->when(!empty($request->get('section')), function($q) use ($section) {
            return $q->whereJsonContains('category', $section);
        })

        ->when(!empty($request->get('search')), function($q) use ($search) {
            return $q->where('title','like', '%'.$search.'%');
        })

        ->when(empty($request->get('lang')), function($q) use ($lang) {
            return $q->where('has_spanish', 0);
        })
        ->when(!empty($request->get('lang')), function($q) use ($lang) {
            if($lang == 2){
                return $q->where('has_spanish', 1);
            }else{
                return $q->where('has_spanish', 0);
            }
        })

        ->when(!empty($request->get('targetaudience')), function($q) use ($targetaudiences_array) {
            $healthtool_targetaudiences_ids = DB::table('healthtool_targetaudiences')
                                        ->whereIn('targetaudience_id',$targetaudiences_array)
                                        ->distinct('healthtool_id')
                                        ->pluck('healthtool_id');
            if($healthtool_targetaudiences_ids ){
                return $q->whereIn('id', $healthtool_targetaudiences_ids);
            }
        })

        ->when(!empty($request->get('patienttype')), function($q) use ($patienttypes_array) {
            $healthtool_paitienttype_ids = DB::table('healthtool_paitienttype')
                                        ->whereIn('patienttype_id',$patienttypes_array)
                                        ->distinct('healthtool_id')
                                        ->pluck('healthtool_id');
            if($healthtool_paitienttype_ids ){
                return $q->whereIn('id', $healthtool_paitienttype_ids);
            }
        });
        $healthtoolsCount  = $query->get()->count();
        $healthtools =  $query->paginate(6);

        return view('result')->with(compact([
                'healthtoolsCount',
                'healthtools',
                'targetaudiences',
                'patienttypes',
                'section',
                'targetaudiences_array',
                'patienttypes_array',
                'search'
                // 'healthtool_page_contents',
            ]));
    }

    public function customizeTool($healthtool_id)
    {
        $previousUrl = URL::previous();
        $healthTool = Healthtool::where(['id' => $healthtool_id])->first();
        $link = Str::slug($healthTool->title, '-');
        $client_name = Str::slug(Auth::user()->organization, '-');
        return view('customizeTool')->with(compact([
            'healthTool',
            'previousUrl',
            'link',
            'client_name'
        ]));
    }

    public function noLogoUpload(Request $request)
    {
        $tool_id = $request->page_id;
        $file = '';
        $url = $this->getPrettyLinkUrl($file,$tool_id) ;
        echo $url;
    }

    public function logoUpload(Request $request)
    {
        $fileUpload_arr = explode('base64,', $request->file);
        $hashed = $fileUpload_arr[1];
        $healthtool_id = $request->healthtool_id;
        $link = $this->getPrettyLinkUrl($hashed,$healthtool_id);

        return response()->json(['status' => 200,'url' => $link]);
    }

    public function logoUploadTwo(Request $request)
    {
       if ($request->orgImage == 1) {
        $getHealthTool = Healthtool::where(['id' => $request->healthtool_id])->first();
        $user_avatar = Auth::user()->avatar;
        if(file_exists(storage_path('app/public/'.$user_avatar)))
        {
            $hashed = base64_encode(file_get_contents(storage_path('app/public/'.$user_avatar)));

            $link = $this->getPrettyLinkUrl($hashed,$request->healthtool_id);

            return response()->json(['status' => 200,'url' => $link]);
        }
        else
        {
            return 1;
        }

        }
    }

    public function postToFav(Request $request)
    {
        $data = $request->all();
        $data['is_fav'] = 1 ;
        $save = HealthToolsFav::create($data);

        if ($save) {
            return response()->json(['status' => 200 ]);
        }else{
            return response()->json(['status' => 500 ]);
        }
    }

    public function deleteFromFav(Request $request)
    {
        // return Auth::user()->id;
        $favTool = HealthToolsFav::where(['id' => $request->id])->first();
        $delete = $favTool->delete();

        if ($delete) {
          return response()->json(['status' => 200 ]);
        }else{
          return response()->json(['status' => 500 ]);
        }
    }

    public function shareMyfavorites($user_id)
    {
        // return $user_id;
        $username = User::where(['id' => $user_id])->pluck('name')->first();

        //return $username;
        $favTools = HealthToolsFav::where(['user_id' => $user_id])->get();
        // return  $favTools;
        return view('myfavtool')->with(compact(['favTools','username']));
    }

    public function myaccount()
    {
        $favTools = HealthToolsFav::where(['user_id' => Auth::user()->id,'is_fav' => 1 ])->get();
        // return $favTools;
        return view('myaccount')->with(compact(['favTools']));
    }

    public function myaccount_update(Request $request)
    {
        // return $request->all();

        $validatedData = $request->validate([
               'name' => ['required', 'string', 'max:255'],
               'lname' => ['required', 'string', 'max:255'],
               'organization' => ['required', 'string', 'max:255'],
               'contact' => [ 'max:12'],
            //    'update' => ['required'],
               'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore(Auth::id())],
           ]);

        $update_profile = User::find(Auth::id());
        $update_profile->name = $request->name;
        $update_profile->lname = $request->lname;
        $update_profile->organization = $request->organization;
        $update_profile->email = $request->email;
        $update_profile->contact = $request->contact;
        if($request->update == "on"){
            $update_profile->update = 1;
        }else{
            $update_profile->update = 0;
        }

        if (!empty($request->avatar)) {
            // change avatar
            if ($request->hasFile('avatar')) {
                $avatar = $request->file('avatar');

                $test_name = Rand(1,1000);

                $name = $test_name.'.'.$avatar->getClientOriginalExtension();
                $destinationPath = storage_path('/app/public/users');
                $avatarPath = $destinationPath. "/".  $name;
                $test =  $avatar->move($destinationPath, $name);
                //$grades->avatar = $name;
                $final_name = $name;
            }
            $update_profile->avatar = 'users/'.$final_name;
        }
        $check = $update_profile->save();

        if (!($check)) {

            return redirect()->back()->with(['message' => "Website Under maintenance, please try again later.!", 'alert-type' => 'error']);
        }else{

            return redirect()->back()->with(['message' => "Profile updated successfully!", 'alert-type' => 'success']);
        }

    }
    public function checkpassword(Request $request)
    {
        $old_password = Hash::check($request->old_password, Auth::user()->password);

        if ($old_password == 1) {
            return response()->json(['status' => '1', 'message' => 'Correct Password.!']);
        }else{
            return response()->json(['status' => '0', 'message' => 'Wrong Password.!' ]);
        }

    }
    public function changepassword(Request $request)
    {
        // validation for passwords
        $validatedData = $request->validate([
               'new_password' => ['required', 'min:8'],
           ]);

        $user = User::find(Auth::user()->id);
        if ($request->new_password == $request->password_confirmation) {
            $user->password = Hash::make($request->new_password);
            $update = $user->save();

              if (!$update) {
                return redirect()->back()->with(['message' =>'Your password has been updated successfully!', 'alert-type' => 'error']);
              }else{
                $name = Auth::user()->name;
                $email = Auth::user()->email;


                Mail::send('password_update_email',['email' => $email],function($message) use ($email){
                $message->subject('Password Changed');
                $message->from('noreply@LIVERHEALTHNOW.com','LIVERHEALTHNOW');
                $message->sender('noreply@LIVERHEALTHNOW.com', 'LIVERHEALTHNOW');
                $message->to($email);
                });

                return redirect()->back()->with(['message' => "Your password has been updated successfully!", 'alert-type' => 'success']);
              }
        }else{
            return redirect()->back()->with(['message' => "Passwords do not match.!", 'alert-type' => 'error']);
        }

    }

    public function profilepicture(Request $request)
    {
      if($_FILES["file"]["name"] != '')
      {
        $ext = $request->file->extension();
        $temp_name =  $request->file->getClientOriginalName();
        // $hashed = Hash::make($temp_name);
        $hashed = base64_encode($temp_name);
        $uid =  date('YmdHis');
        $new_name = $hashed.$uid;
        $final_name = $new_name.'.'.$ext;
        $save_image = $request->file->storeAs('public/users/',$final_name);
        if (!$save_image) {
          return response()->json(['message' => 2]);
        }else{
          // image saved

          // find his previous image and check if it is default.png
          $check_previous_image = $avatar = User::find(Auth::user()->id);
          if ($check_previous_image->avatar != "users/default.png") {
              if(file_exists(storage_path('app/public/'.$check_previous_image->avatar)))
              {
                $delete_old_profile_picture = unlink(storage_path('app/public/'.$check_previous_image->avatar));
              }

          }
          // find his previous image and check if it is default.png
          $avatar = User::find(Auth::user()->id);
          $avatar->avatar = 'users/'.$final_name;
          $update = $avatar->save();
          if (!$update) {
            return response()->json(['message' => 0]);
          }else{
            return response()->json(['message' => 1]);
          }
        }
      }else{
        return response()->json(['message' => 3]);
      }
    }

    public function storePluginOrder(Request $request)
    {
        if(Auth::check() && Auth::user()->need_assessment == 1) {
            $validator = Validator::make($request->all(), [
                'plugin_type' => 'required',
                'name' => 'required',
                'email' => 'required',
                'professional_title' => 'required',
                'organization_name' => 'required',
                'organization_city' => 'required',
                'organization_state' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }



        $order = new PluginOrder;
        $order->plugin_type = $request->plugin_type;
        $plugin_type = '';
        if($request->plugin_type == 1){
            $plugin_type = 'Ambulatory Care';
        }elseif($request->plugin_type == 2){
            $plugin_type = 'Long Term Care';
        }elseif($request->plugin_type == 3 ){
            $plugin_type = 'All';
        }else{
            $plugin_type = '';
        }

        $custom_log = '';
        if($request->custom_log == "on"){
            $order->custom_log = 1;
            $custom_log = 'Yes';
        }else{
            $order->custom_log = 0;
            $custom_log = 'No';
        }
        if(Auth::check() && Auth::user()->need_assessment == 1) {
            $order->manager_name = Auth::user()->name ." ".Auth::user()->lname;
            $order->manager_email = Auth::user()->email;
            $order->name = $request->name;
            $order->email = $request->email;
            $order->professional_title = $request->professional_title;
            $order->organization_name = $request->organization_name;
            $order->organization_city = $request->organization_city;
            $order->organization_state = $request->organization_state;

            $data = array(
                'plugin_type' => $plugin_type,
                'custom_log' => $custom_log,
                'manager_name' => Auth::user()->name ." ".Auth::user()->lname,
                'manager_email' => Auth::user()->email,
                'name'=>$request->name,
                'email'=>$request->email,
                'professional_title'=>$request->professional_title,
                'organization_name'=>$request->organization_name,
                'organization_city'=>$request->organization_city,
                'organization_state'=>$request->organization_state,
            );
        }else{
            $stateInfo = State::where('id',Auth::user()->state_id)->first();
            if($stateInfo){
                $organization_state = $stateInfo->state;
            }else{
                $organization_state = '';
            }
            $order->name = Auth::user()->name ." ".Auth::user()->lname;
            $order->manager_name = Auth::user()->salix_manager_name;
            $order->email = Auth::user()->email;
            $order->professional_title = Auth::user()->job_title;
            $order->organization_name = Auth::user()->organization;
            $order->organization_city = Auth::user()->city;
            $order->organization_state = $organization_state;

            $data = array(
                'plugin_type' => $plugin_type,
                'custom_log' => $custom_log,
                'manager_name' => Auth::user()->salix_manager_name,
                'manager_email' => 'NA',
                'name'=>Auth::user()->name ." ".Auth::user()->lname,
                'email'=>Auth::user()->email,
                'professional_title'=>Auth::user()->job_title,
                'organization_name'=>Auth::user()->organization,
                'organization_city'=>Auth::user()->city,
                'organization_state'=> $organization_state,
            );
        }



        $save = $order->save();



        if($save){
            // email to admins
            $name = Auth::user()->email;
            $email = Auth::user()->email;
            Mail::send('order_plugin_email',$data,function($message) use ($email){
                $message->subject('ORDER PLUGIN');
                $message->from('support@liverhealthnow.com','LIVERHEALTHNOW');
                $message->sender('support@liverhealthnow.com', 'LIVERHEALTHNOW');
                $message->to('support@liverhealthnow.com');
            });
            if(Auth::check() && Auth::user()->need_assessment == 1){
                return redirect()->back()->with('manager_success','manager_success');
            }else{
                return redirect()->back()->with('customer_success','customer_success');
            }
        }else{
            return redirect()->back();
        }
    }

    public function getOrgTypes(Request $request)
    {
        return $request->all();
    }
    public function getUser(Request $request)
    {
        if($request->token == 'combo@actonms#123!')
        {
            $users = User::get();
            return UserHealthToolCollection::collection($users);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }

    public function getTools(Request $request)
    {
        if($request->token == 'combo@actonms#123!')
        {
            $tools = DB::table('user_health_tools')->get();
            return response()->json(['data' => $tools]);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }

    public function userPluginOrders(Request $request)
    {
        if($request->token == 'combo@actonms#123!')
        {
            $plugins = PluginOrder::where('manager_email',null)->get();

            return PluginOrderCollection::collection($plugins);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }

    public function managerPluginOrders(Request $request)
    {
        if($request->token == 'combo@actonms#123!')
        {
            $plugins = PluginOrder::where('manager_email','!=',null)->get();

            return ManagerPluginOrderCollection::collection($plugins);
        }
        else
        {
            return response()->json(['status' => 401 , 'message' => 'Unauthenticated']);
        }
    }
    
    // public function migrateUser()
    // {
    //     $migrated_users = DB::table('migrate_users')->get();
    //     foreach ($migrated_users as $migrated_user){
    //         $checkUser = User::where('email',$migrated_user->email)->first();
    //         if(empty($checkUser)){
    //             $newUser = new User;
    //             $newUser->role_id = $migrated_user->role_id;
    //             $newUser->name = $migrated_user->name;
    //             $newUser->email = $migrated_user->email;
    //             $newUser->avatar = $migrated_user->avatar;
    //             $newUser->email_verified_at = $migrated_user->email_verified_at;
    //             $newUser->password = $migrated_user->password;
    //             $newUser->remember_token = $migrated_user->remember_token;
    //             $newUser->settings = $migrated_user->settings;
    //             $newUser->created_at = $migrated_user->created_at;
    //             $newUser->updated_at = $migrated_user->updated_at;
    //             $newUser->created_at_time_zone = $migrated_user->created_at_time_zone;
    //             $newUser->updated_at_timezone = $migrated_user->updated_at_timezone;
    //             $newUser->lname = $migrated_user->lname;
    //             $newUser->organization = $migrated_user->organization;
    //             $newUser->organization_id = $migrated_user->organization_id;
    //             $newUser->job_title = $migrated_user->job_title;
    //             $newUser->contact = $migrated_user->contact;
    //             $newUser->address = $migrated_user->address;
    //             $newUser->salix_manager_name = $migrated_user->salix_manager_name;
    //             $newUser->source_id = $migrated_user->source_id;
    //             $newUser->update = $migrated_user->update;
    //             $newUser->agreement_check = $migrated_user->agreement_check;
    //             $newUser->timezone = $migrated_user->timezone;
    //             $newUser->last_login = $migrated_user->last_login;
    //             $newUser->last_login_timezone = $migrated_user->last_login_timezone;
    //             $newUser->code = $migrated_user->code;
    //             $newUser->status = $migrated_user->status;
    //             $newUser->department_id = $migrated_user->department_id;
    //             $newUser->has_department = $migrated_user->has_department;
    //             $newUser->need_assessment = 0;
    //             $newUser->save();
    //         }

    //     }
    // }
}