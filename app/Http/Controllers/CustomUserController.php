<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Auth;
use Mail;

class CustomUserController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function update(Request $request, $id)
    {
    	// return $request->all();
    	// email to user if status is edited
    	if ($request->status == 1) {
    		$reciever = $request->email;
    // 		$mail_message ="LiverHealth Access: ". "\r\n" .
    // 		"Hi ".$request->name. "\r\n" .
    // 		"LiverHealth has given you the access to your dashboard". "\r\n" .
    // 		"Please login with ".$request->email." and your configured password.!" ;

        	Mail::send('access_mail',[],function($message) use ($reciever){
                $message->subject('Account Activated');
                $message->from('noreply@liverhealthnow','LIVERHEALTHNOW');
                $message->sender('noreply@liverhealthnow', 'LIVERHEALTHNOW');
                $message->to($reciever);
            });
        }
        if ($request->status == 0) {
            $reciever = $request->email;
            Mail::send('access_denied_mail',[],function($message) use ($reciever){
                $message->subject('Criteria Not Met');
                $message->from('noreply@liverhealthnow','LIVERHEALTHNOW');
                $message->sender('noreply@liverhealthnow', 'LIVERHEALTHNOW');
                $message->to($reciever);
            });

        }
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }


    }
}
