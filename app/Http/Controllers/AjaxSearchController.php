<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class AjaxSearchController extends Controller
{
    function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $output_two = '';
      $output_three = '';
      $query = $request->get('query');
      $page = $request->get('page');

      if($query != '')
      {
       $data = DB::table('healthtools')
         ->where('title', 'like', '%'.$query.'%')
         ->when(!empty($request->get('page')), function($q) use ($page) {
            return $q->whereJsonContains('category', $page);
         })
         ->where(['status' => 1])
         ->get();

      //  $ebwdata = DB::table('workflows')
      //    ->where('title', 'like', '%'.$query.'%')
      //    ->where(['status' => 1])
      //    ->get();
      //  $guidelines = DB::table('guidelines')
      //    ->where('title', 'like', '%'.$query.'%')
      //    ->where(['status' => 1])
      //    ->get();

      }
      else
      {
       $data = DB::table('healthtools')
        ->when(!empty($request->get('page')), function($q) use ($page) {
            return $q->whereJsonContains('category', $page);
         })
         ->orderBy('title')
         ->where(['status' => 1])->get();
      //  $ebwdata = DB::table('workflows')
      //    ->orderBy('id', 'desc')
      //    ->where(['status' => 1])->get();
      //  $guidelines = DB::table('guidelines')
      //    ->orderBy('id', 'desc')
      //    ->where(['status' => 1])->get();
      }
      // $total_row = $data->count() + $ebwdata->count() + $guidelines->count();
      $total_row = $data->count() ;
      if($total_row > 0)
      {
       foreach($data as $row)
        {
        if (!(Auth::check())) {
          $row->pretty_link = route('login');
        }
        $output .= '
        <li><a href='.route('customizeHealthTools',['healthtool_id' => $row->id]).'>'.$row->title.'</a></li>';
       }

      //  foreach($ebwdata as $row_two)
      //  {
      //   if (!(Auth::check())) {
      //     $row_two->pretty_link = route('login');
      //   }
      //   $output_two .= '
      //   <li><a href='.$row_two->pretty_link.'>'.$row_two->title.'</a></li>';
      //  }

      //  foreach($guidelines as $row_three)
      //  {
      //   if (!(Auth::check())) {
      //     $row_three->pretty_link = route('login');
      //   }
      //   $output_three .= '
      //   <li><a href='.$row_three->preety_link.'>'.$row_three->title.'</a></li>';
      //  }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';

      //  $output_two = '
      //  <tr>
      //   <td align="center" colspan="5">No Data Found</td>
      //  </tr>
      //  ';
      //   $output_three = '
      //  <tr>
      //   <td align="center" colspan="5">No Data Found</td>
      //  </tr>
      //  ';
      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row,
      //  'table_data_two'  => $output_two,
      //  'table_data_three'  => $output_three,

      );

      echo json_encode($data);


     }
    }
}
