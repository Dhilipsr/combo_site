<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Rules\Captcha;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register/thank-you';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $message = [
            'name.required' => 'This field is required',
            'lname.required' => 'This field is required',
            'organization.required' => 'This field is required',
            'organization_id.required' => 'This field is required',
            'job_title.required' => 'This field is required',
            'city.required' => 'This field is required',
            'state_id.required' => 'This field is required',
            'agreement_check.required' => 'This field is required',
            'email.required' => 'This field is required',
            'password.required' => 'This field is required',
            'npi_number.digits' => 'The NPI number must be 10 digits',
        ];

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'organization' => ['required', 'string', 'max:255'],
            'organization_id' => ['required'],
            'job_title' => ['required', 'string'],
            // 'code' => ['sometimes','nullable','alpha_num'],
            // 'address' => ['required', 'string', 'max:255'],
            // 'salix_manager_name' => ['required', 'string', 'max:255'],
            'city' => ['required'],
            'state_id' => ['required'],
            // 'source_id' => ['required'],
            'npi_number' => ['sometimes','nullable','digits:10'],
            'agreement_check' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'g-recaptcha-response' => new Captcha(),
        ], $message);


    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // return $data;
        if (!empty($data['update'])) {
            if ($data['update'] == "on") {
            $update = 1;
            }
        }else{
            $update = 0;
        }

        if (!empty($data['agreement_check'])) {
            if ($data['agreement_check'] == "on") {
            $agreement_check = 1;
            }
        }else{
            $agreement_check = 0;
        }
            $code = '';
        if (!empty($data['code'])) {
            $code = $data['code'];
        }else{
            $code = null;
        }
        $ip =  $_SERVER['REMOTE_ADDR'];
        // $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
        // $ipInfo = json_decode($ipInfo);
        // $timezone = $ipInfo->timezone;
        // date_default_timezone_set($timezone);
        // $timezonename = date_default_timezone_get();
        $loginTime = date('Y-m-d H:i:s');

         $user = User::create([
            'name' => $data['name'],
            'lname' => $data['lname'],
            'organization' => $data['organization'],
            'organization_id' => $data['organization_id'],
            'job_title' => $data['job_title'],
            'contact' => $data['contact'],
            // 'address' => $data['address'],
            'salix_manager_name' => $data['salix_manager_name'],
            'source_id' => $data['source_id'],
            'code' => $code,
            'update' => $update,
            'agreement_check' => $agreement_check,
            'status' => 0,
            'email' => $data['email'],
            'npi_number' => $data['npi_number'],
            'city' => $data['city'],
            'state_id' => $data['state_id'],
            // 'timezone' => $timezonename,
            // 'created_at_time_zone' => $timezonename,
            // 'updated_at_timezone' => $timezonename,
            'password' => Hash::make($data['password']),
        ]);

         // send user an email
         $user_mail = $data['email'];
         $user_name = $data['name'];

          Mail::send('new_registeration_mail',[],function($message) use ($user_mail){
                $message->subject('Thank you for registering with liverhealthnow.com/all');
                $message->from('noreply@liverhealthnow.com','LIVERHEALTHNOW');
                $message->sender('noreply@liverhealthnow.com', 'LIVERHEALTHNOW');
                $message->to($user_mail);
            });


            Mail::send('admin_update_email',['email' => $data['email'],'name' => $data['name'],'lname' => $data['lname'],'manager' => $data['salix_manager_name'],'organization' => $data['organization']],function($message){
                $message->subject('New User Registration');
                $message->from('noreply@liverhealthnow.com','LIVERHEALTHNOW');
                $message->sender('noreply@liverhealthnow.com', 'LIVERHEALTHNOW');
                $message->to('noreply@liverhealthnow.com');

            });

            $organisation_type;
            $source_type;


         return $user;
    }
}
