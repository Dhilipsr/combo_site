<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/test';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm()
    {
        return view('auth.login');
    }

 public function login(Request $request)
    {

        // echo $request;exit;
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // $ip =  $_SERVER['REMOTE_ADDR'];

        // $ipInfo = file_get_contents('http://ip-api.com/json/' . $ip);
        // $ipInfo = json_decode($ipInfo);
        // $timezone = $ipInfo->timezone;

        // date_default_timezone_set($timezone);

        // $timezonename = date_default_timezone_get();

        $loginTime = date('Y-m-d H:i:s');



        if ($this->attemptLogin($request)) {
            $user = User::where(['id' => Auth::user()->id])->first();
            if($user->status != 1){
                Auth::logout();

                return redirect()->route('login')->with('under_review','Under review');
            }
            // echo $loginTime;
            // die;
            // $user->last_login_timezone = $timezonename;
            $user->last_login = $loginTime;
            $user->save();
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public $last_url = "";

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $last_url = request('last_url');

        // $last_url = Session::get('page');
        $this->redirectTo = $last_url;
    }

    public function logout()
    {
        Session::flush();

        Auth::logout();

        return redirect('https://liverhealthnow.com/login');
    }
}
