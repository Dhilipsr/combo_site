<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        $email = $user->email;
         Mail::send('password_update_email',['email' => $email],function($message) use ($email){
                $message->subject('Password Changed');
                $message->from('noreply@LIVERHEALTHNOW.com','LIVERHEALTHNOW');
                $message->sender('noreply@LIVERHEALTHNOW.com', 'LIVERHEALTHNOW');
                $message->to($email);
        });

        // event(new PasswordReset($user));

        $this->guard()->login($user);
    }

     protected function setUserPassword($user, $password)
    {
        $user->password = Hash::make($password);
    }
}
