<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SalixManager extends Model
{
    public function managerName(){
        return $this->belongsTo('App\SalixManager','salix_manager_name');
    }
}