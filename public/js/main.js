$(document).ready(function () {

  /*   var butInt = $('.refrences_sec h5');
    var slider = $('.slider');

    documnet.on('click', 'butInt', function (e) {
      // alert();
      if (slider.hasClass('open')) {
        butInt.toggleClass('open');
        slider.toggleClass('open');
      } else {
        slider.toggleClass('open');
        butInt.toggleClass('open');
      }

    }); */

  var button = $('.refrences_sec h5');
  var slider = $('.slider');

  button.on('click', function (e) {

    if (slider.hasClass('open')) {
      button.toggleClass('open');
      slider.toggleClass('open');
    } else {
      slider.toggleClass('open');
      button.toggleClass('open');
    }

  });

  stickyHeader();
  $(window).scroll(function () {
    stickyHeader();
  });

  $(".sortSel").select2({
    placeholder: "Sort By"
  });
  $(".orgTp").select2({
    placeholder: "Organization Type"
  });

  $(".languageSel").select2({
    placeholder: "Language"
  });

  var dataIdRef;
  $(document).on('click', '.htFiltHdSpan', function (e) {
    e.stopPropagation();
    let dataId = $(this).data('id');
    // console.log(dataId);
    if (dataIdRef == undefined || dataIdRef != dataId) {
      $('.htFiltHdSpan,.select2 ').css('opacity', 0.5);
      $(this).css('opacity', 1);
      $('.filtIntBox').show();
      $('.htFiltBody').hide();
      $('.htFiltSearchBox').hide();
      $('.htFiltSortBox').hide();
      $('.htFiltBody' + dataId).show();
      dataIdRef = dataId;
    } else {
      filterClose();
    }
  });
  $(document).on('click', 'body', function () {
    filterClose();
  });

  function filterClose() {
    $('.htFiltHdSpan,.select2').css('opacity', 1);
    $('.filtIntBox').hide();
    $('.htFiltBody').show();
    $('.htFiltSearchBox').show();
    $('.htFiltSortBox').show();
    dataIdRef = undefined;
  }

  $(document).on('click', '.filtIntBox', function (e) {
    e.stopPropagation();
  })

  $(document).on('click', '.srchBtn', function (e) {
    $('.searchContainer').fadeIn(300);
    $('body').css('overflow', 'hidden');

  })
  $(document).on('click', '.srchClose, .searchContainer', function (e) {
    $('.searchContainer').fadeOut(300);
    $('body').css('overflow', 'auto');
  })
  $(document).on('click', '.srch, .srch__container', function (e) {
    e.stopPropagation();
  })

  /* Menu */
  $(document).on('click', '.menu-icon', function () {
    $('.mob-menu-up').fadeIn();
  })
  $(document).on('click', '.mob-close', function () {
    $('.mob-menu-up').fadeOut();
  })
  /* //Menu */

  /* Alert message */
  $('.successBtn').on('click', function () {
    $.alert('Success Message Comes Here!', {
      closeTime: 3000,
      position: ['top-right'],
      type: 'success'
    });
  });
  $('.errorBtn').on('click', function () {
    $.alert('Error Message Comes Here!', {
      closeTime: 3000,
      position: ['top-right'],
      type: 'error'
    });
  });

  /* //Alert message */


  /* SWIPER SLIDER */
  /*   var swiper = new Swiper('.swiper-container', {
      spaceBetween: 30,
      effect: 'fade',

      // autoplay:false,
      fadeEffect: {
        crossFade: true
      },
      /* autoplay: {
          delay: 3000,
      },
      speed: 3000,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },

    }); */



  $("form").on("change", ".file-upload-field", function () {
    $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
  });



  // $('.copy-url').click(function (event) {
  //   copyT();
  //   $(this).html("<i><img src='img/tick.svg'></i> Copied");
  //   event.preventDefault();
  // });

  // $('.favrite-btn').click(function (event) {
  //   $(this).html("<i class='icon-img'><img src='img/tick.svg'></i> Added to your profile page");
  //   event.preventDefault();
  // });




  $('.select2').select2({
    placeholder: "Select an option",
  });

  $('.select2Title').select2({
    placeholder: "Language",
  });




});





function stickyHeader() {
  var sticky = $('header'),
    scroll = $(window).scrollTop();

  if (scroll >= 40) sticky.addClass('fixHeader');
  else sticky.removeClass('fixHeader');
}

// function startTransition() {
//   var outTransition = new TimelineMax();
//   outTransition
//     .to("body", 0.5, {
//       autoAlpha: 0,
//       ease: Power1.easeInOut,
//     }, 0)
// }

// function endTransition() {
//   TweenMax.to("body", 0.5, {
//     autoAlpha: 1,
//     ease: Expo.easeOut
//   }, 0);
// }
// $(window).load(function () {
//   // console.log('loaded');
//   endTransition();
// });


function copyT() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");



  /* Alert the copied text */
  alert("Copied the text: " + copyText.value);
}