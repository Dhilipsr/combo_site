-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2021 at 03:15 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.3.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lhnace_combo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ace_care_pathways`
--

CREATE TABLE `ace_care_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_ehr_plugins_page`
--

CREATE TABLE `ace_ehr_plugins_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gina_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gina_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_guidelines`
--

CREATE TABLE `ace_guidelines` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ace_guidelines`
--

INSERT INTO `ace_guidelines` (`id`, `title`, `content`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'Guidelines and Links', '<p>Quality measures help quantify processes, outcomes, patient perceptions, and/or risk share, with the goal of providing high-quality health care. Measures drive the standardization of care by identifying differences in care delivery and outcomes.</p>\r\n<p>Quality measures are becoming increasingly important as our health care system shifts from traditional fee-for-service toward value-based care and Centers for Medicare &amp; Medicaid Services (CMS) quality-based reimbursement models.</p>\r\n<p>The links below include the latest quality measures for cirrhosis care as well as a link to the American Association for the Study of Liver Diseases (AASLD) for more information around managing chronic liver disease.</p>\r\n<p>All of the health tools on this site are written to CMS guidelines for font and readability and support the AASLD guidelines below. We are committed to providing members with the latest CMS and AASLD updates as they are released to help members stay aligned with the latest quality care.</p>', NULL, NULL, NULL, '2021-11-23 00:18:50', '2021-11-23 00:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `ace_guidelines_links`
--

CREATE TABLE `ace_guidelines_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_health_systems`
--

CREATE TABLE `ace_health_systems` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_line` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ace_health_systems`
--

INSERT INTO `ace_health_systems` (`id`, `meta_title`, `meta_description`, `keywords`, `page_title`, `banner_image`, `banner_description`, `ad_line`, `about_title`, `about_description`, `image_two`, `description_two`, `created_at`, `updated_at`) VALUES
(1, 'Health 1', 'Health 2', 'Health 3', 'Help Improve Care for OHE Patients', 'ace-health-systems\\November2021\\LCG2Pz9t6RYodGmABiQQ.jpg', 'Identifying chronic liver disease patients who have overt hepatic encephalopathy (OHE), then providing those patients and/or their caregivers with vital health tools about the disease, may proactively reduce the risk of complications and HE-related readmissions. Access health tools, guidelines, and more.', '<p><strong>Digital Health Award Winner</strong> for Interactive Content and Rich Media</p>', 'About ACE for Health Systems', 'ACE is a population health platform that allows health system providers to deliver patient tools at the point of care or through the patient web portal. The goal of this platform is to help health care providers manage their patient populations by educating, engaging, and supporting patients with overt hepatic encephalopathy (OHE) and their caregivers.', 'ace-health-systems\\November2021\\868sUw69bBEbBFmP4zU8.jpg', '<p>Discover the latest trends in chronic liver disease care in the first edition of the Liver Health Annual Trends Report.</p>', '2021-11-22 01:30:00', '2021-11-22 02:08:48');

-- --------------------------------------------------------

--
-- Table structure for table `ace_health_system_ehr_plugins`
--

CREATE TABLE `ace_health_system_ehr_plugins` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_image_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_image_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_image_three` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gina_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ace_health_system_ehr_plugins`
--

INSERT INTO `ace_health_system_ehr_plugins` (`id`, `page_title`, `description`, `plugin_one_title`, `plugin_image_one`, `plugin_one_link`, `plugin_two_title`, `plugin_image_two`, `plugin_two_link`, `plugin_three_title`, `plugin_image_three`, `plugin_three_link`, `title_two`, `title_two_link`, `gina_text`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'EHR Plugins', '<p>A plugin is a way to integrate customized provider, patient, and caregiver tools into your workflow within your existing information technology system.</p>\r\n<p>Once installed, resources or tools on the plugin can be <strong> viewed, printed, or sent to patients. </strong>The plugin is EHR-agnostic and also can be used as a mobile application or integrated into a website.</p>', 'Patient Identification', 'ace-health-system-ehr-plugins\\November2021\\YaeUAMB4gPQ7R1Us2Mvb.png', NULL, 'OHE Management', 'ace-health-system-ehr-plugins\\November2021\\6BINmV0RJV5pz2BGfy8a.png', NULL, 'Transitions of Care', 'ace-health-system-ehr-plugins\\November2021\\rRRPm3ZXikiuvs5QDg9Q.png', NULL, 'View Long-Term Care Plugins', NULL, 'GINA™ (Guided INtegration Assistant) can help you integrate the plugin into your EHR system.', NULL, NULL, NULL, '2021-11-24 05:42:27', '2021-11-24 05:42:27');

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_terms_ehr_plugins`
--

CREATE TABLE `ace_long_terms_ehr_plugins` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_image_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_image_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gina_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ace_long_terms_ehr_plugins`
--

INSERT INTO `ace_long_terms_ehr_plugins` (`id`, `page_title`, `description`, `plugin_one_title`, `plugin_image_one`, `plugin_one_link`, `plugin_two_title`, `plugin_image_two`, `plugin_two_link`, `title_two`, `title_two_link`, `gina_text`, `created_at`, `updated_at`, `meta_title`, `meta_description`, `keywords`) VALUES
(1, 'EHR Plugins', '<p>A plugin is a way to integrate customized provider, patient, and caregiver tools into your workflow within your existing information technology system.</p>\r\n<p>Once installed, resources or tools on the plugin can be <strong> viewed, printed, or sent to patients. </strong>The plugin is EHR-agnostic and also can be used as a mobile application or integrated into a website.</p>', 'OHE in Long-term Care', 'ace-long-terms-ehr-plugins\\November2021\\mh1ihlngNZCRbTVP895E.jpg', NULL, 'Cirrhosis Management Kit for Long-term Care', 'ace-long-terms-ehr-plugins\\November2021\\Gjqp9p4cda7eJs10qT7W.jpg', NULL, 'View Health Systems Plugins', NULL, 'GINA™ (Guided INtegration Assistant) can help you integrate the plugin into your EHR system.', '2021-11-23 08:35:20', '2021-11-23 08:35:20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_term_care_d_pathways`
--

CREATE TABLE `ace_long_term_care_d_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_term_care_pathways`
--

CREATE TABLE `ace_long_term_care_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ace_long_term_care_pathways`
--

INSERT INTO `ace_long_term_care_pathways` (`id`, `meta_title`, `meta_description`, `keywords`, `page_title`, `description`, `link_one_title`, `link_one`, `link_two_title`, `link_two`, `link_three_title`, `link_three`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 'Care Pathways', '<p>The American Association for the Study of Liver Diseases (AASLD) and the European Association for the Study of the Liver (EASL) Practice Guideline recommends ways to reduce the risk of overt HE (OHE) recurrence and OHE-related hospitalizations.</p>\r\n<p>Incorporating these guideline recommendations into health system workflows is one way to support standardized care.</p>\r\n<p><strong> The ACE platform</strong> can help standardize evidence-based OHE care within the EHR via care pathways and health tools that align with practice guidelines.</p>', 'Appropriate Identification of Patients With Overt Hepatic Encephalopathy (OHE)', NULL, 'Evidence-based Overt Hepatic Encephalopathy (OHE) Management Recommendations', NULL, 'Evidence-based Overt Hepatic Encephalopathy (OHE) Transitions of Care Recommendations', NULL, '2021-11-23 05:56:00', '2021-11-23 05:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_term_landings`
--

CREATE TABLE `ace_long_term_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ace_long_term_landings`
--

INSERT INTO `ace_long_term_landings` (`id`, `banner_image`, `banner_description`, `description_one`, `title`, `sub_title`, `image`, `content_one`, `content_two`, `ref_text`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'ace-long-term-landings\\November2021\\org7dpkCEZ57Wp4Sg40L.png', 'Help Improve Care for Your Long-Term Care Residents With Cirrhosis and Cirrhotic Complications, Including Hepatic Encephalopathy', '<p>Identifying residents with cirrhosis and cirrhotic complications (including hepatic encephalopathy) and managing their care according to evidence-based guidelines may reduce their risk of further complications and cirrhosis-related readmissions. Implementation of quality improvement measures, when combined with an educational program and standardized order set, may improve the quality of care.</p>', 'ACE|Long-Term Care Is Committed to Quality Care', 'This site may help you identify and manage long-term care residents with cirrhosis and cirrhotic complications, including hepatic encephalopathy.', 'ace-long-term-landings\\November2021\\lavr2POXtyvyK3hxkJmc.jpg', '<p>Discover the latest trends in chronic liver disease care in the first edition of the <a data-toggle=\"modal\" data-target=\"#guideModal\">Liver Health Annual Trends Report.</a></p>', '<p><strong> Reference: 1.</strong> Johnson EA, Spier BJ, Leff JA, Lucey MR, Said A. Optimizing the care of patients with cirrhosis and gastrointestinal hemorrhage: a quality improvement study.<em> Aliment Pharmacol Ther.</em> 2011;34:76&ndash;82.</p>', NULL, NULL, NULL, NULL, '2021-11-22 04:18:00', '2021-11-22 04:22:58');

-- --------------------------------------------------------

--
-- Table structure for table `ace_pathways`
--

CREATE TABLE `ace_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_popups`
--

CREATE TABLE `ace_popups` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ace_popups`
--

INSERT INTO `ace_popups` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
(1, 'AccessClickEngage (ACE) Plugin Order Form', '<p class=\"heading\"><strong>Simple steps to order:</strong></p>\r\n<ul class=\"model-list\">\r\n<li>1) Complete the information below and hit Submit.</li>\r\n<li>2) A member of the Aventria team will contact you if you request a logo on the plugin and health tools.</li>\r\n<li>3) Your plugin will be sent to you in 1 week (5 business days).</li>\r\n</ul>\r\n<p>If you have any questions, please contact <a> support@liverhhealthnow.com.</a></p>', '2021-11-24 07:19:33', '2021-11-24 07:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `ace_quality_measures`
--

CREATE TABLE `ace_quality_measures` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'voyager::seeders.data_rows.roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(24, 4, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(25, 4, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 12),
(26, 4, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(27, 4, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(28, 4, 'link_one_title', 'text', 'Link One Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(29, 4, 'link_one', 'text', 'Link One', 0, 1, 1, 1, 1, 1, '{}', 5),
(30, 4, 'link_two_title', 'text', 'Link Two Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(31, 4, 'link_two', 'text', 'Link Two', 0, 1, 1, 1, 1, 1, '{}', 7),
(32, 4, 'link_three_title', 'text', 'Link Three Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(33, 4, 'link_three', 'text', 'Link Three', 0, 1, 1, 1, 1, 1, '{}', 9),
(34, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(35, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(36, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(37, 5, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(38, 5, 'link_title', 'text', 'Link Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(39, 5, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(42, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(43, 7, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(44, 7, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(45, 7, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 12),
(46, 7, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(47, 7, 'banner_image', 'image', 'Banner Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(48, 7, 'banner_description', 'text', 'Banner Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(49, 7, 'ad_line', 'rich_text_box', 'Ad Line', 0, 1, 1, 1, 1, 1, '{}', 5),
(50, 7, 'about_title', 'text', 'About Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(51, 7, 'about_description', 'rich_text_box', 'About Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(52, 7, 'image_two', 'image', 'Image Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(53, 7, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 9),
(54, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(55, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(56, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 8, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(58, 8, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(59, 8, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 12),
(60, 8, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(61, 8, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(62, 8, 'link_one_title', 'text', 'Link One Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(63, 8, 'link_one', 'text', 'Link One', 0, 1, 1, 1, 1, 1, '{}', 5),
(64, 8, 'link_two_title', 'text', 'Link Two Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(65, 8, 'link_two', 'text', 'Link Two', 0, 1, 1, 1, 1, 1, '{}', 7),
(66, 8, 'link_three_title', 'text', 'Link Three Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(67, 8, 'link_three', 'text', 'Link Three', 0, 1, 1, 1, 1, 1, '{}', 9),
(68, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(69, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(70, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(71, 9, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(72, 9, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 3),
(73, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(74, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(75, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(76, 10, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(77, 10, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(78, 10, 'plugin_one_title', 'text', 'Plugin One Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(79, 10, 'plugin_image_one', 'image', 'Plugin Image One', 0, 1, 1, 1, 1, 1, '{}', 5),
(80, 10, 'plugin_one_link', 'text', 'Plugin One Link', 0, 1, 1, 1, 1, 1, '{}', 6),
(81, 10, 'plugin_two_title', 'text', 'Plugin Two Title', 0, 1, 1, 1, 1, 1, '{}', 7),
(82, 10, 'plugin_image_two', 'image', 'Plugin Image Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(83, 10, 'plugin_two_link', 'text', 'Plugin Two Link', 0, 1, 1, 1, 1, 1, '{}', 9),
(84, 10, 'title_two', 'text', 'Title Two', 0, 1, 1, 1, 1, 1, '{}', 10),
(85, 10, 'title_two_link', 'text', 'Title Two Link', 0, 1, 1, 1, 1, 1, '{}', 11),
(86, 10, 'gina_text', 'text', 'Gina Text', 0, 1, 1, 1, 1, 1, '{}', 12),
(87, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 16),
(88, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 17),
(89, 10, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 13),
(90, 10, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 14),
(91, 10, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 15),
(92, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(93, 12, 'banner_image', 'image', 'Banner Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(94, 12, 'banner_description', 'text_area', 'Banner Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(95, 12, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 4),
(96, 12, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(97, 12, 'sub_title', 'text', 'Sub Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(98, 12, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 7),
(99, 12, 'content_one', 'rich_text_box', 'Content One', 0, 1, 1, 1, 1, 1, '{}', 8),
(100, 12, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 9),
(101, 12, 'ref_text', 'text', 'Ref Text', 0, 1, 1, 1, 1, 1, '{}', 10),
(102, 12, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 11),
(103, 12, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 12),
(104, 12, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 13),
(105, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(106, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(107, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(108, 13, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(109, 13, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(110, 13, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(111, 13, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(112, 13, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 6),
(113, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(114, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(115, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(116, 14, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(117, 14, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 3),
(118, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(119, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(120, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(121, 15, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(122, 15, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(123, 15, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(124, 15, 'content_two', 'text', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(125, 15, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(126, 15, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(127, 15, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 8),
(128, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(129, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(130, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(131, 16, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(132, 16, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(133, 16, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(134, 16, 'title_two', 'text', 'Title Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(135, 16, 'description_two', 'text', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 6),
(136, 16, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 7),
(137, 16, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(138, 16, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(139, 16, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 10),
(140, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(141, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(142, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(143, 17, 'title', 'rich_text_box', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(144, 17, 'reference', 'rich_text_box', 'Reference', 0, 1, 1, 1, 1, 1, '{}', 3),
(145, 17, 'title_two', 'rich_text_box', 'Title Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(146, 17, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 5),
(147, 17, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(148, 17, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 7),
(149, 17, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(150, 17, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(151, 17, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 10),
(152, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(153, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(154, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(155, 18, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(156, 18, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(157, 18, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(158, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(159, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(160, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(161, 19, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(162, 19, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 3),
(163, 19, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(164, 19, 'description_three', 'rich_text_box', 'Description Three', 0, 1, 1, 1, 1, 1, '{}', 5),
(165, 19, 'tool_one_title', 'text', 'Tool One Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(166, 19, 'tool_one_description', 'rich_text_box', 'Tool One Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(167, 19, 'tool_two_title', 'text', 'Tool Two Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(168, 19, 'tool_two_description', 'rich_text_box', 'Tool Two Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(169, 19, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 10),
(170, 19, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 11),
(171, 19, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 12),
(172, 19, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 13),
(173, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(174, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(175, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(176, 20, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(177, 20, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(178, 20, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(179, 20, 'content_three', 'rich_text_box', 'Content Three', 0, 1, 1, 1, 1, 1, '{}', 5),
(180, 20, 'provider_resource_content', 'rich_text_box', 'Provider Resource Content', 0, 1, 1, 1, 1, 1, '{}', 6),
(181, 20, 'patient_resource_content', 'rich_text_box', 'Patient Resource Content', 0, 1, 1, 1, 1, 1, '{}', 7),
(182, 20, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 8),
(183, 20, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 9),
(184, 20, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 10),
(185, 20, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 11),
(186, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 12),
(187, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(188, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(189, 21, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(190, 21, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 3),
(191, 21, 'content_one', 'rich_text_box', 'Content One', 0, 1, 1, 1, 1, 1, '{}', 4),
(192, 21, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(193, 21, 'tool_one_description', 'rich_text_box', 'Tool One Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(194, 21, 'tool_two_description', 'rich_text_box', 'Tool Two Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(195, 21, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 10),
(196, 21, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 11),
(197, 21, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 12),
(198, 21, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 13),
(199, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(200, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(201, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(202, 22, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(203, 22, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 3),
(204, 22, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(205, 22, 'description_three', 'rich_text_box', 'Description Three', 0, 1, 1, 1, 1, 1, '{}', 7),
(206, 22, 'description_four', 'rich_text_box', 'Description Four', 0, 1, 1, 1, 1, 1, '{}', 10),
(207, 22, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 11),
(208, 22, 'description_five', 'rich_text_box', 'Description Five', 0, 1, 1, 1, 1, 1, '{}', 12),
(209, 22, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 13),
(210, 22, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 14),
(211, 22, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 15),
(212, 22, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 16),
(213, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 17),
(214, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 18),
(215, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(216, 23, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(217, 23, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(218, 23, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(219, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(220, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(221, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(222, 24, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(223, 24, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(224, 24, 'video_link', 'text', 'Video Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(225, 24, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(226, 24, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(227, 24, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 7),
(228, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(229, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(230, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(231, 25, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(232, 25, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(233, 25, 'video_link', 'text', 'Video Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(234, 25, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(235, 25, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(236, 25, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 7),
(237, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(238, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(239, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(240, 26, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(241, 26, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(242, 26, 'banner_image', 'image', 'Banner Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(243, 26, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(244, 26, 'lhn_btn_one', 'text', 'Lhn Btn One', 0, 1, 1, 1, 1, 1, '{}', 6),
(245, 26, 'lhn_content_one', 'text_area', 'Lhn Content One', 0, 1, 1, 1, 1, 1, '{}', 7),
(246, 26, 'lhn_btn_two', 'text', 'Lhn Btn Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(247, 26, 'lhn_content_two', 'text_area', 'Lhn Content Two', 0, 1, 1, 1, 1, 1, '{}', 9),
(248, 26, 'ace_link_one', 'text', 'Ace Link One', 0, 1, 1, 1, 1, 1, '{}', 10),
(249, 26, 'ace_content_one', 'text_area', 'Ace Content One', 0, 1, 1, 1, 1, 1, '{}', 11),
(250, 26, 'ace_link_two', 'text', 'Ace Link Two', 0, 1, 1, 1, 1, 1, '{}', 12),
(251, 26, 'ace_content_two', 'text_area', 'Ace Content Two', 0, 1, 1, 1, 1, 1, '{}', 13),
(252, 26, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 14),
(253, 26, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 15),
(254, 26, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 16),
(255, 26, 'note', 'text', 'Note', 0, 1, 1, 1, 1, 1, '{}', 17),
(256, 26, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 18),
(257, 26, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 19),
(258, 26, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 20),
(259, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 21),
(260, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 22),
(261, 22, 'video_title_one', 'text', 'Video Title One', 0, 1, 1, 1, 1, 1, '{}', 5),
(262, 22, 'video_title_one_link', 'text', 'Video Title One Link', 0, 1, 1, 1, 1, 1, '{}', 6),
(263, 22, 'video_title_two', 'text', 'Video Title Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(264, 22, 'video_title_two_link', 'text', 'Video Title Two Link', 0, 1, 1, 1, 1, 1, '{}', 9),
(265, 21, 'tool_one_title', 'text', 'Tool One Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(266, 21, 'tool_two_title', 'text', 'Tool Two Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(267, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(268, 27, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(269, 27, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(270, 27, 'plugin_one_title', 'text', 'Plugin One Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(271, 27, 'plugin_image_one', 'image', 'Plugin Image One', 0, 1, 1, 1, 1, 1, '{}', 5),
(272, 27, 'plugin_one_link', 'text', 'Plugin One Link', 0, 1, 1, 1, 1, 1, '{}', 6),
(273, 27, 'plugin_two_title', 'text', 'Plugin Two Title', 0, 1, 1, 1, 1, 1, '{}', 7),
(274, 27, 'plugin_image_two', 'image', 'Plugin Image Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(275, 27, 'plugin_two_link', 'text', 'Plugin Two Link', 0, 1, 1, 1, 1, 1, '{}', 9),
(276, 27, 'plugin_three_title', 'text', 'Plugin Three Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(277, 27, 'plugin_image_three', 'image', 'Plugin Image Three', 0, 1, 1, 1, 1, 1, '{}', 11),
(278, 27, 'plugin_three_link', 'text', 'Plugin Three Link', 0, 1, 1, 1, 1, 1, '{}', 12),
(279, 27, 'title_two', 'text', 'Title Two', 0, 1, 1, 1, 1, 1, '{}', 13),
(280, 27, 'title_two_link', 'text', 'Title Two Link', 0, 1, 1, 1, 1, 1, '{}', 14),
(281, 27, 'gina_text', 'text', 'Gina Text', 0, 1, 1, 1, 1, 1, '{}', 15),
(282, 27, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 16),
(283, 27, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 17),
(284, 27, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 18),
(285, 27, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 19),
(286, 27, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 20),
(287, 31, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(288, 31, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(289, 31, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(290, 31, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(291, 31, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(292, 33, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(293, 33, 'category', 'select_dropdown', 'Category', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Ambulatory Care\",\"2\":\"Primary Care\",\"3\":\"Health System\",\"4\":\"Long Term Care\"}}', 2),
(294, 33, 'item_code', 'text', 'Item Code', 0, 1, 1, 1, 1, 1, '{}', 5),
(295, 33, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(296, 33, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(297, 33, 'pretty_link', 'text', 'Pretty Link', 0, 1, 1, 1, 1, 1, '{}', 8),
(298, 33, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 9),
(299, 33, 'customize_image', 'image', 'Customize Image', 0, 1, 1, 1, 1, 1, '{}', 10),
(300, 33, 'html', 'text', 'Html', 0, 0, 0, 0, 0, 0, '{}', 11),
(301, 33, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 12),
(302, 33, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(303, 33, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 19),
(304, 33, 'api_title', 'text', 'Api Title', 0, 1, 1, 1, 1, 1, '{}', 18),
(305, 35, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(306, 35, 'section', 'text', 'Section', 0, 1, 1, 1, 1, 1, '{}', 2),
(307, 35, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(308, 35, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(309, 36, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(310, 36, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(311, 36, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 3),
(312, 36, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(313, 36, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(315, 36, 'sections', 'select_multiple', 'Sections', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Ambulatory Care\",\"2\":\"Primary Care\",\"3\":\"Health System\",\"4\":\"Long Term Care\"}}', 3),
(316, 37, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(317, 37, 'type', 'text', 'Type', 0, 1, 1, 1, 1, 1, '{}', 2),
(318, 37, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"Inactive\",\"1\":\"Active\"}}', 4),
(319, 37, 'sections', 'select_multiple', 'Sections', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"1\":\"Ambulatory Care\",\"2\":\"Primary Care\",\"3\":\"Health System\",\"4\":\"Long Term Care\"}}', 3),
(320, 37, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(321, 37, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(322, 33, 'has_spanish', 'select_dropdown', 'Has Spanish', 0, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}', 14),
(323, 33, 'spanish_image', 'image', 'Spanish Image', 0, 1, 1, 1, 1, 1, '{}', 15),
(324, 33, 'spanish_pdf', 'file', 'Spanish Pdf', 0, 1, 1, 1, 1, 1, '{}', 16),
(325, 33, 'healthtool_belongstomany_targetaudience_relationship', 'relationship', 'targetaudiences', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Targetaudience\",\"table\":\"targetaudiences\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"healthtool_targetaudiences\",\"pivot\":\"1\",\"taggable\":\"on\"}', 3),
(326, 33, 'healthtool_belongstomany_patienttype_relationship', 'relationship', 'patienttypes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Patienttype\",\"table\":\"patienttypes\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"type\",\"pivot_table\":\"healthtool_paitienttype\",\"pivot\":\"1\",\"taggable\":\"on\"}', 4),
(327, 33, 'api_id', 'text', 'Api Id', 0, 1, 1, 1, 1, 1, '{}', 17);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-11-08 08:41:06', '2021-11-08 08:41:06'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-11-08 08:41:06', '2021-11-08 08:41:06'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-11-08 08:41:06', '2021-11-08 08:41:06'),
(4, 'ace_pathways', 'ace-pathways', 'Ace Pathway', 'Ace Pathways', 'voyager-dot', 'App\\AcePathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(5, 'ace_care_pathways', 'ace-care-pathways', 'Ace Care Pathway', 'Ace Care Pathways', 'voyager-dot', 'App\\AceCarePathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(6, 'ace_ehr_plugins_page', 'ace-ehr-plugins-page', 'Ace Ehr Plugins Page', 'Ace Ehr Plugins Pages', 'voyager-dot', 'App\\AceEhrPluginsPage', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(7, 'ace_health_systems', 'ace-health-systems', 'Ace Health System', 'Ace Health Systems', 'voyager-dot', 'App\\AceHealthSystem', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 02:08:05', '2021-11-22 02:11:48'),
(8, 'ace_long_term_care_pathways', 'ace-long-term-care-pathways', 'Ace Long Term Care Pathway', 'Ace Long Term Care Pathways', 'voyager-dot', 'App\\AceLongTermCarePathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(9, 'ace_long_term_care_d_pathways', 'ace-long-term-care-d-pathways', 'Ace Long Term Care D Pathway', 'Ace Long Term Care D Pathways', 'voyager-dot', 'App\\AceLongTermCareDPathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 04:33:20', '2021-11-09 04:33:20'),
(10, 'ace_long_terms_ehr_plugins', 'ace-long-terms-ehr-plugins', 'Ace Long Terms Ehr Plugin', 'Ace Long Terms Ehr Plugins', 'voyager-dot', 'App\\AceLongTermsEhrPlugin', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 04:44:07', '2021-11-09 04:45:11'),
(11, 'ace_long_term_landing', 'ace-long-term-landing', 'Ace Long Term Landing', 'Ace Long Term Landings', 'voyager-dot', 'App\\AceLongTermLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 04:53:49', '2021-11-09 04:59:12'),
(12, 'ace_long_term_landings', 'ace-long-term-landings', 'Ace Long Term Landing', 'Ace Long Term Landings', 'voyager-dot', 'App\\AceLongTermLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 05:02:23', '2021-11-22 04:21:09'),
(13, 'ace_guidelines', 'ace-guidelines', 'Ace Guideline', 'Ace Guidelines', 'voyager-dot', 'App\\AceGuideline', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(14, 'ace_guidelines_links', 'ace-guidelines-links', 'Ace Guidelines Link', 'Ace Guidelines Links', 'voyager-dot', 'App\\AceGuidelinesLink', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(15, 'ace_quality_measures', 'ace-quality-measures', 'Ace Quality Measure', 'Ace Quality Measures', 'voyager-dot', 'App\\AceQualityMeasure', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(16, 'lhn_annual_trend_reports', 'lhn-annual-trend-reports', 'Lhn Annual Trend Report', 'Annual Trend Reports', 'voyager-dot', 'App\\LhnAnnualTrendReport', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:48:03', '2021-11-09 05:48:03'),
(17, 'lhn_ambulatory_cares_landings', 'lhn-ambulatory-cares-landings', 'Lhn Ambulatory Cares Landing', 'Lhn Ambulatory Cares Landings', 'voyager-dot', 'App\\LhnAmbulatoryCaresLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 05:56:41', '2021-11-22 07:53:05'),
(18, 'lhn_ambulatorycare_banners', 'lhn-ambulatorycare-banners', 'Lhn Ambulatorycare Banner', 'Lhn Ambulatorycare Banners', 'voyager-dot', 'App\\LhnAmbulatorycareBanner', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(19, 'lhn_ambulatory_coord_cares', 'lhn-ambulatory-coord-cares', 'Lhn Ambulatory Coord Care', 'Ambulatory Coord Cares', 'voyager-dot', 'App\\LhnAmbulatoryCoordCare', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(20, 'lhn_ambulatory_define_episodes', 'lhn-ambulatory-define-episodes', 'Lhn Ambulatory Define Episode', 'Ambulatory Define Episodes', 'voyager-dot', 'App\\LhnAmbulatoryDefineEpisode', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 06:19:23', '2021-11-23 04:07:38'),
(21, 'lhn_amb_screen_patients', 'lhn-amb-screen-patients', 'Lhn Amb Screen Patient', 'Lhn Amb Screen Patients', 'voyager-dot', 'App\\LhnAmbScreenPatient', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 06:32:20', '2021-11-23 02:08:45'),
(22, 'lhn_primary_care_landings', 'lhn-primary-care-landings', 'Primary Care Landing', 'Primary Care Landings', 'voyager-dot', 'App\\LhnPrimaryCareLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 06:41:36', '2021-11-22 09:17:16'),
(23, 'lhn_primary_care_banners', 'lhn-primary-care-banners', 'Primary Care Banner', 'Primary Care Banners', 'voyager-dot', 'App\\LhnPrimaryCareBanner', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(24, 'lhn_primarycare_raisebars', 'lhn-primarycare-raisebars', 'Primary Care Raisebar', 'Primary Care Raisebars', 'voyager-dot', 'App\\LhnPrimarycareRaisebar', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(25, 'lhn_primcare_wres_mons', 'lhn-primcare-wres-mons', 'Primary Care Wrestling The Monster', 'Primary Care Wrestling The Monsters', 'voyager-dot', 'App\\LhnPrimcareWresMon', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(26, 'main_landings', 'main-landings', 'Main Landing', 'Main Landings', 'voyager-world', 'App\\MainLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(27, 'ace_health_system_ehr_plugins', 'ace-health-system-ehr-plugins', 'Ace Health System Ehr Plugin', 'Ace Health System Ehr Plugins', NULL, 'App\\AceHealthSystemEhrPlugin', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-24 02:12:24', '2021-11-24 02:12:24'),
(30, 'ace_popup', 'ace-popup', 'Ace Popup', 'Ace Popups', NULL, 'App\\AcePopup', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-24 07:10:38', '2021-11-24 07:10:38'),
(31, 'ace_popups', 'ace-popups', 'Ace Popup', 'Ace Popups', NULL, 'App\\AcePopup', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-24 07:15:51', '2021-11-24 07:17:06'),
(33, 'healthtools', 'healthtools', 'Healthtool', 'Healthtools', 'voyager-dot', 'App\\Healthtool', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-25 08:13:44', '2021-11-30 05:30:34'),
(34, 'website_section', 'website-section', 'Website Section', 'Website Sections', 'voyager-dot', 'App\\WebsiteSection', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-29 05:55:46', '2021-11-29 05:55:46'),
(35, 'website_sections', 'website-sections', 'Website Section', 'Website Sections', 'voyager-dot', 'App\\WebsiteSection', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-29 05:58:16', '2021-11-29 05:58:16'),
(36, 'targetaudiences', 'targetaudiences', 'Targetaudience', 'Targetaudiences', 'voyager-dot', 'App\\Targetaudience', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-29 06:03:59', '2021-11-29 06:23:14'),
(37, 'patienttypes', 'patienttypes', 'Patienttype', 'Patient Types', 'voyager-dot', 'App\\Patienttype', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-29 06:29:37', '2021-11-29 06:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `healthtools`
--

CREATE TABLE `healthtools` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pretty_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customize_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_spanish` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spanish_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spanish_pdf` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `healthtools`
--

INSERT INTO `healthtools` (`id`, `category`, `item_code`, `title`, `description`, `pretty_link`, `image`, `customize_image`, `html`, `created_at`, `updated_at`, `status`, `api_id`, `api_title`, `has_spanish`, `spanish_image`, `spanish_pdf`) VALUES
(19, '1', 'HED.0170.USA.19', 'Counseling Your Patients With Hepatic Encephalopathy About Driving', 'This tool discusses how patients with HE have limited navigational skills and need to be counseled on their driving ability.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/6b29eba3b76ebdf37d64cdbbf84eef5f', 'healthtools/July2020/ad5CsV9KnBE4GKsNQOZt.jpg', 'healthtools/July2020/N2djZoRxPFmKaZ5cQc0A.jpg', '<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Counseling Your Patients With Hepatic Encephalopathy About Driving</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>Patients with hepatic encephalopathy (HE) often have deficits in driving ability, which can lead to poor driving outcomes. <sup>1</sup></h3>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p>One study demonstrated a significantly higher rate of crashes in cirrhotic patients with minimal hepatic encephalopathy diagnosed using an inhibitory control test (MHE ICT) compared to cirrhotic patients without MHE ICT (16% vs 4%, respectively).<sup>2</sup></p>\r\n<p>Driving is an important daily function, and it often determines whether a patient can be employed and self-sufficient in activities of daily life. The inability to drive can have an effect on quality of life.<sup>1</sup></p>\r\n<p>Encourage your patients to discuss their lifestyle and goals so you understand the part driving plays in their daily life. Driving may be a very important factor in their freedom and/or social life, or they may need to drive to get to work. If you determine they should no longer be driving, be sure to offer them a variety of alternative solutions for getting around.</p>\r\n<p>Although some patients with HE may be unfit to drive, it is not necessary to assume the same about all patients with HE. There are no guidelines about whether patients with HE can drive safely or not; however, there are factors to consider when counseling your patients.</p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<h4>Considerations</h4>\r\n<p>Patients with HE may have defects in the following<sup>1</sup>:</p>\r\n<ul class=\"list-1\">\r\n<li>Visuospatial assessment</li>\r\n<li>Attention</li>\r\n<li>Response inhibition</li>\r\n<li>Working memory</li>\r\n<li>Impaired speed of information processing</li>\r\n<li>Psychomotor speed</li>\r\n</ul>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p>These patients have poor navigational skills, get fatigued easily, and demonstrate poor insight into their driving deficits. The combination of all these can lead them to have poor driving skills, leading to traffic violations and crashes as demonstrated not only in simulation testing but also in real-life driving events.<sup>1</sup></p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec\">\r\n<h4>Steps to take</h4>\r\n<p>There are steps you can take to help keep your patients safe.</p>\r\n<ol class=\"list-2\">\r\n<li>Get an idea of the patient\'s driving history prior to diagnosis so you can assess the effect HE is having on his/her ability to drive.</li>\r\n<li>Take a driving history from the patient and caregiver at every appointment. (NOTE: Patients with HE may forget or lack insight into their driving issues, so ongoing communication about recent driving history may increase the receptivity of counseling on this sensitive issue.)<sup>1</sup></li>\r\n<li>Focus counseling efforts more on patients with recent history of traffic crashes and history of alcohol cessation within the last year.<sup>1</sup></li>\r\n</ol>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<h4>The best course of action</h4>\r\n<p>As a physician, you will need to use your best judgment and maintain a balance between your ethical duty to your patient and society. If you are concerned about a patient, the best course of action is to<sup>1</sup>:</p>\r\n<ul class=\"list-1\">\r\n<li>Ask the patient to stop driving</li>\r\n<li>Involve the caregiver in the discussion</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"normal-banner text-center\">\r\n<h5>The final authority for such determination lies with the state <br class=\"br-850-none\" />driving licensing agencies.</h5>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\"><strong>References:</strong><br /><strong>1.</strong> Shaw J, Bajaj JS. Covert hepatic encephalopathy: can my patient drive? <em>J Clin Gastroenterol</em>. 2017;51(2):118. <strong>2.</strong> Bajaj JS, Saeian K, Schuert CM, et al. Minimal hepatic encephalopathy is associated with motor vehicle crashes: the reality beyond the driving test. <em>Hepatology</em>. 2009;50(4):1175-1183.</p>\r\n<div class=\"mb-10\">&nbsp;</div>\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:34:00', '2020-07-30 17:06:07', '1', NULL, NULL, NULL, NULL, NULL),
(20, NULL, 'HED.0166.USA.19', 'Diagnosing Hepatic Encephalopathy in Patients With Liver Disease', 'Using this tool while reviewing your patient’s medical history and symptoms and performing a thorough clinical exam will help rule out or confirm HE.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/b62198c1d437e987e6b4538d3fe7c301', 'healthtools/July2020/sUhx5jdAfCcJS3A0yQyP.jpg', 'healthtools/July2020/B2Q4SYyly7HDlCfC8tVQ.jpg', '<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Diagnosing Hepatic Encephalopathy (HE)<br />in Patients With Liver Disease</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>An HE diagnosis is often referred to as a diagnosis of exclusion, since there is not one single test to help make a proper diagnosis.<sup>1</sup></h3>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p>It is recommended that you and your staff <strong class=\"clr\">conduct a thorough evaluation of your patient&rsquo;s medical history, recognize the common symptoms of HE, and perform a clinical examination that includes a range of simple tests and more advanced psychometric and neurophysiological tools.</strong><sup>1</sup></p>\r\n</div>\r\n<div class=\"flex-center cmb-mb\">\r\n<div class=\"w50 w-768-100 mb-768-20\">\r\n<div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls/img/diagnostic-workup.jpg\" alt=\"Diagnostic Workup\" /></div>\r\n</div>\r\n<div class=\"w50 w-768-100\">\r\n<ul class=\"list-1\">\r\n<li>HE severity is graded using the West Haven Criteria. Overt HE (OHE) is more severe than covert HE (CHE).<sup>2</sup></li>\r\n<li>Diagnosing the progression of CHE to OHE early on can impact a patient&rsquo;s quality of life.<sup>2</sup></li>\r\n<li>A clinical diagnosis of OHE can be determined by altered mental status and impaired neuromotor function.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec bg-sec-2\">\r\n<h4 class=\"text-center\">Patients with cirrhosis and 1 of the following conditions are at a higher risk for HE<sup>1</sup>:</h4>\r\n<div class=\"mb-15\">&nbsp;</div>\r\n<ul class=\"list-1\">\r\n<li>Portal hypertension</li>\r\n<li>Renal failure</li>\r\n<li>Transvenous intrahepatic portosystemic shunt (TIPS) placement</li>\r\n<li>Hyponatremia/refractory <br />ascites</li>\r\n<li>Sarcopenia</li>\r\n<li>Diabetes mellitus</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"w50 w-768-100 cmb-mb\">\r\n<div class=\"bg-title\">\r\n<h3>Precipitating factors of OHE<sup>1</sup></h3>\r\n</div>\r\n<ul class=\"list-1\">\r\n<li>Gastrointestinal bleeding</li>\r\n<li>Infection: urinary tract infection (UTI), spontaneous bacterial peritonitis (SBP), bacteremia</li>\r\n<li>Certain medications that may impact the nervous system, such as sedatives, pain medications, psychoactive tranquilizers</li>\r\n<li>Electrolyte abnormalities: hypernatremia, hyperglycemia</li>\r\n<li>Renal failure</li>\r\n<li>Dehydration</li>\r\n<li>Dietary</li>\r\n<li>Medication noncompliance</li>\r\n</ul>\r\n</div>\r\n<div class=\"w50 w-768-100 cmb-mb\">\r\n<div class=\"bg-title\">\r\n<h3>Neurologic manifestations of OHE<sup>1</sup></h3>\r\n</div>\r\n<h4>Common</h4>\r\n<ul class=\"list-1\">\r\n<li>Confusion or coma</li>\r\n<li>Asterixis</li>\r\n<li>Loss of fine motor skills</li>\r\n<li>Hyperreflexia</li>\r\n</ul>\r\n<h4>Less Common</h4>\r\n<ul class=\"list-1\">\r\n<li>Cognitive deficits detected by special testing</li>\r\n<li>Babinski sign</li>\r\n<li>Slow, monotonous speech</li>\r\n<li>Extrapyramidal-type movement disorders</li>\r\n<li>Clonus</li>\r\n<li>Decerebrate posturing</li>\r\n<li>Decorticate posturing</li>\r\n<li>Hyperventilation</li>\r\n<li>Seizures</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<h4>Tests that help diagnose and manage HE</h4>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"myTableBox myTableBox-2\"><!-- start -->\r\n<div class=\"myTable myTableHdn\">\r\n<div class=\"cell\">\r\n<h5>Testing options</h5>\r\n</div>\r\n<div class=\"cell\">\r\n<h5>Description</h5>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>Stroop Test</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>Stroop tests evaluate mental speed and flexibility through a combination of ink colors and words to determine a person&rsquo;s cognitive abilities. Not applicable for color-blind individuals.<sup>1</sup></p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>PHES&mdash;The Psychometric Hepatic Encephalopathy Score (PHES)</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>This is a series of 5 tests: number connection test-A (NCT-A), number connection test-B (NCT-B), serial dotting test (SDT), line tracing test (LTT), and digital symbol test (DST). Can be used to assess motor speed, motor accuracy, concentration, attention, visual perception, visual-spatial orientation, visual construction, and memory.<sup>1</sup></p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>Child-Turcotte-Pugh (CTP) Calculator</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>Use this interactive calculator to estimate the severity of cirrhosis.<sup>4</sup></p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>MELD Calculator </strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>This model for end-stage liver disease (MELD) calculator uses the international normalized ratio time (INR), the serum creatinine, and serum bilirubin values in order to provide a score on the severity of the chronic liver disease.<sup>4</sup></p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>Imaging tests</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>Magnetic resonance imaging (MRI) and computerized tomography (CT) scans as well as an electroencephalogram (EEG) may look at changes in the brain as part of the diagnostic workup.<sup>5</sup></p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>Lab testing</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>In patients with cirrhosis and suspected HE, lab testing may help identify precipitating factors such as ammonia levels, gastrointestinal bleeding, renal failure, drug and alcohol use, etc.<sup>1</sup></p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>Glasgow Coma Scale</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>For patients with significantly altered mental status, the Glasgow Coma Scale may provide additional diagnostic insight.</p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>West Haven Criteria</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>Considered the gold standard, West Haven Criteria is a clinical scale used to analyze the severity of HE.<sup>5</sup></p>\r\n</div>\r\n</div>\r\n<!-- end --> <!-- start -->\r\n<div class=\"myTable\">\r\n<div class=\"cell\">\r\n<p><strong>Caregiver questionnaire</strong></p>\r\n</div>\r\n<div class=\"cell\">\r\n<p>Patients with HE might not even know they have it.<sup>3</sup> Use a caregiver intake questionnaire to ask your patient&rsquo;s caregiver questions about their loved one&rsquo;s current mental and physical condition.</p>\r\n</div>\r\n</div>\r\n<!-- end --></div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\"><strong>References:</strong><br /><strong>1.</strong> Reau NS, Brown RX, Flamm SL, Poordad F. A step-by-step approach to the diagnosis and management of hepatic encephalopathy in the United States. <em>Gastroenterol Hepatol</em>. 2016;12(12 Suppl 5):1-20. <strong>2.</strong> Nabi E, Bajaj JS. Useful tests for hepatic encephalopathy in clinical practice. <em>Curr Gastroenterol Rep</em>. 2013;16(1):362. <strong>3.</strong> US Department of Veterans Affairs website. https://www.hepatitis.va.gov/cirrhosis/background/child-pugh-calculator.asp. Child-Turcotte-Pugh calculator. Accessed December 4, 2019. <strong>4.</strong> MELD Score (Model for end-stage liver disease, 12 and older) website. https://www.mdcalc.com/meld-score-model-end-stage-liver-disease-12-older. Accessed December 4, 2019. <strong>5.</strong> Vilstrup H, Amodio P, Bajaj J, et al. <em>Hepatic Encephalopathy in Chronic Liver Disease: 2014 Practice Guideline by AASLD and EASL</em>. Alexandria, VA: American Association for the Study of Liver Disease; 2014. https://www.aasld.org/sites/default/files/2019-06/141022_AASLD_Guideline_Encephalopathy_4UFd_2015.pdf. Accessed December 4, 2019.</p>\r\n<div class=\"mb-10\">&nbsp;</div>\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:34:00', '2020-07-30 17:06:58', '1', NULL, NULL, NULL, NULL, NULL),
(21, NULL, 'HED.0155.USA.19', 'Assess HE With the Stroop Test', 'This 5-minute test assesses cognitive processing and provides valuable screening information on brain dysfunction, cognition, and psychopathology.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/e5583f71a957dc8428d8ddc5f874d991', 'healthtools/July2020/lPAsP7pUlmSWTCHGM5gc.jpg', 'healthtools/July2020/xHqMu7zlmazIeQrmq1Xf.jpg', '<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Assess HE With the Stroop Test</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>The Stroop Test is designed to assess psychomotor speed, cognitive flexibility, and psychopathology in patients suspected of having hepatic encephalopathy (HE) who present with an altered mental status.</h3>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p>The fastest and easiest way to administer the Stroop Test is with the <strong>EncephalApp Stroop App.</strong></p>\r\n<p>The 5-minute test asks patients to identify the color of hashtags (####) that appear on the screen, and then identify the color of the text of words written on the screen. The second task is more challenging because each word spells a color, but the words are not in the same color that the word spells. For example, the word RED may be in green text. The correct answer would be green.</p>\r\n<p>The <strong>EncephalApp Stroop App</strong> has been evaluated for the diagnosis of covert/minimal HE and the prediction of overt HE development in Virginia Commonwealth University, Richmond Virginia Medical Center, University of Arkansas Medical Center, and Cleveland Clinic.</p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec\">\r\n<h4>To get the app:</h4>\r\n<ul class=\"list-1\">\r\n<li>Go to <a href=\"http://encephalapp.com\" target=\"_blank\" rel=\"noopener\">encephalapp.com</a>.</li>\r\n</ul>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<h4>To use the app:</h4>\r\n<ul class=\"list-1\">\r\n<li>Watch the instructional video at <a href=\"http://www.chronicliverdisease.org/webcasts/EncephalApp/Stroop_Instructions/\" target=\"_blank\" rel=\"noopener\">chronicliverdisease.org/webcasts/encephalapp/stroop_instructions</a>.</li>\r\n<li>Ensure that your patient is alert and oriented without active neurological issues.</li>\r\n<li>Ensure that your patient does not have red-green color blindness. (Several apps are available to check for this, including the pseudochromatic Ishihara test app.)</li>\r\n<li>Ensure that your patient understands English.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:35:00', '2020-07-29 20:32:45', '1', NULL, NULL, NULL, NULL, NULL),
(22, NULL, 'HED.0161.USA.19', 'Discuss Patient Journaling With Your Patients', 'Encourage your patients to talk to you about their liver disease, track any symptoms, and write down their concerns.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/315b49c2a8ca95d3f7092c5e62d0d679', 'healthtools/July2020/0wcwDeN0EmIf3y4Fulu0.jpg', 'healthtools/July2020/euNJ51kJnUMDySgLeVQl.jpg', '<p>&nbsp;</p>\r\n<!-- Sub Banner -->\r\n<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Discuss Patient Journaling With Your Patients<br />and Their Caregivers</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"clr\"><strong>It is important that your patients learn to manage their liver disease.</strong> You can help by encouraging them to write down specific information about their diet, symptoms, and changes that occur in between appointments.</p>\r\n<p>When patients come in for their next appointment, have a conversation about what they wrote down and experienced since their last visit. This information can open up a 2-way discussion between you and your patient&mdash;shared decision making.</p>\r\n<p>Shared decision making balances your expertise with specific concerns from your patients. It is a process that encourages you and your patient to participate in collaborative dialogues.</p>\r\n<p>Provide your patients with the Patient Journaling health tool to write down information. Inform them to be specific and to bring the chart with them for their next appointment.</p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec\">\r\n<h4>Ask your patients to keep track of the following:</h4>\r\n<ul class=\"list-1\">\r\n<li>Illness</li>\r\n<li>Weight gain or loss</li>\r\n<li>Changes in diet</li>\r\n<li>New symptoms (including side effects from medications)</li>\r\n<li>Any new medications or supplements</li>\r\n<li>Treatment difficulties</li>\r\n<li>Number of bowel movements per day</li>\r\n</ul>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p class=\"clr\"><strong>The more you can encourage your patients to express their symptoms, concerns, lifestyle, and what is important to them, the more you will be able to work together to help manage their liver disease.</strong></p>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"normal-banner text-center\">\r\n<h5>Patient journaling may assist in your discussion with your patients and get them more comfortable speaking with you.</h5>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:35:00', '2020-07-30 18:10:23', '1', NULL, NULL, NULL, NULL, NULL),
(23, NULL, 'HED.0171.USA.19', 'Goal Setting When You Have Chronic Liver Disease', 'This tool provides patients with information on the importance of setting goals and communicating with their doctor.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/e66ac73e8dff9915526bc2c0cd6f6e39', 'healthtools/July2020/vwB42ERhBvFgmeaEAc7W.jpg', 'healthtools/July2020/OsmzcdFJNkJ3lgAVHcP1.jpg', '<p>&nbsp;</p>\r\n<!-- Sub Banner -->\r\n<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Goal Setting When You Have Chronic Liver Disease</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>You have been diagnosed with a form of chronic liver disease (CLD). It is important during this time to set goals that will help you work toward what is important to you.</h3>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p>Shared decision making is one way to talk with your doctor about your goals. It means you have a 2-way discussion with your doctor. Your doctor respects the goals you have set. You respect the knowledge your doctor has about the management of your CLD. This process of communication may help you and your doctor work better together to support your wellness.</p>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<h4>Take some time to think about your overall health and lifestyle.</h4>\r\n<ul class=\"list-1\">\r\n<li>How are you feeling about your diagnosis?</li>\r\n<li>What goals would you like to set and why?</li>\r\n<li>What obstacles might you face?</li>\r\n<li>Who can help you with your goals?</li>\r\n<li>How will you track progress?</li>\r\n<li>What steps can you take to reach these goals?</li>\r\n<li>What does it mean to you when you accomplish these goals?</li>\r\n<li>How can you work with your doctor (prepare questions beforehand, keep track of new symptoms)?</li>\r\n<li>Do you have hobbies, social activities, and/or people in your life you want to remain healthy and active for?</li>\r\n<li>Do you have a trip or an event (like a wedding) you want to be able to attend in the future?</li>\r\n</ul>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p><strong class=\"clr\">Set goals that work for you and are specific and manageable.</strong> Setting goals that make sense to you and for your lifestyle may help you feel you are part of the plan to manage your CLD and have a sense of control. Your doctor is there to offer guidance that supports your goals and helps you to manage them.</p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec\">\r\n<h4>Here are some lifestyle goals to think about:</h4>\r\n<ul class=\"list-1\">\r\n<li><strong class=\"clr\">Don\'t drink alcohol&mdash;</strong>even if drinking alcohol did not cause your disease. Alcohol may cause further liver damage.</li>\r\n<li><strong class=\"clr\">Eat a diet recommended by your doctor.</strong></li>\r\n<li><strong class=\"clr\">Protect your health by avoiding infection.</strong> Stay away from people who are sick. Wash your hands frequently.</li>\r\n<li><strong class=\"clr\">Be careful when taking over-the-counter medications.</strong> Talk to your doctor about any new medications or supplements.</li>\r\n<li><strong class=\"clr\">Figure out some alternate methods of transportation in case you are unable to drive in the future.</strong> Can you call a taxi? Call a friend? Take a bus?</li>\r\n</ul>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p><strong class=\"clr\">Talk with your doctor about your goals and why they are important to you.</strong></p>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"normal-banner text-center\">\r\n<h5>The progression of CLD can be slowed or even stopped if you work with your doctor to manage your health.</h5>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:35:00', '2020-07-30 17:05:07', '1', NULL, NULL, NULL, NULL, NULL),
(24, NULL, 'HED.0169.USA.19', 'Help Patients Set Goals Using Shared Decision Making', 'This tool provides information for the provider to discuss the importance of goal setting with their patients.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/1b7a2e8fe3e022f5cd8399a88cc39604', 'healthtools/July2020/i1OkCKU88PzHjGrs3KG6.jpg', 'healthtools/July2020/hhzkdVlhatPHiSenIKr1.jpg', '<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Help Patients Set Goals Using <br />Shared Decision Making</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>Communication is important when discussing chronic liver disease (CLD) with your patients. Using shared decision making (SDM) opens the door to communicating with your patients and the importance of goal setting.</h3>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p>Shared decision making balances your clinical knowledge, expertise, and decision-making abilities with your patient\'s goals, preferences, beliefs, and cultural values. This process encourages you and your patient to participate in collaborative dialogues.</p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec bg-sec-2\">\r\n<h4>The Agency for Healthcare Research &amp; Quality (AHRQ) suggests that health care providers use this SHARE approach<sup>1</sup>:</h4>\r\n<div class=\"mb-15\">&nbsp;</div>\r\n<ul class=\"list-4\">\r\n<li>Seek your patient\'s participation.</li>\r\n<li>Help your patient explore and compare treatment options.</li>\r\n<li>Assess your patient\'s values and preferences.</li>\r\n<li>Reach a decision with your patient.</li>\r\n<li>Evaluate your patient\'s decision.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p>Constructive, effective clinician-patient relationships are built on engaging, meaningful, empathetic communication. Not only does this increase patient satisfaction and improve clinical outcomes; it also fulfills an ethical imperative.<sup>2</sup></p>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p>Conversations with your patients about their health, lifestyle, and what is important to them may get them thinking about their goals and make them more comfortable speaking with you.</p>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<h4>Questions to discuss with patients</h4>\r\n<ul class=\"list-1\">\r\n<li>How are you feeling about your diagnosis?</li>\r\n<li>What goals would you like to set and why?</li>\r\n<li>How can I help you with your goals?</li>\r\n<li>What steps will you take to reach these goals?</li>\r\n<li>What does it mean to you when you accomplish these goals?</li>\r\n<li>How can we work together? (Suggest preparing questions before a visit, keeping track of new symptoms, bringing someone during appointments to listen.)</li>\r\n<li>Do you have hobbies, social activities, and/or grandchildren you want to remain healthy and active for?</li>\r\n<li>Do you have a trip or an event (like a wedding) you want to be able to attend in the future?</li>\r\n</ul>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p class=\"clr\">Remind your patients to set goals that work for them. Goals that are specific are easier to manage.</p>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<h4>Quality improvement measures to communicate</h4>\r\n<ul class=\"list-1\">\r\n<li>Don&rsquo;t drink alcohol&mdash;even if drinking alcohol was not what caused your disease. Alcohol may cause further liver damage.</li>\r\n<li>Eat a recommended diet. (Be specific with recommendations to each individual patient based on their specific condition.)</li>\r\n<li>Avoid infection by staying away from people who are sick and washing your hands frequently.</li>\r\n<li>Discuss medications (specific dosing, side effects), including over-the-counter medications (new medication or supplements).</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"normal-banner text-center\">\r\n<h5>Work with your patients on managing their CLD and<br />discuss how the progression of their CLD may be slowed<br />or even stopped if they manage their health.</h5>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\"><strong>References:</strong><br /><strong>1.</strong> Agency for Healthcare Research and Quality. The SHARE Approach&mdash;Essential Steps of Shared Decisionmaking: Quick Reference Guide. https://www.ahrq.gov/health-literacy/curriculum-tools/shareddecisionmaking/tools/tool-1/index.html. Accessed January 2, 2020. <strong>2.</strong> Dunlay SM, Strand JJ.&nbsp;How to discuss goals of care with the patient. <em>Trends Cardiovasc Med</em>. 2016;26(1):36-43. <strong>3.</strong> Saberifiroozi M. Improving quality of care in patient with liver cirrhosis. <em>Middle East J Dig Dis</em>. 2017;9(4):189-200.</p>\r\n<div class=\"mb-10\">&nbsp;</div>\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:37:00', '2020-07-30 17:03:13', '1', NULL, NULL, NULL, NULL, NULL),
(25, NULL, 'HED.0152.USA.19', 'I Have HE', 'This tool can be carried by the patient and/or caregiver to help explain HE behaviors and management to others (including health care providers).', 'https://cc-cms.online/cmsbuilder/page_preview/dist/b348105d4eba1c9a8d607badbddc2558', 'healthtools/July2020/7e1P97RxSLzneYSQTVCB.jpg', 'healthtools/July2020/DLgjyUEgItzsYXbLEkRu.jpg', '<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>I Have HE</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>I have hepatic encephalopathy, also known as HE.</h3>\r\n<p><strong class=\"clr\">I want the people in my life to understand how it may affect my behavior.</strong> HE may affect my mental status. At its worst, HE can put me in a coma.</p>\r\n<p><strong class=\"clr\">HE is a complication from liver disease.</strong> Liver disease can be from a virus, autoimmune disease, blood transfusion (before 1992), genetics, cancer, or substance abuse. Between 30 percent and 40 percent of adults in the United States have liver disease that does not come from drinking alcohol.</p>\r\n<p><strong class=\"clr\">HE attacks happen when toxins like ammonia build up.</strong> I need to take my medication, as prescribed every day, to prevent toxin buildup.</p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p><strong class=\"clr\">I must manage my diet and lifestyle.</strong> Also, I need to avoid the following to help prevent an attack from HE:</p>\r\n<ul class=\"list-1\">\r\n<li>Alcohol</li>\r\n<li>Certain medications, as recommended by my doctor</li>\r\n<li>Dehydration (not getting enough water)</li>\r\n<li>Sudden changes in diet</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p><strong class=\"clr\">If my toxin levels get too high, I can have an attack</strong> If I am having an attack, I may:</p>\r\n<ul class=\"list-1\">\r\n<li>Have changes in my mood or personality</li>\r\n<li>Be confused</li>\r\n<li>Slur my words</li>\r\n<li>Not sleep well</li>\r\n<li>Move slowly</li>\r\n</ul>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p><strong class=\"clr\">If I am having an attack, call my doctor right away.</strong></p>\r\n<div class=\"name-line\">Name:</div>\r\n<div class=\"name-line\">Number:</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p><strong class=\"clr\">Then call my (circle one) relative, friend, case worker.</strong></p>\r\n<div class=\"name-line\">Name:</div>\r\n<div class=\"name-line\">Number:</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p><strong class=\"clr\">Thank you for showing your support and for taking the time to understand HE and the steps I need to take for liver health.</strong></p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p>If you would like to know more about HE, go to the American Liver Foundation website at <a href=\"https://liverfoundation.org/\" target=\"_blank\" rel=\"noopener\">liverfoundation.org.</a></p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:39:00', '2020-07-30 17:01:17', '1', NULL, NULL, NULL, NULL, NULL),
(26, NULL, 'HED.0153.USA.19', 'Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy', 'This tool provides the caregiver with important information about medications for HE and symptoms of the condition.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/32d3566916ad1f4f2896f4722f160c61', 'healthtools/July2020/thi8SwHSARNclDvkujMf.jpg', 'healthtools/July2020/TMrOelguAFLElzbA2c9n.jpg', '<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Importance of Taking Medication As Prescribed for Overt Hepatic Encephalopathy</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>An overt hepatic encephalopathy (OHE) attack will most likely not improve without proper medication and lifestyle modifications. As a caregiver, you will need to monitor your loved one\'s progress at home.</h3>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec\">\r\n<h4>Here are some important things to keep in mind as you help your loved one:</h4>\r\n<ul class=\"list-1\">\r\n<li><strong class=\"clr\">Ensure that your loved one is able to pick up their next prescription.</strong> Ensure the patient can pick up their medicine, have it delivered, or arrange to have it mailed so they don\'t miss any medication as prescribed.</li>\r\n<li><strong class=\"clr\">Monitor your loved one\'s daily medication intake</strong> to ensure that the doctor\'s instructions are being followed. Medication side effects can sometimes cause patients to stop taking their medicine, so staying on top of their daily regimen can help avoid future OHE occurrences. Ask your doctor about any side effects that your loved one may be experiencing.</li>\r\n<li><strong class=\"clr\">Ask your loved one about the frequency of their bowel movements if they are on lactulose</strong> to ensure that the medication is working.</li>\r\n<li><strong class=\"clr\">Have your loved one follow the doctor\'s dietary instructions.</strong></li>\r\n<li><strong class=\"clr\">Ensure that your loved one has the transportation and support needed to get to<br class=\"br-850-none\" />follow-up doctor appointments</strong> if they are unable to drive or travel alone.</li>\r\n<li><strong class=\"clr\">Monitor your loved one</strong> to help them avoid the risk of falling.</li>\r\n<li><strong class=\"clr\">Communicate any concerns that you have about your loved one</strong> to the doctor and other members of the health care team to help minimize potential problems down the road.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"normal-banner text-center\">\r\n<h5><strong>Monitoring your loved one at home is important.</strong></h5>\r\n<h5>Be sure to alert the doctor if you observe any signs<br />or symptoms of OHE to help avoid a future attack.</h5>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:39:00', '2020-07-30 17:02:20', '1', NULL, NULL, NULL, NULL, NULL),
(27, '4', 'HED.0151.USA.19', 'Important Signs and Symptoms for Patients With Chronic Liver Disease', 'Mental and physical symptoms of HE that patients with chronic liver disease should be aware of', 'https://cc-cms.online/cmsbuilder/page_preview/dist/3d8fa9db4991e39de4aa06977dc5b49f', 'healthtools/July2020/qCnnPLxq3oaNW10exYyy.jpg', 'healthtools/July2020/fk9oPBsaXY5kcFhvJYIs.jpg', '<div class=\"sub-banner\"><div class=\"container\"><h5>Important Signs and Symptoms for Patients<br />With Chronic Liver Disease</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h3>Patients and caregivers: know the signs and symptoms of chronic liver disease (CLD) with hepatic encephalopathy (HE).</h3><p>It is important to identify the signs of an episode of HE. By paying close attention to the mental and physical symptoms of HE, you can alert the doctor before things get worse. <strong>If you notice any of the symptoms below, call the doctor immediately.</strong></p></div><div class=\"w50 w-768-100 cmb-mb\"><div class=\"bg-title\"><h4 class=\"mb-0\">Mental symptoms</h4></div><ul class=\"list-3\"><li>Forgetfulness</li><li>Confusion</li><li>Poor judgment</li><li>Extra nervousness or excitement</li><li>Not knowing where you are or where you are going</li><li>Inappropriate behavior</li><li>Severe personality changes</li></ul></div><div class=\"w50 w-768-100 cmb-mb\"><div class=\"bg-title\"><h4 class=\"mb-0\">Physical symptoms</h4></div><ul class=\"list-3\"><li>Change in sleep patterns</li><li>Worsening of handwriting</li><li>Loss of small hand movements</li><li>Tremors or shaking of hands or arms</li><li>Slurred speech</li><li>Slowed or sluggish movement</li><li>Breath with a musty or sweet odor</li><li>Asterixis</li><li><div class=\"name-line w-100\">&nbsp;</div></li></ul></div><div class=\"col-12 cmb-mb\"><div class=\"dateSec\">Date:<div class=\"name-line\">&nbsp;</div>/<div class=\"name-line\">&nbsp;</div>/ 20<div class=\"name-line\">&nbsp;</div></div></div><div class=\"col-12 cmb-mb\"><h4>Asterixis or &ldquo;flapping tremor&rdquo;</h4><p class=\"clr\">is often present in the early to middle stages of HE.</p></div><div class=\"col-12\"><div class=\"row text-center \"><div class=\"w33 w-768-100 cmb-mb\"><div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls/img/hn1.jpg\" alt=\"Img\" /></div><div class=\"mb-20\">&nbsp;</div><p>The patient extends both arms out with wrists extended up and fingers separated.</p><div class=\"mb-20\">&nbsp;</div></div><div class=\"w33 w-768-100 cmb-mb\"><div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls/img/hn2.jpg\" alt=\"Img\" /></div><div class=\"mb-20\">&nbsp;</div><p>The examiner exerts pressure on the fingers, pushing the hands further into extension and then releases.</p><div class=\"mb-20\">&nbsp;</div></div><div class=\"w33 w-768-100 cmb-mb\"><div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls/img/hn3.jpg\" alt=\"Img\" /></div><div class=\"mb-20\">&nbsp;</div><p>The examiner observes the patient&rsquo;s hands for any uncontrollable flapping motion.</p><div class=\"mb-20\">&nbsp;</div></div></div></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5><strong>Try this examination with your loved one with CLD.</strong></h5><h5>Be sure to alert the doctor if you observe any of the signs of asterixis so that they can properly evaluate the patient for HE.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:40:00', '2021-11-29 04:47:45', '1', NULL, NULL, NULL, NULL, NULL),
(28, NULL, 'HED.0159.USA.19', 'Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy', 'This tool helps patients with OHE or their caregivers remember which lifestyle decisions (such as what to eat or drink) can impact liver health.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/67ce8a7c39c698099642387f5bdd9a44', 'healthtools/July2020/7FszWXxBcItd1PHJaWcb.jpg', 'healthtools/July2020/DoAt1PY1Q9nlo2Nsc6Ze.jpg', '<div class=\"sub-banner\">\r\n<div class=\"container\">\r\n<h5>Lifestyle Self-Management for Patients With Overt Hepatic Encephalopathy (OHE)</h5>\r\n</div>\r\n</div>\r\n<!-- // Sub Banner -->\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-12 cmb-mb\">\r\n<h3>Important considerations when managing your OHE</h3>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<p><strong class=\"clr\">Managing your OHE takes teamwork.</strong> You need to have open and honest communication with your caregiver. Be sure to share all treatment information you get from your doctor and discuss ways you need help.</p>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"bg-sec\">\r\n<h4>Your doctor may have specific directions for you to follow:</h4>\r\n<ul class=\"list-1\">\r\n<li>Medications that may need to be taken on a daily basis\r\n<ul>\r\n<li>It is important to take medicine exactly as prescribed and for as long as your doctor told you.</li>\r\n<li>Medicines may help reduce the amount of toxins in your body and make attacks less likely to occur.</li>\r\n</ul>\r\n</li>\r\n<li>Diet restrictions and nutritional needs</li>\r\n<li>Do not drink alcohol\r\n<ul>\r\n<li>Alcohol can damage liver cells. Even a small amount can be harmful.</li>\r\n</ul>\r\n</li>\r\n<li>Whether or not you will be able to continue working</li>\r\n<li>If you are allowed to drive or travel alone to appointments</li>\r\n<li>Activities that you should no longer do alone</li>\r\n</ul>\r\n<div class=\"cmb-mb\">&nbsp;</div>\r\n<h4>Doctor\'s instructions for patient:</h4>\r\n<div class=\"name-line w-100\">&nbsp;</div>\r\n<div class=\"name-line w-100\">&nbsp;</div>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<div class=\"normal-banner text-center\">\r\n<h5><strong>Coping with your emotions</strong></h5>\r\n<h5>Managing your OHE can feel overwhelming and may make you sad or angry. Be sure to openly talk to your caregiver about your feelings. It may also be helpful to talk with a professional counselor.</h5>\r\n</div>\r\n</div>\r\n<div class=\"col-12 cmb-mb\">\r\n<p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p>\r\n</div>\r\n</div>\r\n</div>', '2020-03-12 12:40:00', '2020-07-30 17:00:08', '1', NULL, NULL, NULL, NULL, NULL),
(29, '3', 'HED.0172.USA.19', 'Living With Diabetes and Liver Disease', 'This tool discusses liver diseases and management tips for patients with diabetes.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/1b7d74eb615393905c80280305ddff40', 'healthtools/July2020/n7AVPkFENv17Rix5PcTB.jpg', 'healthtools/July2020/vWEah9PTnt9aDzEoFVKG.jpg', '<div class=\"sub-banner\"><div class=\"container\"><h5>Living With Diabetes and Liver Disease</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h3>It is important to monitor your liver condition when you have diabetes.</h3><div class=\"cmb-mb\">&nbsp;</div><p>Some people with liver disease or type 2 diabetes are at higher risk for developing the other disease. Your chronic liver disease may be caused by hepatitis, non-alcoholic steatohepatitis (NASH), cancer, or a rare disease. Other factors such as drugs, too much alcohol, or poisons may harm the liver.</p><p>Your doctor may recommend an ultrasound examination of your liver when you are first diagnosed with diabetes. Having regular follow-up blood tests monitors your liver function.</p><p>Increased insulin resistance is frequently associated with chronic liver disease. Having diabetes puts you at an increased risk for chronic liver disease and its serious complications, including hepatic encephalopathy (HE).</p><div class=\"cmb-mb\">&nbsp;</div><h4>Hepatitis</h4><p>Chronic liver disease impairs the liver\'s ability to keep blood sugar levels within a normal range.</p><p>Hepatitis C is one of the most common causes of chronic liver disease. If you have hepatitis C (HCV) you have a much higher risk of having type 2 diabetes. And people with diabetes are more likely than most to have HCV. HCV increases the chance that you will have trouble making insulin and your body will have trouble responding to it.</p><div class=\"cmb-mb\">&nbsp;</div><h4>NAFLD</h4><p>If you have diabetes, a buildup of extra fat in liver cells called non-alcoholic fatty liver disease (NAFLD) may develop. This is not caused by alcohol. Your liver normally contains fat, but if more than 5 percent to 10 percent of the weight of the liver is fat, this is called fatty liver (steatosis).</p><p>A more severe form of NAFLD is called non-alcoholic steatohepatitis (NASH). You can have NASH for years before symptoms occur. NASH is one of the leading causes of cirrhosis in adults in the United States.</p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec\"><h4>Tips to manage your liver disease</h4><ul class=\"list-1\"><li>Discuss your diabetes with your doctor and the best ways to manage it.</li><li>Eat a diet recommended by your doctor.</li><li>Lose weight if you are overweight.</li><li>Exercise per your doctor&rsquo;s suggestions.</li><li>Avoid or limit your alcohol intake.</li><li>Tell your doctor about all your medications and supplements. Be sure to always take<br />your medicine exactly as prescribed.</li><li>Work with your doctor to lower your high cholesterol and triglycerides.</li></ul></div></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Talk with your doctor about your diabetes and concerns you may have regarding your liver health.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:41:00', '2021-11-30 06:23:16', '1', '21d93cf67920a7813695219e2e3014b3', NULL, '1', NULL, '[]');
INSERT INTO `healthtools` (`id`, `category`, `item_code`, `title`, `description`, `pretty_link`, `image`, `customize_image`, `html`, `created_at`, `updated_at`, `status`, `api_id`, `api_title`, `has_spanish`, `spanish_image`, `spanish_pdf`) VALUES
(30, '2', 'HED.0160.USA.19', 'Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers', 'Patients will learn why it\'s important to take their HE medications exactly as prescribed by their doctor.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/26238cea46b4f55404c56020a14d3dc9', 'healthtools/July2020/X68nDuNAAsPb57Ht3ynh.jpg', 'healthtools/July2020/vVAqJKPEEPH7GAxjzU3i.jpg', '<div class=\"sub-banner\"><div class=\"container\"><h5>Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><div class=\"bg-sec\"><h4 class=\"normal\"><strong>8</strong> things to remember about hepatic encephalopathy (HE)</h4><div class=\"cmb-mb\">&nbsp;</div><ol class=\"list-2\"><li>HE mainly happens to people who have cirrhosis or other types of liver damage.</li><li>HE is caused by toxins that build up in the blood and reach the brain.</li><li>Symptoms of HE can be both mental and physical.</li><li>HE can start slowly. Symptoms of HE may not be noticed at first.</li><li>HE will not get better on its own. Symptoms will likely get worse without continuous therapy.</li><li>Medications for HE aim to control the disease and keep people out of the hospital. Hospital stays for HE can be lengthy and costly.</li><li>Lactulose and antibiotics are used to help manage HE and prevent toxins from building up.</li><li>With timely and proper maintenance therapy, the progression of HE can be slowed and sometimes stopped.</li></ol></div></div><div class=\"col-12 cmb-mb\"><h3>The importance of managing HE</h3><p>It is unlikely that an HE episode will get better without proper medication. Tell the doctor about any warning signs as soon as you notice them. The doctor may prescribe medicine for HE. If so, it is important that patients take it exactly as prescribed for as long as the doctor instructs.</p></div><div class=\"col-12 cmb-mb\"><h3>How is HE managed?</h3><p>The doctor may have the patient:</p><ul class=\"list-1\"><li><strong class=\"clr\">Take a medication</strong> that can help remove toxins</li><li><strong class=\"clr\">Get treatment</strong> for certain infections</li><li><strong class=\"clr\">Stop taking certain drugs</strong></li><li><strong class=\"clr\">Follow specific instructions</strong> to carefully raise his/her sodium and potassium levels</li><li><strong class=\"clr\">Get treatment</strong> for any urinary blockages</li></ul><div class=\"cmb-mb\">&nbsp;</div><p>A patient with HE may also have to change his/her diet. Follow the doctor&rsquo;s instructions.</p></div><div class=\"col-12 cmb-mb\"><h3>How important is it to stay on HE medication?</h3><p>With ongoing management, HE can be controlled and sometimes even stopped. So, it&rsquo;s very important that patients with HE are taken care of and continue to keep HE symptoms from returning.</p></div><div class=\"col-12 cmb-mb\"><h3>What medications are used to manage HE?</h3><p>There are 2 types of medicine that are used most often to manage HE:</p><p><strong class=\"clr\">Lactulose</strong> is a kind of sugar. It works by causing a person to have more bowel movements. This will help flush toxins out of the system. Lactulose may also help reduce the amount of toxins that are made in the intestines. Lactulose has been shown to help during HE recurrences and to make them less likely to happen. Talk to the patient&rsquo;s doctor about the side effects of lactulose.</p><p><strong class=\"clr\">Antibiotics</strong> stop the growth of certain bacteria that create toxins as food is digested. By reducing bacteria, antibiotics reduce the amount of toxins. There are a few different antibiotics that are used to manage HE. The doctor will choose the one that is best for the patient. Antibiotics may make episodes less likely to occur. Talk to the patient&rsquo;s doctor about whether antibiotics may be right for managing his/her HE.</p></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>It is important to discuss any questions or concerns you have<br />regarding ongoing management of HE with your doctor.</h5><h5>If you discontinue your medication for any reason, contact<br />your doctor immediately.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:42:00', '2021-11-29 04:47:11', '1', NULL, NULL, NULL, NULL, NULL),
(32, '3', 'HED.0163.USA.19', 'Nurse Checklist for Liver Disease and Hepatic Encephalopathy', 'Use this checklist to help provide quality care for patients with HE.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/839517158e05a7d4f2e6424c26daccd7', 'healthtools/July2020/BpiDva6nSw81jzian0SA.jpg', 'healthtools/July2020/QOo4qH8B6lQ2Sf2jMXAl.jpg', '<div class=\"sub-banner\"><div class=\"container\"><h5>Nurse Checklist for Liver Disease and<br />Hepatic Encephalopathy</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h4>Patients with liver failure are at risk for hepatic encephalopathy (HE).</h4><p>Once a patient has been diagnosed with having an HE episode, identify and correct precipitating factors and focus on managing the symptoms, supporting their body systems.<sup>1</sup></p><ul class=\"list-3\"><li>Ask patients and caregivers about previous HE episodes, including episodes of altered mental status, that may not have required hospitalization or been previously documented, such as difficulty driving, accidents, or not knowing the season, month, or day.<sup>2</sup></li><li>If there is a history of HE, reduce the risk of recurrence with secondary prophylaxis.<sup>2</sup></li><li>Patients should be encouraged to take their medicine prescribed by the doctor, understand the importance of eating a healthy diet, and monitor their bowel habits and changes in stools.<sup>3</sup></li></ul></div><div class=\"col-12 cmb-mb\"><h4>When transitioning a patient to a caregiver or another provider, consider these important elements of quality care<sup>1</sup>:</h4><ul class=\"list-3\"><li>Monitor the patient&rsquo;s level of consciousness, blood pressure, volume status, and signs and symptoms.</li><li>Watch for signs of infection and administer antibiotics as needed and ordered.</li><li>Monitor glucose levels for possible hypoglycemia or hyperglycemia.</li><li>Provide nutritional support as ordered.</li><li>Manage ascites, which can affect multiple body systems.</li><li>Watch for confusion, hyperthermia, respiratory and circulatory problems, and increased intracranial pressure.</li></ul></div><div class=\"col-12 cmb-mb\"><h4>Use these 9 competencies related to liver care and a person-centered approach with your patients.</h4><p>Continuous assessment of the patient&rsquo;s behavior and mental status is also important due to the fluctuating nature of HE.<sup>3</sup> Here&rsquo;s how people helping with the care of patients can do both.<sup>3</sup></p><ul class=\"list-3\"><li>Ask questions and actively listen to the patient.</li><li>Be supportive and help the patient perform basic activities.</li><li>Develop, implement, and evaluate a personal care plan for the patient.</li><li>Identify signs of distress and protect the patient from stigma.</li><li>Monitor the patient&rsquo;s food and drink intake, output, and fluid and electrolyte balance.</li><li>Provide and maintain a safe environment for effective communication.</li><li>Frequently assess for early signs of other diseases as well as new or worsening complications of liver disease.</li></ul></div><div class=\"col-12 cmb-mb\"><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\"><strong>References:<br />1.</strong> Lynn SJ. How to help patients with liver failure. <em>Am Nurse Today</em>. 2016;11(9):26-29. <strong>2.</strong> Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014 Practice Guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver. <em>Hepatology</em>. 2014;60(2):715-735. <strong>3.</strong> Royal College of Nursing. <em>Caring for People with Liver Disease including Liver Transplantation: a Competence Framework for Nursing</em>. London: Royal College of Nursing; 2019.</p><div class=\"mb-10\">&nbsp;</div><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div></div>', '2020-03-12 12:42:00', '2021-11-29 04:47:29', '1', NULL, NULL, NULL, NULL, NULL),
(35, '2', 'HED.0173.USA.19', 'Setting Up Medical Alerts on Digital Devices', 'Patient instructions for setting up emergency services on a digital device', 'https://cc-cms.online/cmsbuilder/page_preview/dist/bb5b7e78d2c53c0fc313f7dd491cd55b', 'healthtools/July2020/2rYMf01dVp5CuNaFlR7n.jpg', 'healthtools/July2020/SCETzxihmPzc0FJyodlY.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>Setting Up Medical Alerts on Digital Devices</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h3>A cell phone or watch can be set up to call emergency services quickly when you, or a patient you are caring for, falls or needs immediate medical attention. Once first responders arrive, having your medical information available as a digital medical ID can help you get the best care based on your existing conditions. Read through the steps below to set up a device.</h3><p class=\"clr\">Note: Steps may differ if you have an older phone. Check online for additional information.</p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec\"><h4>Set up your Medical ID on your iPhone</h4><p>Medical ID helps first responders access your critical medical information from the Lock screen, without needing your passcode. They can see information about your medical conditions as well as who to contact in case of an emergency.</p><ol class=\"list-2\"><li>Open the Health app and tap the Summary tab.</li><li>Tap your profile picture in the upper-right corner.</li><li>Under Medical Details, tap Medical ID.</li><li>Tap Edit in the upper-right corner.</li><li>To make your Medical ID available from the Lock screen on your iPhone, turn on Show When Locked. In an emergency, this gives information to people who want to help.</li><li>Enter health information like your medical condition, medications, date of birth, allergies, and blood type.</li><li>Tap Done.</li></ol><div class=\"cmb-mb\">&nbsp;</div><p><strong class=\"clr\">Note to Android users:</strong> Android phones usually don\'t have built-in health apps that allow you to create a Medical ID. You can still create Medical IDs that anyone can see from the lock screen without unlocking the phone. There are free apps you can download that can give you a Medical ID.</p></div></div><div class=\"col-12 cmb-mb\"><h4>Emergency SOS messages for iPhone iOS version 10.2 or newer</h4><p>To access Emergency SOS, press the power button 5 times in a row. The Emergency SOS screen will appear on the phone.</p><p>When making a call with Emergency SOS, the iPhone automatically calls the local emergency number.</p><p>During this time, you have 3 seconds to cancel (in case you were just testing it or if you triggered it accidentally) by pressing the red X on the screen to pause the countdown and selecting \"Stop Calling\" from the menu.</p></div><div class=\"col-12 cmb-mb\"><div class=\"row\"><div class=\"w60 w-640-100 cmb-mb\"><h4>For an iPhone 8 or later</h4><ol class=\"list-2\"><li>Press and hold the side button and one of the volume buttons until the Emergency SOS slider appears.</li><li>Drag the Emergency SOS slider to call emergency services. If you continue to hold down the side button and volume button, instead of dragging the slider, a countdown begins and an alert sounds. If you hold down the buttons until the countdown ends, your iPhone automatically calls emergency services.</li></ol><div class=\"cmb-mb\">&nbsp;</div><h4>For an iPhone 7 or earlier</h4><ol class=\"list-2\"><li>Rapidly press the side (or top) button 5 times. The Emergency SOS slider will appear.</li><li>Drag the Emergency SOS slider to call emergency services.</li></ol><div class=\"cmb-mb\">&nbsp;</div><p>After the call ends, your iPhone sends your emergency contacts a text message with your current location, unless you choose to cancel. If \"Location Services\" is off, it will temporarily turn on. If your location changes, your contacts will get an update, and you\'ll get a notification about 10 minutes later.</p></div><div class=\"w40 w-640-100 cmb-mb\"><div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls//img/mob1.jpg\" alt=\"Diagnostic Workup\" width=\"224\" height=\"450\" /></div></div></div></div><div class=\"col-12 cmb-mb\"><div class=\"bg-title space\"><h3 class=\"text-center\">Use Emergency SOS on your Apple Watch</h3></div><div class=\"cmb-mb\">&nbsp;</div><div class=\"row\"><div class=\"w40 w-640-100 cmb-mb\"><div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls//img/watch1.png\" alt=\"Diagnostic Workup\" width=\"224\" height=\"250\" /></div></div><div class=\"w60 w-640-100 cmb-mb\"><h4>If the Apple Watch doesn\'t have cellular</h4><p>To use Emergency SOS on an Apple Watch that doesn\'t have cellular, your iPhone needs to be nearby. If your iPhone isn\'t nearby, your Apple Watch needs to be connected to a known Wi-Fi network and you must set up Wi-Fi Calling.</p><div class=\"cmb-mb\">&nbsp;</div><h4>How to set up Fall Detection on an Apple Watch</h4><ol class=\"list-2\"><li>Open the <strong>Watch app</strong> on your iPhone.</li><li>Tap <strong>Emergency SOS.</strong></li><li>Toggle <strong>Fall Detection</strong> on. A confirmation window may <br />pop up.</li></ol><div class=\"cmb-mb\">&nbsp;</div><p>Now your Apple Watch will help detect if you\'ve taken a fall. If you do, the Apple Watch will vibrate taps on your wrist, ring an alarm, and show a slider option to call emergency services.</p></div></div></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec text-center\"><h3>Although there is no fall detection alert available on cell phones, there are plenty of fall apps that can easily be added to your phone.</h3></div></div><div class=\"col-12 cmb-mb\"><div class=\"row\"><div class=\"w60 w-640-100 cmb-mb\"><h4>Emergency SOS messages for Samsung Galaxy Android phones</h4><ol class=\"list-2\"><li>Open your phone\'s settings, go to \"Personal,\" select \"Privacy and Emergency,\" and tap \"Send SOS messages.\"</li><li>First, enable the feature by tapping the toggle at the top right. It will prompt you to agree to a disclaimer. Once you accept the terms, you will be able to set up SOS messages.</li><li>Next, click \"Send messages to\" to select up to 4 emergency contacts to receive your emergency alerts. You can add new contacts for this or select from contacts already on your phone.</li><li>And finally, in addition to sending your location, you can choose to enable 2 additional SOS messaging features. <br />&ndash; \"Attach pictures\" enables you to attach photos taken from both the front and rear cameras before the emergency alert is sent. <br />&ndash; \"Attach audio recording\" allows you to attach a 5-second audio recording to the message. <br />When triggered, an emergency message with your location, a picture of your situation, and an audio message will be sent automatically.</li></ol><div class=\"cmb-mb\">&nbsp;</div><p>Once you set SOS messages up, you can send an emergency alert to 4 preselected contacts by pressing the power button on your device 3 times in a row.</p></div><div class=\"w40 w-640-100 cmb-mb\"><div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls/img/mob2.jpg\" alt=\"Diagnostic Workup\" width=\"224\" height=\"411\" /></div></div></div></div><div class=\"col-12 cmb-mb\"><div class=\"bg-title space\"><h3 class=\"text-center\">Emergency SOS messages for Samsung Gear smartwatches</h3></div><div class=\"row\"><div class=\"w40 w-640-100 cmb-mb\"><div class=\"diagnostic-workup-img\"><img src=\"https://liverhealthnow.com/custom_htmls/img/watch2.gif\" alt=\"Diagnostic Workup\" width=\"224\" height=\"222\" /></div></div><div class=\"w60 w-640-100 cmb-mb\"><p>Here\'s how to set it up:</p><ol class=\"list-2\"><li>Open up the Samsung Gear app on your smartwatch.</li><li>Locate and select &ldquo;Send SOS Requests.&rdquo;</li><li>Slide the toggle at the top from Off to On.</li><li>Select at least 1 emergency contact.</li><li>Once you\'re done adding emergency contacts, go back to the main Send SOS Requests menu.</li></ol><div class=\"cmb-mb\">&nbsp;</div><p>Now when you triple-press the home button, your watch will send an SOS alert to your predefined contacts. If you are worried about accidentally sending SOS requests, you can enable a countdown option to add a 5-second wait before a message is sent. Also keep in mind that you may be charged for this SOS feature, depending on your carrier plan.</p></div></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt \">Apple &amp; Samsung device images are registered trademarks of company. <br />https://support.apple.com/en-us/HT207021<br />https://www.gottransition.org/resourceGet.cfm?id=439</p><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:44:00', '2021-11-29 04:47:07', '1', NULL, NULL, NULL, NULL, NULL),
(36, '4', 'HED.0167.USA.19', 'Stages and Types of Liver Disease', 'Patient information on stages of liver disease and associated symptoms.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/cd0243a1b486eb2ddfaf75e967bc976e', 'healthtools/July2020/z6NWi4HbQmJW80KGkrL9.jpg', 'healthtools/July2020/h5s0WJrA1mdVxrElaga0.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>Stages and Types of Liver Disease</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h4 class=\"normal\"><strong>Your doctor may have recently diagnosed you with a liver problem.</strong> Your liver is the second largest organ in your body. It is located on the right side of your body under your rib cage. Your liver is shaped like a football with one side flat and weighs about <br />3 pounds.</h4><div class=\"cmb-mb\">&nbsp;</div><h4 class=\"normal\">Your liver performs many important functions in your body. It helps process what you eat and drink into energy and nutrients. <strong>Anything that keeps your liver from doing its job may put your life in danger.</strong></h4></div><div class=\"col-12 cmb-mb\"><h4>Common causes of liver disease</h4><ul class=\"list-1\"><li>Viruses</li><li>Genetics</li><li>Immune system disorders</li><li>Unhealthy choices</li><li>Reactions to medications or toxins</li></ul><div class=\"cmb-mb\">&nbsp;</div><p>You may have been diagnosed with 1 of the following liver diseases: inflammation/hepatitis, fibrosis, non-alcoholic fatty liver disease (NAFLD), cirrhosis, liver cancer, or chronic liver failure.</p></div><div class=\"col-12 cmb-mb\"><div class=\"blue-banner text-center\"><h5>A healthy liver has the amazing ability to grow back, or regenerate, when it is damaged.</h5></div></div><div class=\"col-12 cmb-mb\"><h4>Inflammation/Hepatitis</h4><p>In the early stage of any liver disease, your liver may become inflamed or swollen. Inflammation shows that your body is trying to fight an infection or heal an injury. An inflamed liver is also known as hepatitis. Hepatitis is the first stage of liver disease. When most other parts of your body become inflamed, you can feel it&mdash;the area becomes hot and painful. However, when your liver becomes tender and enlarged, you may have no discomfort at all. But if the inflammation continues over time, it can start to hurt your liver permanently.</p><p>There are 5 types of hepatitis: A, B, C, D, and E. Hepatitis A and E are often caused by eating or drinking unclean food or water. Hepatitis B, C, and D often occur as a result of internal contact with infected body fluids such as blood or semen. All types of hepatitis cause liver disease.</p><p>If your liver disease is diagnosed and treated successfully at this stage, the inflammation may go away.</p><div class=\"cmb-mb\">&nbsp;</div><h4>Fibrosis</h4><p>Fibrosis is scarring of the liver. When an inflamed liver is left untreated, it will start to scar. As excess scar tissue grows, it replaces healthy liver tissue. Scar tissue cannot do the work that healthy liver tissue can. Scar tissue can keep blood from flowing through your liver. As more scar tissue builds up, your liver may not work as well as it once did. Or, the healthy part of your liver has to work harder to make up for the scarred part. Severe fibrosis leads to cirrhosis.</p><p>There are no symptoms of fibrosis. It is diagnosed by blood tests, image scans, and a liver biopsy. A liver biopsy removes a small piece of the liver, and it is then examined in a lab.</p><p>If your liver disease is diagnosed and treated successfully at this stage, there&rsquo;s still a chance that your liver can heal itself over time.</p><div class=\"cmb-mb\">&nbsp;</div><h4>NAFLD</h4><p>NAFLD is a buildup of extra fat in liver cells. This is not caused by alcohol. Your liver normally contains fat, but if more than 5 percent to 10 percent of the weight of the liver is fat, this is called fatty liver (steatosis).</p><p>A more severe form of NAFLD is called non-alcoholic steatohepatitis (NASH). You can have NASH for years before symptoms occur. NASH is one of the leading causes of cirrhosis in adults in the United States.</p><div class=\"cmb-mb\">&nbsp;</div><h4>Cirrhosis</h4><p>Cirrhosis is the severe scarring of the liver. This is when hard scar tissue replaces soft healthy tissue. It can take many years for liver disease to lead to cirrhosis. As the amount of scar tissue increases, the liver may not be able to perform its jobs.</p><p>If the cause of cirrhosis is not treated, the liver will not be able to do vital functions. Cirrhosis can lead to a number of complications, including liver cancer. In some people, the symptoms of cirrhosis may be the first signs of liver disease.</p><p>Once you&rsquo;ve been diagnosed with cirrhosis, treatment will focus on keeping your condition from getting worse. It may be possible to stop or slow the liver damage. It is important to protect the healthy liver tissue you have left.</p><p>Cirrhosis is diagnosed by symptoms and blood and imaging tests. A liver biopsy may be needed to check how much of the liver has been damaged. During a biopsy, a small piece of liver tissue is removed and studied in the lab. Treatment for cirrhosis depends on the cause and the level of liver damage. The goals of treatment are to prevent further liver damage and reduce complications.</p></div><div class=\"col-12 cmb-mb\"><div class=\"blue-banner text-center\"><h5>It is important that you discuss with your doctor your goals and<br />what is important to you so you can work together to balance <br />and manage your liver disease.</h5></div></div><div class=\"col-12 cmb-mb\"><h4>Liver cancer</h4><p>Liver cancer is the growth and spread of unhealthy cells in the liver. It often only occurs if advanced scarring of the liver is present. Cancer that starts in the liver is called &ldquo;primary&rdquo; liver cancer or hepatocellular carcinoma. Cancer that spreads to the liver from another organ is called &ldquo;metastatic&rdquo; liver cancer. Liver cancer may also occur without extensive scarring, particularly in people with chronic hepatitis B. Often there are no symptoms of liver cancer until it is in an advanced stage.</p><p>Long-term infection with the hepatitis B or C virus is the most common risk factor for liver cancer. Liver cancer is seen more often in men than in women.</p><p>Liver cancer is usually diagnosed by imaging tests and scans, blood tests, and an x-ray of your blood or lymph vessels. A liver biopsy may be needed to check how much of the liver has been damaged.</p><div class=\"cmb-mb\">&nbsp;</div><h4>Chronic liver failure</h4><p>Chronic liver failure indicates the liver has been failing gradually, possibly for years. Acute liver failure occurs suddenly and is often a reaction to poisoning or medication overdose. It may also occur due to acute viral hepatitis or other causes of liver disease. If the liver is failing, a liver transplant may be needed.</p><p>A liver transplant is the process of replacing a sick liver with a donated, healthy liver. Liver transplants require that the blood type and body size of the donor match the person receiving the transplant. Donated livers come from living and non-living donors. Liver transplant surgery usually takes between 4 and 12 hours. Most patients stay in the hospital for up to 3 weeks after surgery.</p><p>Most patients return to a regular lifestyle 6 months to a year after a successful liver transplant. In some patients, the liver disease they had before the transplant comes back. When this happens, they may need treatment or another transplant.</p><p>Regardless of what form of liver disease you may be facing, it is important to communicate with your doctor. Express feelings you may be having and what is important to you. Keep track of any new symptoms you may be having.</p></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Your doctor is there to help you balance your goals, treat, and manage your liver problems.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:45:00', '2021-11-29 04:46:28', '1', NULL, NULL, NULL, NULL, NULL),
(37, '4', 'HED.0164.USA.19', 'Symptoms, Complications, and Management of Cirrhosis', 'This tool helps patients understand the symptoms of cirrhosis, its complications, and how to manage it.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/4e9521766c0d0e74b30d6abf8d7bc91d', 'healthtools/July2020/xQcoTCh8IpZamKv65Ofa.jpg', 'healthtools/July2020/gKYmRHfmAodBjl8N8aWH.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>Symptoms, Complications, <br />and Management of Cirrhosis</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h3>Cirrhosis is the scarring of the liver. This happens when hard scar tissue replaces soft healthy tissue.</h3><div class=\"cmb-mb\">&nbsp;</div><p>If the liver has been infected with a virus, damaged by chemicals or alcohol, or attacked by the body&rsquo;s own immune system, cirrhosis can be the result. Fat build up in the liver that is not caused by alcohol use, is nonalcoholic steatohepatitis (NASH). NASH can cause the liver to swell and also lead to cirrhosis. Identifying and treating Inflammation early may give the liver a chance to heal itself and recover. If the cause of cirrhosis is not treated, blood will not be able to flow through the liver. The liver will then eventually fail.</p><div class=\"cmb-mb\">&nbsp;</div><p>Cirrhosis can lead to a number of complications, including liver cancer. Symptoms of cirrhosis may be the first signs of liver disease.</p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec bg-sec-2 list-hf\"><h4 class=\"text-center\">Symptoms of cirrhosis</h4><div class=\"mb-15\">&nbsp;</div><ul><li>Skin and eyes may take on a yellow color, a condition called jaundice.</li><li>Tiredness</li><li>Nausea</li></ul><ul class=\"small-list\"><li>Weight loss</li><li>Spider-like blood vessels</li><li>Intense itching</li><li>Loss of appetite</li></ul></div></div><div class=\"col-12 cmb-mb\"><p>Once you are diagnosed with cirrhosis, treatment will focus on keeping your condition from getting worse. It is important to follow your doctor&rsquo;s recommendation for diet and medication.</p></div><div class=\"col-12 cmb-mb\"><div class=\"blue-banner text-center\"><h5>It may be possible to stop or slow the liver damage.</h5></div></div><div class=\"col-12 cmb-mb\"><h4>Complications of cirrhosis</h4><ul class=\"list-1\"><li>Enlarged veins in the lower esophagus (esophageal varices) and stomach (gastropathy)</li><li>Enlarged spleen (splenomegaly)</li><li>Stone-like particles in gallbladder and bile duct (gallstones)</li><li>Mental confusion (hepatic encephalopathy)</li><li>Liver cancer (hepatocellular carcinoma)</li><li>Build up of fluid and painful swelling of the legs and abdomen</li><li>Bruising and bleeding easily</li></ul><div class=\"cmb-mb\">&nbsp;</div><p class=\"clr\">Tell your doctor about any warning signs as soon as you notice them and any medications or supplements you are taking.</p><div class=\"cmb-mb\">&nbsp;</div><h4>Managing cirrhosis</h4><p>It is possible to prevent further liver damage with proper management of cirrhosis. Here are<br class=\"br-850-none\" />some tips:</p><ul class=\"list-1\"><li>Maintain a healthy lifestyle (eat a healthy diet and exercise as recommended by your doctor).</li><li>Stop drinking alcohol.</li><li>Discuss with your doctor all of the medications, vitamins, and supplements you take.</li><li>Talk with your doctor about specific vaccinations you may need (hepatitis A and hepatitis B, annual flu, pneumococcal, etc).</li><li>Learn about prevention and how to treat any underlying liver disease (eg, viral hepatitis, NASH).</li></ul></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Your doctor is there to help you best manage and treat the <br class=\"br-850-none\" />cause of your cirrhosis.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:45:00', '2021-11-29 04:46:31', '1', NULL, NULL, NULL, NULL, NULL),
(38, '1', 'HED.0185.USA.19', 'The Importance of Patient Journaling', 'Information and chart to help patients keep track of changes/concerns between doctor visits', 'https://cc-cms.online/cmsbuilder/page_preview/dist/f7896af50f7fa446d86ff29eaf22e04a', 'healthtools/July2020/pKMYSZUCaUSQtiSp2mow.jpg', 'healthtools/July2020/GleYqGuiXioTVYB58040.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>The Importance of Patient Journaling</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><p class=\"clr\">When you have any type of liver disease, it is important to manage your health every day. It is also important to discuss any changes and concerns with your doctor. <strong> An important tool to assist in this process is called shared decision making.</strong></p><p>Shared decision making is a 2-way discussion that is developed between you and your doctor. Shared decision making involves your doctor respecting your choices and listening to how you wish to manage your liver disease. It also is about you respecting the knowledge that your doctor provides to you. Your doctor will give you information about your liver disease and your options. Know that the decisions about your health are yours to make, and your doctor is there to support you with your choices.</p><p>Your role in shared decision making is very important, and your input matters. Remember, you know what is important to you. You know what you are experiencing daily and if you have any new symptoms. The more a doctor understands you, the better the 2 of you will be able to design a plan that you can manage.</p><p>Understanding the shared decision making process and actively discussing options with your doctor helps you to be more involved in your health care. Learning more about your disease and being responsible for your care can play a big part in managing your disease.</p></div><div class=\"col-12 cmb-mb\"><h4>Things to note or write down to discuss with your doctor:</h4><ul class=\"list-1\"><li>Have you been sick?</li><li>Have you gained or lost weight?</li><li>Has your diet changed in a significant way?</li><li>Do you have any new symptoms?</li><li>Have you added any new medications or supplements?</li><li>Is it difficult to follow your treatment plan for any reason?</li></ul></div><div class=\"col-12 cmb-mb\"><p class=\"clr\"><strong>Between visits with your doctor, use the chart below to write down any changes you may have, the date, and any specific details. Bring the chart with you to discuss with your doctor at your next appointment.</strong></p></div><div class=\"col-12 cmb-mb\"><div class=\"myTableBox myTableBox-3\"><!-- start --><div class=\"myTable myTableHdn\"><div class=\"cell\"><h5>Changes/Concern</h5></div><div class=\"cell\"><h5>Type/Name</h5></div><div class=\"cell\"><h5>Date</h5></div><div class=\"cell\"><h5>Notes/Specifics</h5></div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Example: Illness</p></div><div class=\"cell\"><p>Cold</p></div><div class=\"cell\"><p>12/16</p></div><div class=\"cell\"><p>Had fever, chills, and stuffy nose.</p></div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Illness</p></div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Change in weight</p></div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>New medications and/or supplements</p></div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>New symptoms</p></div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Change in eating habits</p></div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Treatment difficulties</p></div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Other</p></div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div><div class=\"cell\">&nbsp;</div></div><!-- end --></div></div><div class=\"col-12 cmb-mb\"><h4>Since my last appointment, I have been:</h4><ul class=\"list-3\"><li>Feeling good about the plan we worked on together and have been making progress with my goals</li><li>Not feeling up to doing things I normally enjoy</li><li>Frustrated with some symptoms that are keeping me from doing things</li></ul></div><div class=\"col-12 cmb-mb\"><p>Work with your doctor using this shared decision making tool to help you manage your<br class=\"br-640-none\" />liver disease.</p></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Talk openly with your doctor about any of your concerns<br />or changes you are experiencing.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:45:00', '2021-11-29 04:46:54', '1', NULL, NULL, NULL, NULL, NULL),
(39, '1', 'HED.0156.USA.19', 'West Haven Criteria', 'Use the chart to help understand your patient’s stage of hepatic encephalopathy.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/0d24495e48dca01c587ded93b470ff4a', 'healthtools/July2020/epmmcHshlVUol8K3biGs.jpg', 'healthtools/July2020/RqwOL9Ak6jvyNpj8XgDF.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>West Haven Criteria</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h4>Use the chart below to grade the severity of your patient\'s hepatic encephalopathy (HE) in accordance with the AASLD/EASL Practice Guidelines.<sup>1</sup></h4></div><div class=\"col-12 cmb-mb\"><div class=\"myTableBox\"><!-- start --><div class=\"myTable myTableHdn\"><div class=\"cell\"><h5>West Haven Criteria</h5></div><div class=\"cell\"><h5>ISHEN</h5></div><div class=\"cell\"><h5>Description</h5></div><div class=\"cell\"><h5>Suggested operative criteria</h5></div><div class=\"cell\"><h5>Comment</h5></div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Unimpaired</p></div><div class=\"cell\"><p>&nbsp;</p></div><div class=\"cell\"><p>No encephalopathy at all, no history of HE</p></div><div class=\"cell\"><p>Tested and proved to be normal</p></div><div class=\"cell\"><p>&nbsp;</p></div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Minimal</p></div><div class=\"cell\"><h6>Covert</h6></div><div class=\"cell\"><p>Psychometric or neuropsychological alterations of tests exploring psychomotor speed/executive functions or neurophysical alterations without clinical evidence of mental change</p></div><div class=\"cell\"><p>Abnormal results or established psychometric or neuropsychological tests without clinical manifestations</p></div><div class=\"cell\"><p>No universal criteria for diagnosis; local standards and expertise required</p></div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Grade I</p></div><div class=\"cell\"><h6>Covert</h6></div><div class=\"cell\"><ul class=\"list-1\"><li>Trivial lack of awareness</li><li>Euphoria or anxiety</li><li>Shortened attention span</li><li>Impairment of addition or subtraction</li><li>Altered sleep rhythm</li></ul></div><div class=\"cell\"><p>Despite oriented in time and space, the patient appears to have some cognitive/ behavioral decay with respect to his or her standard on clinical examination or to the caregivers</p></div><div class=\"cell\"><p>Clinical findings usually not reproducible</p></div></div><!-- end --> <!-- start --><div class=\"myTable myTableHdn myTableFull\"><div class=\"cell\">&nbsp;</div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Grade II</p></div><div class=\"cell\"><h6>Overt</h6></div><div class=\"cell\"><ul class=\"list-1\"><li>Lethargy or apathy</li><li>Disorientation for time</li><li>Obvious personality change</li><li>Inappropriate behavior</li><li>Dyspraxia</li><li>Asterixis</li></ul></div><div class=\"cell\"><p>Disoriented for time (at least 3 of the following are wrong: day of the month, day of the week, month, season, or year) &plusmn; the other mentioned symptoms</p></div><div class=\"cell\"><p>Clinical findings variable but reproducible to some extent</p></div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Grade III</p></div><div class=\"cell\"><h6>Overt</h6></div><div class=\"cell\"><ul class=\"list-1\"><li>Somnolence to semistupor</li><li>Responsive to stimuli</li><li>Confused</li><li>Gross disorientation</li><li>Bizarre behavior</li></ul></div><div class=\"cell\"><p>Disoriented also for space (at least 3 of the following wrongly reported: country, state [or region], city, or place) &plusmn; the other mentioned symptoms</p></div><div class=\"cell\"><p>Clinical findings reproducible to some extent</p></div></div><!-- end --> <!-- start --><div class=\"myTable\"><div class=\"cell\"><p>Grade IV</p></div><div class=\"cell\"><h6>Overt</h6></div><div class=\"cell\"><p>Coma</p></div><div class=\"cell\"><p>Does not respond even to painful stimuli</p></div><div class=\"cell\"><p>Comatose state usually reproducible</p></div></div><!-- end --></div><p class=\"myTableTxt\">ISHEN=International Society for Hepatic Encephalopathy and Nitrogen Metabolism <br />All conditions are required to be related to liver insufficiency and/or portosystemic shunt (PSS).</p></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt link\"><strong>Reference:</strong><br /><strong>1.</strong> Vilstrup H, Amodio P, Bajaj J, et al. <em>Hepatic Encephalopathy in Chronic Liver Disease: 2014 Practice Guideline by AASLD and EASL</em>. Alexandria, VA: American Association for the Study of Liver Disease; 2014. https://www.aasld.org/sites/default/files/2019-06/141022_AASLD_Guideline_Encephalopathy_4UFd_2015.pdf. Accessed November 22, 2019.</p><div class=\"mb-10\">&nbsp;</div><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:45:00', '2021-11-29 04:46:57', '1', NULL, NULL, NULL, NULL, NULL),
(40, '3', 'HED.0157.USA.19', 'What Is Hepatic Encephalopathy?', 'This tool informs patients about how HE can affect their daily life and what they can do to manage the condition.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/ab0cec7d01fad71b46f8e96ff42ea2cb', 'healthtools/July2020/uEFo7qoFDOl6MoJwEH1n.jpg', 'healthtools/July2020/PDON5SkOhPjzTR4i3FlS.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>What Is Hepatic Encephalopathy?</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h3>If you have cirrhosis or another type of liver disease, you are at risk for hepatic encephalopathy (HE).</h3></div><div class=\"col-12 cmb-mb\"><p>HE is a condition that causes worsening of brain function in people with advanced liver disease. When your liver is damaged, it can no longer remove toxic substances from your blood. These toxins build up and travel through your body until they reach your brain, causing mental and physical symptoms.</p><p>HE is diagnosed in 2 forms (covert and overt). Covert symptoms are harder to detect by family and friends, but as the disease progresses and you develop more noticeable symptoms, it becomes overt.</p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec bg-sec-2 list-hf\"><h4 class=\"text-center\">HE can affect your daily life</h4><div class=\"mb-15\">&nbsp;</div><ul class=\"list-1\"><li>Your ability to drive; you may have more traffic violations or accidents</li><li>Forgetfulness and confusion</li><li>Poor judgment</li><li>Not knowing where you are or where you are going</li></ul><ul class=\"list-1  small-list\"><li>Inappropriate behavior or personality changes</li><li>Being extra nervous or excited</li><li>Drowsiness and fatigue</li><li>Shaky hands or slow movements</li><li>Sleep disturbances</li></ul></div></div><div class=\"col-12 cmb-mb\"><h4>Ask your doctor for a plan to help manage your symptoms</h4><p>Your symptoms may keep you from doing everyday things. You may have been admitted to the hospital with your symptoms. It is important you work with your doctor on a plan that allows you to minimize symptoms. This way, you can do the things that are important to you. Following the plan may even help you to avoid hospital visits from HE.</p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec\"><h4>Steps to manage HE</h4><ol class=\"list-2\"><li>Inform your caregiver about HE.</li><li>Know the important signs and symptoms of HE.</li><li>Educate yourself about what triggers HE attacks.</li><li>Take your medication as prescribed.</li><li>Avoid sudden changes in your diet.</li></ol><div class=\"cmb-mb\">&nbsp;</div><p>Tell your doctor about any warning signs as soon as you notice them and any medications or supplements you are taking.</p></div></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>The progression of your HE may be slowed or even stopped if<br class=\"br-850-none\" />you work with your doctor to manage your health.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-03-12 12:46:00', '2021-11-29 04:46:13', '1', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `healthtools` (`id`, `category`, `item_code`, `title`, `description`, `pretty_link`, `image`, `customize_image`, `html`, `created_at`, `updated_at`, `status`, `api_id`, `api_title`, `has_spanish`, `spanish_image`, `spanish_pdf`) VALUES
(41, '3', 'HED.0165.USA.19', 'Cirrhosis and Its Complications', 'This tool provides facts about the symptoms and complications that patients with cirrhosis may face.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/e92ea7814957da3bc9fb9ffafa469734', 'healthtools/July2020/6Ts8KbybKUB9wIJiVE7w.jpg', 'healthtools/July2020/xh25QqBT20Rl4tmnk1JF.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>Cirrhosis and Its Complications</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h3>Cirrhosis is the scarring of the liver. As hard scar tissue replaces soft healthy tissue, the liver eventually stops functioning the way it should.<sup>1</sup></h3></div><div class=\"col-12 cmb-mb\"><p>If the liver has been infected with a virus, damaged by chemicals, or attacked by the body&rsquo;s own immune system, liver disease is the result. Cirrhosis is the result of chronic damage or disease of the liver. Identifying and managing cirrhosis early may stop further damage. If cirrhosis progresses too far, it can be life-threatening.</p></div><div class=\"col-12 cmb-mb\"><h4>Symptoms of cirrhosis<sup>2</sup></h4><ul class=\"list-1\"><li>Loss of appetite</li><li>Tiredness</li><li>Jaundice</li><li>Severe itching</li><li>Weight loss</li><li>Nausea</li><li>Spider-like blood vessels</li></ul></div><div class=\"col-12 cmb-mb\"><p>Cirrhosis results from chronic liver disease (CLD) and is characterized by advanced fibrosis, scarring, and formation of regenerative nodules, which distorts the size and shape of the liver. Patients with cirrhosis are at increased risk of numerous complications.<sup>2,3</sup></p></div><div class=\"col-12 cmb-mb\"><div class=\"blue-banner text-center\"><h5>CLD and cirrhosis are the 11th leading cause of death<br class=\"br-640-none\" />in the United States.<sup>4</sup></h5></div></div><div class=\"col-12 cmb-mb\"><h4>Major complications of cirrhosis include<sup>2</sup>:</h4><ul class=\"list-1\"><li>Varices</li><li>Ascites</li><li>Hepatic encephalopathy (HE)</li><li>Hepatopulmonary hypertension</li><li>Hepatocellular carcinoma</li><li>Hepatorenal syndrome</li><li>Spontaneous bacterial peritonitis</li><li>Coagulation disorders</li></ul></div><div class=\"col-12 cmb-mb\"><h4>The main goals of management of cirrhosis are to<sup>2</sup>:</h4><ul class=\"list-1\"><li>Treat the underlying cause when applicable</li><li>Recognize and treat complications early</li><li>Prevent multiple complications</li></ul></div><div class=\"col-12 cmb-mb\"><h4>Patients with cirrhosis should:</h4><ul class=\"list-1\"><li><strong class=\"clr\">Be monitored for complications</strong>, and when possible, steps should be taken to prevent their occurrence.<sup>3</sup></li><li><strong class=\"clr\">Be immunized against hepatitis A and B.</strong> Other vaccines like influenza, pneumonia, tetanus, diphtheria, zoster, meningococcal, MMR, varicella, and human papillomavirus (HPV) are also recommended.<sup>5</sup></li><li><strong class=\"clr\">Avoid nonsteroidal anti-inflammatory drugs (NSAIDs).<sup>6</sup></strong></li></ul></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>For patients with cirrhosis, a thorough neurological exam is relevant to define the cause of altered mental status.<sup>7</sup></h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\"><strong>References:</strong><br /><strong>1.</strong> American Liver Foundation. The Progression of Liver Disease. https://liverfoundation.org/for-patients/about-the-liver/the-progression-of-liver-disease<br />/#1503432933768-040e8645-d918. Accessed December 11, 2019. <strong>2.</strong> American Liver Foundation. Cirrhosis. &copy; 2016 American Liver Foundation. https://liverfoundation.org/wp-content/uploads/2019/11/Cirrhosis.pdf. Accessed February 11, 2020. <strong>3.</strong> Nusrat S, Khan MS, Fazili J, Madhoun M. Cirrhosis and its complications: evidence-based treatment. <em>World J Gastroenterol</em>. 2014;20(18):5442-5460. <strong>4.</strong> Kochanek K. Deaths: final data for 2017. <em>Natl Vital Stat Rep</em>. 2019;68(9):1-77. <strong>5.</strong> Centers for Disease Control and Prevention. Liver disease and adult vaccination. https://www.cdc.gov/vaccines/adults/rec-vac/health-conditions/liver-disease.html. Accessed February 11, 2020. <strong>6.</strong> Imani F, Motavaf M, Safari S, et al. The therapeutic use of analgesics in patients with liver cirrhosis: a literature review and evidence-based recommendations. <em>Hepat Mon</em>. 2014;14(10):e23539. <strong>7.</strong> Ganapathy D, Bajaj JS. Neurological examination. <em>Clin Liver Dis.</em> 2016;7(6):151-153.</p><div class=\"mb-10\">&nbsp;</div><p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-04-01 07:57:00', '2021-11-29 04:46:10', '1', NULL, NULL, NULL, NULL, NULL),
(42, '2', 'HED.0168.USA.19', 'Monitoring and Documenting Overt Hepatic Encephalopathy Episodes', 'This tool reminds providers to monitor and document OHE symptoms of their patients.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/6ee3fe0bdc607724756cd47a0be929b3', 'healthtools/July2020/TUXC6Jz6sAOsszqben0K.jpg', 'healthtools/July2020/ShEsbZMenyVWnXFoxqLL.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>Monitoring and Documenting Overt Hepatic Encephalopathy Episodes</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h4>Patients with a previous episode of overt hepatic encephalopathy (OHE) were found to have a 40% risk of recurring OHE at 1 year, and patients with recurrent OHE have a 40% risk of another recurrence within 6 months, despite lactulose treatment.<sup>1</sup></h4><div class=\"cmb-mb\">&nbsp;</div><p>Between 30% and 40% of patients with cirrhosis have been diagnosed with OHE.<sup>1</sup> OHE remains a diagnosis of exclusion and can be difficult to diagnose. Therefore, there may be patients with undiagnosed or misdiagnosed OHE. To exclude other causes, you may need to rely on labs and radiological tests.<sup>1</sup></p><div class=\"cmb-mb\">&nbsp;</div><p>Ask patients and caregivers about previous OHE episodes, including episodes of altered mental status that may not have required hospitalization or previously been documented such as difficulty driving, car accidents, or not knowing the season, month or day.<sup>1</sup></p><div class=\"cmb-mb\">&nbsp;</div><p>Episodes can develop rapidly and without warning.<sup>2</sup></p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec bg-sec-2 list-hf\"><p class=\"clr\"><strong>An episode or recurrence in patients with OHE can be defined as having any of the following symptoms that are associated with severe liver insufficiency and/or portosystemic shunting; however, there may be other symptoms not include in this list<sup>1</sup>:</strong></p><div class=\"mb-15\">&nbsp;</div><ul class=\"list-1\"><li>Getting any 3 of the following wrong: <br />day of the month, day of the week, month, season, or year</li><li>Disorientation for time and space</li><li>Lethargy or apathy</li><li>Obvious personality change</li><li>Inappropriate behavior</li><li>Dyspraxia (inability to perform coordinated movements)</li></ul><ul class=\"list-1 small-list\"><li>Asterixis</li><li>Somnolence to semistupor</li><li>Responsive to stimuli</li><li>Confusion</li><li>Gross disorientation</li><li>Bizarre behavior</li><li>Coma</li></ul><div class=\"mb-15\">&nbsp;</div><p class=\"clr\"><strong>Be sure to document any episodes in the patient chart and add the K72.9 ICD-10 code for patients diagnosed with OHE.<sup>3</sup></strong></p></div></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Reduce the risk of episode recurrence with secondary prophylaxis per the AASLD Guidelines.<sup>1</sup></h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">AASLD, American Association for the Study of Liver Diseases (available at AASLD.org)</p><p class=\"smallTxt\"><strong>References:<br />1.</strong> Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encepahlopathy in chronic liver disease: 2014 Practice Guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver. <em>Hepatology</em>. 2014;60(2):715-735. <strong>2.</strong> National Organization for Rare Disorders. Hepatic encephalopathy. https://rarediseases.org/rare-diseases/hepatic-encephalopathy/. Accessed January 20, 2020. <strong>3.</strong> ICD10data.com. Hepatic encephalopathy. https://www.icd10data.com/ICD10CM/Codes/K00-K95/K70-K77/K72-/K72.91. Accessed January 22, 2020.</p><div class=\"mb-10\">&nbsp;</div><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-04-01 08:03:00', '2021-11-29 04:45:57', '1', NULL, NULL, NULL, NULL, NULL),
(43, '2', 'HED.0154.USA.19', 'Prevent Another Attack From Overt Hepatic Encephalopathy', 'This tool discusses triggers for OHE attacks.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/83cb6239833f74b36c87d2cb721a5b21', 'healthtools/July2020/ymiqz9XFMme64vuacAvB.jpg', 'healthtools/July2020/98PFtHJMvg6elQB1GJZk.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>Prevent Another Attack From<br />Overt Hepatic Encephalopathy</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><h3>Having an overt hepatic encephalopathy (OHE) attack is difficult for you and your loved ones. Avoiding OHE triggers and taking your medication as prescribed can help manage your condition.</h3><div class=\"cmb-mb\">&nbsp;</div><p>If you are having trouble managing OHE attacks, talk with your doctor.</p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec p-0\"><div class=\"bg-title\"><h4 class=\"text-center m-0\">Be aware of these OHE triggers</h4></div><div class=\"w100\"><div class=\"row triggers\"><div class=\"w50 w-768-100\"><h4>Triggers you can control</h4><div class=\"mb-15\">&nbsp;</div><ul class=\"list-1\"><li>Binge drinking alcohol</li><li>Dehydration (not getting enough water)</li><li>High blood pressure</li><li>Electrolyte abnormalities: <br />- Low sodium (hyponatremia) <br />- High blood sugar (hyperglycemia)</li><li>A sudden change in diet</li><li>Constipation</li></ul><div class=\"mb-15\">&nbsp;</div></div><div class=\"w50 w-768-100\"><h4>Triggers you may not be able to control</h4><div class=\"mb-15\">&nbsp;</div><ul class=\"list-1\"><li>Bleeding in the stomach and intestines</li><li>Blocked urinary tract</li><li>Hepatic portal shunt&mdash;this is a tube that is placed in a vein near the liver to relieve pressure</li><li>Infection</li><li>Kidney failure</li><li>Liver cancer</li><li>Surgery</li></ul><div class=\"cmb-mb\">&nbsp;</div></div></div></div></div></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Take your medication, avoid OHE triggers, and talk with your family and friends about your OHE.</h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">Content contained in this educational disease-state resource is based, in part, on materials that are the property of the American Liver Foundation and is being provided by Salix Pharmaceuticals for informational purposes only with the consent of the American Liver Foundation. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-04-01 08:06:00', '2021-11-29 04:45:54', '1', NULL, NULL, NULL, NULL, NULL),
(44, '1', 'HED.0162.USA.19', 'Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting', 'Use this checklist when patients with HE are transitioning to another site of care.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/7e7308949bf546de217e9560eec9dc81', 'healthtools/July2020/UlnEVhI6sLGkbhEcpdix.jpg', 'healthtools/July2020/rSaNfjvT9MVuZwahBh7r.jpg', '<div class=\"sub-banner\"><div class=\"container\"><h5>Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to<br />Another Care Setting</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><div class=\"col-12 cmb-mb\"><p>If you are referring a patient with hepatic encephalopathy (HE) to another care setting <br />(a rehabilitation center, a long-term care facility, home health care, etc), AASLD/EASL* guidelines recommend that you communicate certain information to the patient and/or caregiver and provider(s) at the care setting to which the patient will be going. The goal is to make sure everyone involved understands how to manage HE in the specific patient to prevent recurrence.</p></div><div class=\"col-12 cmb-mb\"><h4>Information to communicate to the patient and caregiver</h4><p>Evidence-based guidelines recommend educating patients, relatives, and caregivers about the following<sup>1</sup>:</p><ul class=\"list-3\"><li>Effects of medication and any potential side effects</li><li>Importance of adherence to the treatment plan</li><li>Early signs of recurrence</li><li>Actions to be taken if recurrence occurs</li></ul></div><div class=\"col-12 cmb-mb\"><h4>Information to communicate to health care providers at the new care setting<sup>1</sup></h4><ul class=\"list-3\"><li>Results from patient&rsquo;s cognitive assessment so the new care setting has a baseline</li><li>Results from a motor assessment including evaluation of gait and walking and risk of falls</li><li>Any socioeconomic implications of the disease for the specific patient<ul class=\"list-1\"><li>Has there been a decline in work performance?</li><li>Has there been an impairment in the quality of life?</li><li>Has there been an increase in the risk of accidents?</li><li>Does the patient require economic support or extensive care from the public social support system?</li></ul></li><li>Results from monitoring the patient for cognitive performance and daily life abilities</li><li>Any nutritional aspects of treatment, weight loss, protein and energy intake, increase or decrease in muscle mass</li><li>Patient-specific considerations when weighing the risks and benefits of employing a portosystemic shunt</li><li>Any medication reconciliation (new medication, medications held/discontinued, and specific instructions)</li></ul></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Ensuring all stakeholders have the information they need to appropriately manage the patient can help providers customize the treatment plan and avoid unnecessary time trying to re-establish baselines and gather data.</h5></div></div><div class=\"col-12 cmb-mb\"><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\">*American Association for the Study of Liver Diseases/European Association for the Study of Liver Diseases</p><div class=\"mb-10\">&nbsp;</div><p class=\"smallTxt\"><strong>Reference:<br />1.</strong> Vilstrup H, Amodio P, Bajaj J, et al. Hepatic Encephalopathy in Chronic Liver Disease: 2014 Practice Guideline by AASLD and EASL. Alexandria, VA: American Association for the Study of Liver Disease; 2014. https://www.aasld.org/sites/default/files/2019-06/141022_AASLD_Guideline_Encephalopathy_4UFd_2015.pdf. Accessed November 22. 2019.</p><div class=\"mb-10\">&nbsp;</div><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div></div>', '2020-04-01 08:10:00', '2021-11-29 04:45:40', '1', NULL, NULL, NULL, NULL, NULL),
(45, '1', 'HED.0065.USA.20', 'Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy.', 'This tool discusses the importance of understanding blood-ammonia levels when evaluating patients with HE.', 'https://cc-cms.online/cmsbuilder/page_preview/dist/29b3407ced105094375924b0fb4c3862', 'healthtools/August2020/1lt7bf2P52lCNrPB6oXl.jpg', 'healthtools/August2020/e6UfUdHj4TB5Z3LrIpni.jpg', '<p>&nbsp;</p><!-- Sub Banner --><div class=\"sub-banner\"><div class=\"container\"><h5>Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy</h5></div></div><!-- // Sub Banner --><div class=\"container\"><div class=\"row\"><!-- <div class=\"col-12 cmb-mb\">                        <div class=\"bg-sec\">                            <h4 class=\"normal\"><span><b>8</b></span> things to remember about hepatic encephalopathy                                (HE)</h4>                            <div class=\"cmb-mb\"></div>                            <ol class=\"list-2\">                                <li> HE mainly happens to people who have cirrhosis or other types of liver damage.                                </li>                                <li> HE is caused by toxins that build up in the blood and reach the brain. </li>                                <li> Symptoms of HE can be both mental and physical. </li>                                <li> HE can start slowly. Symptoms of HE may not be noticed at first. </li>                                <li> HE will not get better on its own. Symptoms will likely get worse without                                    continuous therapy. </li>                                <li> Medications for HE aim to control the disease and keep people out of the                                    hospital. Hospital stays for HE can be lengthy and costly. </li>                                <li> Lactulose and antibiotics are used to help manage HE and prevent toxins from                                    building up. </li>                                <li> With timely and proper maintenance therapy, the progression of HE can be slowed                                    and sometimes stopped. </li>                            </ol>                        </div>                    </div> --><div class=\"col-12 cmb-mb\"><!--  <h3>The importance of managing HE</h3> --><p>Normally, ammonia is converted to urea and excreted by the kidneys. However, in patients with cirrhosis, a portosystemic shunt, and/or liver dysfunction can elevate ammonia levels, while cachexia and muscle atrophy can reduce ammonia metabolism. This excess ammonia may lead to the development of hepatic encephalopathy (HE).<sup>1</sup></p><p>Although ammonia plays an important role in the pathophysiology of HE, there is no evidence that ammonia levels alone are important in the management or diagnosis of HE.<sup>2</sup></p></div><div class=\"col-12 cmb-mb\"><div class=\"bg-sec\"><h4>AASLD guidelines:</h4><p>High blood-ammonia levels alone do not add any diagnostic, staging, or prognostic value in HE patients with chronic liver disease. A normal value calls for diagnostic reevaluation (GRADE II-3,A,1).<sup>2</sup></p></div></div><div class=\"col-12 cmb-mb\"><h4>Diagnostic reevaluation of Overt HE (OHE)</h4><p>In case an ammonia level is checked in a patient with OHE and it is normal, according to the AASLD guidelines, the diagnosis of HE is in question.</p><p>At this juncture, the following resources (available on LiverHealthNow) may be helpful:</p><ul class=\"list-1\"><li>West Haven Criteria</li><li>Stroop Test</li></ul><div class=\"cmb-mb\">&nbsp;</div><h4>The Society of Hospital Medicine offers the following recommendations:<sup>1</sup></h4><ul class=\"list-1\"><li>HE is a diagnosis of exclusion and is made on clinical grounds.</li><li>Do not check serum ammonia levels in patients with CLD to diagnose HE, to assess the severity of HE, or to determine whether HE is resolving.</li><li>Use your clinical evaluation to determine the severity and course of HE.</li><li>Treatment should be tailored according to clinical findings, not ammonia levels</li></ul></div><div class=\"col-12 cmb-mb\"><h4>When to use ammonia levels for nondiagnostic purposes, according to the AASLD guidelines</h4><p>When your patient is taking ammonia-lowering drugs, repeated measurements of ammonia may be helpful to test the efficacy.<sup>2</sup></p><p>However, there may be logistic challenges to accurately measure blood ammonia, which should be taken into consideration. Ammonia is reported either in venous, arterial blood, or plasma ammonia; so the relevant normal should be used. Multiple methods are available, but measurements should only be employed when laboratory standards allow for reliable analyses.<sup>2</sup></p></div><div class=\"col-12 cmb-mb\"><div class=\"normal-banner text-center\"><h5>Clinical evaluations and exams should guide the management of patients with HE, not ammonia levels.<sup>1</sup></h5></div></div><div class=\"col-12 cmb-mb\"><p class=\"smallTxt\"><strong>References:</strong><br /><strong>1.</strong>Ninan J, Feldma L. Ammonia levels and hepatic encephalopathy in patients with known chronic liver disease. <em>J Hosp Med.</em> 2017; 12(8):659-661. <strong>2.</strong>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014 practice guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver. <em>Hepatology.</em> 2014;60(2):715-735.</p><div class=\"mb-10\">&nbsp;</div><p class=\"smallTxt\">Content contained in this educational disease-state resource is being provided by Salix Pharmaceuticals for informational purposes only. Physicians should use their own clinical judgment in diagnosing, counseling, and advising patients.</p></div></div></div>', '2020-08-06 20:07:00', '2021-11-30 06:22:41', '1', '21d93cf67920a7813695219e2e3014b3', 'Understanding Blood-Ammonia Levels', '1', NULL, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `healthtool_paitienttype`
--

CREATE TABLE `healthtool_paitienttype` (
  `id` int(10) UNSIGNED NOT NULL,
  `healthtool_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patienttype_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `healthtool_paitienttype`
--

INSERT INTO `healthtool_paitienttype` (`id`, `healthtool_id`, `patienttype_id`, `created_at`, `updated_at`) VALUES
(1, '1', '1', NULL, NULL),
(2, '2', '1', NULL, NULL),
(3, '3', '1', NULL, NULL),
(4, '4', '1', NULL, NULL),
(5, '5', '1', NULL, NULL),
(6, '6', '1', NULL, NULL),
(7, '7', '2', NULL, NULL),
(8, '8', '2', NULL, NULL),
(9, '9', '2', NULL, NULL),
(10, '10', '2', NULL, NULL),
(11, '11', '2', NULL, NULL),
(12, '12', '2', NULL, NULL),
(13, '13', '3', NULL, NULL),
(14, '14', '3', NULL, NULL),
(15, '15', '3', NULL, NULL),
(16, '16', '3', NULL, NULL),
(17, '17', '3', NULL, NULL),
(18, '9', '1', NULL, NULL),
(19, '39', '1', NULL, NULL),
(20, '20', '1', NULL, NULL),
(21, '41', '1', NULL, NULL),
(22, '40', '1', NULL, NULL),
(23, '30', '1', NULL, NULL),
(24, '30', '3', NULL, NULL),
(25, '28', '1', NULL, NULL),
(26, '28', '3', NULL, NULL),
(27, '37', '1', NULL, NULL),
(28, '36', '1', NULL, NULL),
(29, '42', '2', NULL, NULL),
(30, '24', '2', NULL, NULL),
(31, '19', '2', NULL, NULL),
(32, '27', '2', NULL, NULL),
(33, '43', '2', NULL, NULL),
(34, '26', '2', NULL, NULL),
(35, '23', '2', NULL, NULL),
(36, '29', '2', NULL, NULL),
(37, '35', '2', NULL, NULL),
(38, '32', '3', NULL, NULL),
(39, '44', '3', NULL, NULL),
(40, '22', '3', NULL, NULL),
(41, '25', '3', NULL, NULL),
(42, '38', '3', NULL, NULL),
(43, '21', '1', NULL, NULL),
(44, '45', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `healthtool_targetaudiences`
--

CREATE TABLE `healthtool_targetaudiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `healthtool_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `targetaudience_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `healthtool_targetaudiences`
--

INSERT INTO `healthtool_targetaudiences` (`id`, `healthtool_id`, `targetaudience_id`, `created_at`, `updated_at`) VALUES
(1, '1', '3', NULL, NULL),
(2, '2', '3', NULL, NULL),
(3, '3', '3', NULL, NULL),
(4, '4', '3', NULL, NULL),
(5, '5', '3', NULL, NULL),
(6, '6', '1', NULL, NULL),
(7, '6', '2', NULL, NULL),
(8, '7', '3', NULL, NULL),
(9, '8', '3', NULL, NULL),
(10, '9', '1', NULL, NULL),
(11, '9', '2', NULL, NULL),
(12, '10', '1', NULL, NULL),
(13, '10', '2', NULL, NULL),
(14, '11', '1', NULL, NULL),
(15, '11', '2', NULL, NULL),
(16, '12', '1', NULL, NULL),
(17, '12', '2', NULL, NULL),
(18, '13', '3', NULL, NULL),
(19, '14', '1', NULL, NULL),
(20, '14', '2', NULL, NULL),
(21, '15', '1', NULL, NULL),
(22, '15', '2', NULL, NULL),
(23, '16', '1', NULL, NULL),
(24, '16', '2', NULL, NULL),
(25, '17', '1', NULL, NULL),
(26, '17', '2', NULL, NULL),
(27, '18', '3', NULL, NULL),
(28, '39', '3', NULL, NULL),
(29, '20', '3', NULL, NULL),
(30, '41', '3', NULL, NULL),
(31, '40', '1', NULL, NULL),
(32, '40', '2', NULL, NULL),
(33, '30', '1', NULL, NULL),
(34, '30', '2', NULL, NULL),
(35, '28', '1', NULL, NULL),
(36, '28', '2', NULL, NULL),
(37, '37', '1', NULL, NULL),
(38, '37', '2', NULL, NULL),
(39, '36', '1', NULL, NULL),
(40, '36', '2', NULL, NULL),
(41, '42', '3', NULL, NULL),
(42, '24', '3', NULL, NULL),
(43, '19', '3', NULL, NULL),
(44, '27', '1', NULL, NULL),
(45, '27', '2', NULL, NULL),
(46, '43', '1', NULL, NULL),
(47, '43', '2', NULL, NULL),
(48, '26', '1', NULL, NULL),
(49, '26', '2', NULL, NULL),
(50, '23', '1', NULL, NULL),
(51, '23', '2', NULL, NULL),
(52, '29', '1', NULL, NULL),
(53, '29', '2', NULL, NULL),
(54, '35', '1', NULL, NULL),
(55, '35', '2', NULL, NULL),
(56, '32', '3', NULL, NULL),
(57, '44', '3', NULL, NULL),
(58, '22', '3', NULL, NULL),
(59, '25', '1', NULL, NULL),
(60, '25', '2', NULL, NULL),
(61, '38', '1', NULL, NULL),
(62, '38', '2', NULL, NULL),
(63, '21', '3', NULL, NULL),
(64, '45', '3', NULL, NULL),
(65, '45', '2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `health_tools_favs`
--

CREATE TABLE `health_tools_favs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `healthtool_id` bigint(20) DEFAULT NULL,
  `healthtool_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_fav` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_link` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatorycare_banners`
--

CREATE TABLE `lhn_ambulatorycare_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_ambulatorycare_banners`
--

INSERT INTO `lhn_ambulatorycare_banners` (`id`, `image`, `content`, `link`, `created_at`, `updated_at`) VALUES
(1, 'lhn-ambulatorycare-banners\\November2021\\IgjIO77j9AsbwKtG77MB.png', '<h5>In 2018, about <span class=\"big-4\">4.5 million</span> people in the US had<br /><span class=\"big-5  hep\"> <strong class=\"super\">Chronic Liver Disease<sup>1</sup></strong></span></h5>', NULL, '2021-11-22 08:15:29', '2021-11-22 08:15:29'),
(2, 'lhn-ambulatorycare-banners\\November2021\\3H5kK3qMYiHXp35pluB5.png', '<h5><span class=\"big-2\">Chronic Liver Disease</span> <br />can lead to<span class=\"big-5  hep\"> <strong class=\"super\">Cirrhosis<sup>2</sup></strong></span></h5>', NULL, '2021-11-22 08:17:23', '2021-11-22 08:17:23'),
(3, 'lhn-ambulatorycare-banners\\November2021\\zbwruanmJP2V6wiMAaf0.png', '<h5><span class=\"big-4\">Cirrhosis</span> can lead to<br /><span class=\"big-5  hep\">Hepatic Encephalopathy <strong class=\"super\">(HE)</strong></span>and/or other complications<sup>3</sup></h5>', NULL, '2021-11-22 08:18:56', '2021-11-22 08:18:56'),
(4, 'lhn-ambulatorycare-banners\\November2021\\DOLhirStlEmYUoFaSFqy.png', '<h5>Here&rsquo;s how <br />you can <span class=\"big-3\">Impact Care</span> for patients with<br /><span class=\"big-5 \">Chronic Liver Disease</span></h5>', NULL, '2021-11-22 08:19:48', '2021-11-22 08:19:48');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatory_cares_landings`
--

CREATE TABLE `lhn_ambulatory_cares_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_ambulatory_cares_landings`
--

INSERT INTO `lhn_ambulatory_cares_landings` (`id`, `title`, `reference`, `title_two`, `description_one`, `image`, `content`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, '<p><strong>Digital Health Award Winner </strong> for Clinician Web-based Health Portal</p>', '<ol>\r\n<li>Centers for Disease Control and Prevention. Chronic liver disease and cirrhosis. <a href=\"https://www.cdc.gov/nchs/fastats/liver-disease.htm\" target=\"_blank\" rel=\"noopener\">https://www.cdc.gov/nchs/fastats/liver-disease.htm.</a> Accessed June 21, 2021.</li>\r\n<li>National Institute of Diabetes and Digestive and Kidney Diseases. Liver disease. <a href=\"https://www.niddk.nih.gov/health-information/liver-disease\" target=\"_blank\" rel=\"noopener\"> https://www.niddk.nih.gov/health-information/liver-disease.</a> Accessed June 21, 2021.</li>\r\n<li>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014 Practice Guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver. <em>Hepatology</em>. 2014;60(2):715-735.</li>\r\n<li>Data on file. Bridgewater, NJ: Salix Pharmaceuticals.</li>\r\n<li>Bustamante J, Rimola A, Ventura P-J, et al. Prognostic significance of hepatic encephalopathy in patients with cirrhosis. <em>J Hepatol.</em> 1999;30(5):890-895.</li>\r\n</ol>', '<h2 class=\"blue_tag\">LIVERHEALTHNOW|Ambulatory Care Is Committed <br />to <a href=\"lhn-evidence-based.html\">Guideline-driven</a> Quality Care</h2>', '<h6 class=\"sub-tag large-box \">LIVERHEALTHNOW|Ambulatory Care health tools and resources can help you <a class=\"link\">identify patients</a>, <a class=\"link\">define an HE episode</a>, and <a class=\"link\">coordinate care</a> for your patients with chronic liver disease and HE.<br />Now available in English and Spanish.</h6>', 'lhn-ambulatory-cares-landings\\November2021\\gYhJO7lPdObwZ1n1KKYm.jpg', '<p>Discover the latest trends in chronic liver disease care in the first edition of the <a data-toggle=\"modal\" data-target=\"#guideModal\">Liver Health Annual Trends Report.</a></p>', NULL, NULL, NULL, '2021-11-22 07:51:00', '2021-11-22 07:53:25');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatory_coord_cares`
--

CREATE TABLE `lhn_ambulatory_coord_cares` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_three` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_one_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_two_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_ambulatory_coord_cares`
--

INSERT INTO `lhn_ambulatory_coord_cares` (`id`, `title`, `description_one`, `description_two`, `description_three`, `tool_one_title`, `tool_one_description`, `tool_two_title`, `tool_two_description`, `ref`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'Coordinate Care', '<h6><strong>22% to 37% of 30-day readmissions </strong></h6>\r\n<h6>among patients with cirrhosis are potentially preventable with improved disease management.<sup>1,2</sup></h6>', '<p>Patients with cirrhosis can present with complex, often conflicting medical needs (eg, diuretics for ascites with renal insufficiency or hyponatremia; anxiety treated with sedatives that may precipitate encephalopathy).<sup>3</sup></p>\r\n<p>Continuous coordination of care is needed in the areas of medication reconciliation, and patient/family education interventions are needed to maintain vigilance.<sup>3</sup></p>\r\n<p>Open discussions with patients about medication and liver disease may help improve adherence.<sup>4</sup></p>\r\n<p>Collaboration with the patient&rsquo;s family, the general practitioner, and caregivers helps everyone involved understand the management plan for the patient, including new medications, comorbidities, and follow-up visits.</p>', '<p><strong>LIVERHEALTHNOW</strong> supports you as you coordinate care for your patients with chronic liver disease and HE with tools for providers and companion patient and caregiver tools to educate patients.</p>\r\n<p>You can add your organization logo to the <a class=\"inline\" href=\"health-tools.html\"> health tools</a> by following the instructions under the Customize button next to each tool. You can also save your tools to your Favorites for easier access.</p>\r\n<p><a class=\"blueBox--link button\" href=\"#\">REGISTER </a> to get access to the <a class=\"plain-link\" href=\"health-tools.html\">health tools</a> below.</p>', 'Coordinated Care Provider Tools', '<ul class=\"\">\r\n<li>Nurse Checklist for Patients With Hepatic Encephalopathy Who Are Transitioning to Another Care Setting</li>\r\n<li>Discuss Patient Journaling With Your Patients</li>\r\n<li>Nurse Checklist for Liver Disease and Hepatic Encephalopathy</li>\r\n<li>Coding of Hepatic Encephalopathy</li>\r\n</ul>', 'Coordinated Care Patient Tools', '<ul class=\"\">\r\n<li>I Have HE</li>\r\n<li>Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers</li>\r\n<li>Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy</li>\r\n<li>The Importance of Patient Journaling</li>\r\n</ul>', '<ol>\r\n<li>Volk ML, Tocco RS, Bazick J, et al. Hospital re-admissions among patients with decompensated cirrhosis. <em> Am J Gastroenterol.</em>2012;107(2):247-252.</li>\r\n<li>Agrawal K, Kumar P, Markert R, Agrawal S. Risk factors for 30-day readmissions of individuals with decompensated cirrhosis. <em> South Med J.</em>2015;108(11):682-687.</li>\r\n<li>Tapper EB, Halbert B, Mellinger J. Rates of and reasons for hospital readmissions in patients with cirrhosis: a multistate population-based cohort study. <em> Clin Gastroenterol Hepatol.</em>2016;14:1181-1188.</li>\r\n<li>Hayward KL, Valery PC, Martin JH, et al. Medication beliefs predict medication adherence in ambulatory patients with decompensated cirrhosis. <em> World J Gastroenterol.</em>2017;23(40):7321-7331.</li>\r\n</ol>', NULL, NULL, NULL, '2021-11-23 05:01:07', '2021-11-23 05:01:07');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatory_define_episodes`
--

CREATE TABLE `lhn_ambulatory_define_episodes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_three` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_resource_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_resource_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_ambulatory_define_episodes`
--

INSERT INTO `lhn_ambulatory_define_episodes` (`id`, `title`, `content`, `content_two`, `content_three`, `provider_resource_content`, `patient_resource_content`, `ref`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'Define an Episode of Hepatic Encephalopathy', '<p>Once you have identified and treated patients with hepatic encephalopathy (HE), work with them and/or their caregivers to come up with a management plan to help reduce the risk of recurrence by monitoring for symptoms and identifying an episode of overt HE (OHE). If patients are monitored regularly, symptoms of an OHE episode can be addressed before a transition of care is needed.</p>\r\n<div class=\"title\">\r\n<p>A potential episode or occurrence in patients with OHE can be identified by testing/monitoring for the following symptoms<sup>1</sup>:</p>\r\n</div>\r\n<div class=\"list-one \">\r\n<ul class=\"\">\r\n<li>Disorientation for time. Getting any 3 of the following wrong: day of the month, day of the week, month, season, or year</li>\r\n<li>Lethargy or apathy</li>\r\n<li>Obvious personality change</li>\r\n<li>Inappropriate behavior</li>\r\n<li>Dyspraxia</li>\r\n<li>Asterixis</li>\r\n<li>Somnolence to semistupor</li>\r\n<li>Responsive to stimuli</li>\r\n<li>Confusion</li>\r\n<li>Gross disorientation</li>\r\n<li>Bizarre behavior</li>\r\n<li>Coma</li>\r\n</ul>\r\n</div>', '<h6>Patients with a previous occurrence of OHE have a <strong>40% risk of recurring OHE</strong>at 1 year.<sup>1</sup></h6>\r\n<h6>Patients with recurrent OHE have a <br /><strong>40% risk of another recurring</strong> within 6 months, despite lactulose treatment.<sup>1</sup></h6>\r\n<h6>In 2018,<strong>37% of hospitalized patients </strong> with OHE were readmitted within 30 days.<sup>2</sup></h6>\r\n<h6>Patients who have OHE have a <br /><strong>42% probability of survival</strong>after 1 year.<sup>*3 </sup></h6>\r\n<div class=\"small-note\">*This is from a retrospective chart review of medical records from 1990-1993.</div>', '<p>The <strong>LIVERHEALTHNOW</strong> tools in this section have been developed to help you recognize evidence of HE episodes so you can help reduce the risk of recurrence and even HE-related hospitalization. There are also tools to help patients and/or their caregivers monitor symptoms between medical visits.</p>\r\n<p>You can add your organization logo to the <a class=\"inline\" href=\"health-tools.html\"> health tools</a> by following the instructions under the Customize button next to each tool. You can also save your tools to your Favorites for easier access.</p>\r\n<p><a class=\"blueBox--link button\" href=\"#\">REGISTER </a> to get access to the <a class=\"plain-link\" href=\"health-tools.html\">health tools</a> below.</p>', '<ul class=\"\">\r\n<li>Monitoring and Documenting Overt Hepatic Encephalopathy Episodes</li>\r\n<li>Help Patients Set Goals Using Shared Decision Making</li>\r\n<li>Counseling Your Hepatic Encephalopathy Patients About Driving</li>\r\n<li>Coding of Hepatic Encephalopathy</li>\r\n</ul>', '<ul class=\"\">\r\n<li>Important Signs and Symptoms for Patients With Chronic Liver Disease</li>\r\n<li>Prevent Another Attack for Overt Hepatic Encephalopathy</li>\r\n<li>Importance of Taking Medication as Prescribed for Overt Hepatic Encephalopathy</li>\r\n<li>Goal Setting When You Have Chronic Liver Disease</li>\r\n<li>Living With Diabetes and Liver Disease</li>\r\n<li>Setting Up Medical Alerts on Digital Devices</li>\r\n</ul>', '<ol>\r\n<li>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014 Practice Guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver. <strong> <em> Hepatology.</em></strong> 2014;60(2):715-735.</li>\r\n<li>Data on file. Bridgewater, NJ: Salix Pharmaceuticals.</li>\r\n<li>Bustamante J, Rimola A, Ventura P-J, et al. Prognostic significance of hepatic encephalopathy in patients with cirrhosis.<strong> <em> J Hepatol.</em></strong> 1999;30(5):890-895.</li>\r\n</ol>', NULL, NULL, NULL, '2021-11-23 04:07:00', '2021-11-23 04:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_amb_screen_patients`
--

CREATE TABLE `lhn_amb_screen_patients` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_one_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_two_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tool_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_amb_screen_patients`
--

INSERT INTO `lhn_amb_screen_patients` (`id`, `title`, `description_one`, `content_one`, `description_two`, `tool_one_description`, `tool_two_description`, `ref`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`, `tool_one_title`, `tool_two_title`) VALUES
(1, 'The Importance of Screening Patients With Chronic Liver Disease for Hepatic Encephalopathy', '<h4>Chronic liver disease (CLD)</h4>\r\n<ul>\r\n<li>Affects <strong>about 4.5 million people</strong> in the United States<sup>1</sup></li>\r\n</ul>\r\n<h4>CLD and chronic are the</h4>\r\n<ul>\r\n<li>6th leading cause of death in persons aged 25-44<sup>2</sup></li>\r\n<li>5th leading cause of death in persons aged 45-54<sup>2</sup></li>\r\n</ul>', '<div class=\"title\">\r\n<p>Common types of CLD in the United States are increasing:</p>\r\n</div>\r\n<p>Prevalence of nonalcoholic fatty liver disease (NAFLD) and resulting nonalcoholic steatohepatitis (NASH) are projected to increase 21% and 63%, respectively, between 2015 and 2030 due to high rates of diabetes and obesity.<sup>*3</sup></p>\r\n<p>Incidence of decompensated cirrhosis may increase 168% by 2030, while hepatocellular carcinoma will increase by 137%.<sup>*3</sup></p>\r\n<div class=\"title margin-top1\">\r\n<p>As the prevalence of cirrhosis rises, it is expected that cirrhosis complications, including hepatic encephalopathy (HE), will also rise.</p>\r\n</div>\r\n<p>The prevalence of minimal HE (MHE) and covert HE (CHE) occurs in as many as 50% of patients with CLD,so guidelines recommend every CLD patient be screened.<sup>4</sup></p>\r\n<p>Overt HE (OHE) is estimated to occur in up to 30% to 40% of patients with cirrhosis.<sup>4</sup></p>\r\n<p>Screening for CHE is important because it can prognosticate OHE development, indicate poor quality of life, and help identify patients and caregivers who may need to be counseled about the disease.<sup>4</sup></p>\r\n<p>In 2018, 37% of hospitalized patients with OHE were readmitted within 30 days.5 Patients who have OHE have a 42% probability of survival after 1 year.<sup>6</sup></p>\r\n<p>HE may affect the patient, the patient&rsquo;s family, and every one the patient comes in contact with. HE may affect the patient&rsquo;s ability to work and drive, and has the potential to increase health care costs. It is important to recognize and manage HE to help improve these conditions.<sup>7</sup></p>\r\n<div class=\"small-note\">*These statistics are based on projections, and actual prevalence may differ.</div>\r\n<div class=\"small-note\">*This is from a retrospective chart review of medical records from 1990-1993.</div>', '<p><strong>LIVERHEALTHNOW</strong> supports you as you screen CLD patients for HE, with tools for providers and companion patient and caregiver tools to educate patients through screening and diagnosis of HE.</p>\r\n<p>You can add your organization logo to the <a class=\"inline\" href=\"health-tools.html\"> health tools</a> by following the instructions under the Customize button next to each tool. You can also save your tools to your Favorites for easier access.</p>\r\n<p><a class=\"blueBox--link button\" href=\"#\">REGISTER </a> to get access to the <a class=\"plain-link\" href=\"health-tools.html\">health tools</a>below.</p>', '<ul class=\"\">\r\n<li>Assess HE With the Stroop Test</li>\r\n<li>West Haven Criteria</li>\r\n<li>Diagnosing Hepatic Encephalopathy in Patients With Liver Disease</li>\r\n<li>Cirrhosis and Its Complications</li>\r\n<li>Understanding Blood-Ammonia Levels in Patients With Hepatic Encephalopathy</li>\r\n<li>Coding of Hepatic Encephalopathy</li>\r\n</ul>', '<ul class=\"\">\r\n<li>What Is Hepatic Encephalopathy?</li>\r\n<li>Medication Information for Patients With Hepatic Encephalopathy and Their Caregivers</li>\r\n<li>Lifestyle Self-management for Patients With Overt Hepatic Encephalopathy</li>\r\n<li>Symptoms, Complications, and Management of Cirrhosis</li>\r\n<li>Stages and Types of Liver Disease</li>\r\n</ul>', '<ol>\r\n<li>Center for Disease Control and Prevention. Chronic liver disease and cirrhosis. <a href=\"https://www.cdc.gov/nchs/fastats/liver-disease.htm\" target=\"_blank\" rel=\"noopener\">https://www.cdc.gov/nchs/fastats/liver-disease.htm.</a> Accessed June 21, 2021.</li>\r\n<li>Heron M. Deaths: leading causes for 2018. Natl Vital Stat Rep. 2021;70(4):1-115. 2021;69(13):1-83.</li>\r\n<li>Estes C, Razavi H, Loomba R, et al. Modeling the epidemic of nonalcoholic fatty liver disease demonstrates an exponential increase in burden of disease. <strong> <em> Hepatology.</em></strong> 2018;67(1):123-133.</li>\r\n<li>Vilstrup H, Amodio P, Bajaj J, et al. Hepatic encephalopathy in chronic liver disease: 2014 Practice Guideline by the American Association for the Study of Liver Diseases and the European Association for the Study of the Liver.<strong> <em> Hepatology.</em></strong> 2014;60(2):715-735.</li>\r\n<li>Data on file. Bridgewater, NJ: Salix Pharmaceuticals.</li>\r\n<li>Bustamante J, Rimola A, Ventura P-J, et al. Prognostic significance of hepatic encephalopathy in patients with cirrhosis. <strong> <em> J Hepatol.</em></strong> 1999;30(5):890-895.</li>\r\n<li>Patidar KR, Bajaj JS. Covert and overt hepatic encephalopathy: diagnosis and management.<strong> <em> Clin Gastroenterol Hepatol.</em></strong> 2015;13(12):2048-2061.</li>\r\n</ol>', NULL, NULL, NULL, '2021-11-23 02:06:00', '2021-11-23 02:09:18', 'Screening Tools for the Provider', 'Tools for the Patient');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_annual_trend_reports`
--

CREATE TABLE `lhn_annual_trend_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_annual_trend_reports`
--

INSERT INTO `lhn_annual_trend_reports` (`id`, `title`, `description`, `image`, `title_two`, `description_two`, `link`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'Liver Health Annual Trends Report', '<div class=\"title\">\r\n<p>The results of the Liver Health Annual Trends Report indicate that it&rsquo;s time to act:</p>\r\n</div>\r\n<ol>\r\n<li>Broaden awareness to AASLD Guidelines for CLD patient care</li>\r\n<li>Increase implementation of guidelines-based care</li>\r\n<li>Address impact of psychosocial factors on patient outcomes</li>\r\n</ol>', 'lhn-annual-trend-reports\\November2021\\gl4lE2EWdHlLpX9GnhVc.jpg', 'Liver Health Annual Trends Report', 'is the findings of primary and secondary research analyzed to understand what barriers exist to quality care for CLD patients and to measure the trends impacting the adoption of guidelines and improved care over time.', NULL, NULL, NULL, NULL, '2021-11-23 00:58:58', '2021-11-23 00:58:58');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primarycare_raisebars`
--

CREATE TABLE `lhn_primarycare_raisebars` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_primarycare_raisebars`
--

INSERT INTO `lhn_primarycare_raisebars` (`id`, `title`, `content`, `video_link`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'Raise the Bar', '<h5 class=\"blk-title\"><strong>LIVERHEALTHNOW|Primary Care</strong> has resources to support you as you raise the bar on quality care for chronic liver disease and it\'s complications.</h5>\r\n<h3>Watch the Raise the Bar on Cirrhosis Care In-service presentation below. At the completion of this presentation, you will be prompted to download your certificate of completion.</h3>', NULL, NULL, NULL, NULL, '2021-11-23 01:33:38', '2021-11-23 01:33:38');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primary_care_banners`
--

CREATE TABLE `lhn_primary_care_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_primary_care_banners`
--

INSERT INTO `lhn_primary_care_banners` (`id`, `image`, `content`, `link`, `created_at`, `updated_at`) VALUES
(1, 'lhn-primary-care-banners\\November2021\\s740lpLbp1OLSpv2axRj.png', '<h5>In 2018, about <span class=\"big-4\">4.5 million</span> people in the US had<br /><span class=\"big-5 yel_colr hep\"> <strong class=\"super\">Chronic Liver Disease<sup>1</sup></strong></span></h5>', NULL, '2021-11-22 05:40:00', '2021-11-22 07:29:03'),
(2, 'lhn-primary-care-banners\\November2021\\VR3mapukCLonxpREIFOS.png', '<h5><span class=\"big-2\">Chronic Liver Disease</span> <br />can lead to<span class=\"big-5 yel_colr hep\"> <strong class=\"super\">Cirrhosis<sup>2</sup></strong></span></h5>', NULL, '2021-11-22 05:43:00', '2021-11-22 07:30:11'),
(3, 'lhn-primary-care-banners\\November2021\\XwDiYvXKaYniQ6gg6ZjD.png', '<h5>Up to <span class=\"big-3\">80%</span> of patients with <span class=\"big-4\">Cirrhosis</span><br />will eventually develop some form of<br /><span class=\"big-5 yel_colr hep\">Hepatic Encephalopathy <strong class=\"super\">(HE)<sup>*3</sup></strong></span>&lt;/h5</h5>\r\n<p>*Ranging from minimal to overt.</p>', NULL, '2021-11-22 05:45:00', '2021-11-22 07:31:51'),
(4, 'lhn-primary-care-banners\\November2021\\FXGEQwcuEDgLRQwyGYJI.png', '<h5>In 2018, <span class=\"big-3\">37%</span> of hospitalized patients with<br /><span class=\"big-2 yel_colr\">overt hepatic encephalopathy (OHE)</span> were <span class=\"big-1 yel_colr\">readmitted </span>within 30 <strong class=\"super\">days<sup>4</sup></strong></h5>\r\n<p>*HE cases are identified by ICD-10 codes K70.41 (alcoholic hepatic failure with coma), K71.11 (toxic liver disease with hepatic necrosis, with coma), K72.01 (acute and subacute hepatic failure with coma), K72.11 (chronic hepatic failure with coma), K72.90 (hepatic failure, unspecified without coma), and K72.91 (hepatic failure, unspecified with coma). Unless otherwise specified, hospital case data include primary and secondary diagnoses. Hospital case data for 2018 were collected from October 1, 2017, to September 30, 2018.</p>', NULL, '2021-11-22 05:48:00', '2021-11-22 07:33:59'),
(5, 'lhn-primary-care-banners\\November2021\\6BF7V619Cgq7tNuOs09n.png', '<h5>Patients who have been hospitalized with <span class=\"big-2 yel_colr\">OHE</span> have <br class=\"br-none\" />a <span class=\"big-4 \">42%</span> probability of <span class=\"big-4 yel_colr\">survival</span> at 1 <strong class=\"super\">year<sup>*5</sup></strong></h5>\r\n<p>*This is from a retrospective chart review of medical records from 1990-1993.</p>', NULL, '2021-11-22 06:03:00', '2021-11-22 07:35:34'),
(6, 'lhn-primary-care-banners\\November2021\\PpORhkB8xv4il0uBDbcg.png', '<h5>Here&rsquo;s how <br />you can <span class=\"big-3\">Impact Care</span> for patients with<br /><span class=\"big-5 yel_colr\"> Chronic Liver Disease</span></h5>', NULL, '2021-11-22 07:37:43', '2021-11-22 07:37:43');

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primary_care_landings`
--

CREATE TABLE `lhn_primary_care_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_three` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_four` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_five` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video_title_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_title_one_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_title_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_title_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lhn_primary_care_landings`
--

INSERT INTO `lhn_primary_care_landings` (`id`, `title`, `description_one`, `description_two`, `description_three`, `description_four`, `image`, `description_five`, `ref`, `meta_title`, `meta_description`, `keywords`, `created_at`, `updated_at`, `video_title_one`, `video_title_one_link`, `video_title_two`, `video_title_two_link`) VALUES
(1, '<p><strong>Digital Health Award Winner </strong> for Clinician Web-based Health Portal </p>', '<h2 class=\"blue_tag trans-bg\">LIVERHEALTHNOW|Primary Care Is Committed <br />to <a href=\"lhn-evidence-based.html\">Guideline-driven</a>Quality Care</h2>', '<p>Watch this short educational presentation and get a certificate for participating in raising the bar in cirrhosis care. You can also share this presentation with your colleagues who also have patients with cirrhosis.</p>', '<p>Watch this documentary to understand the effects of this disease from patients\' perspectives and those of their families.</p>', NULL, 'lhn-primary-care-landings\\November2021\\ir9RaomIfjXRrGAfmbSB.jpg', '<p>Discover the latest trends in chronic liver disease care in the first edition of the <a data-toggle=\"modal\" data-target=\"#guideModal\">Liver Health Annual Trends Report.</a></p>', '<ul>\r\n<li>Centers for Disease Control and Prevention. Chronic liver disease and cirrhosis. <a href=\"https://www.cdc.gov/nchs/fastats/liver-disease.htm\" target=\"_blank\" rel=\"noopener\">https://www.cdc.gov/nchs/fastats/liver-disease.htm.</a> Accessed August 10, 2021.</li>\r\n<li>National Institute of Diabetes and Digestive and Kidney Diseases. Liver disease. <a href=\"https://www.niddk.nih.gov/health-information/liver-disease\" target=\"_blank\" rel=\"noopener\"> https://www.niddk.nih.gov/health-information/liver-disease.</a> Accessed August 10, 2021.</li>\r\n<li>National Institute of Diabetes and Digestive and Kidney Diseases. Definition &amp; Facts for Cirrhosis. <a href=\"https://www.niddk.nih.gov/health-information /liver-disease/cirrhosis/definition-facts\" target=\"_blank\" rel=\"noopener\"> https://www.niddk.nih.gov/health-information /liver-disease/cirrhosis/definition-facts.</a>Reviewed March 2018. Accessed August 10, 2021.</li>\r\n</ul>', NULL, NULL, NULL, '2021-11-22 08:41:00', '2021-11-22 09:19:27', 'Raise the Bar on Cirrhosis Care', NULL, 'Wrestling the Monster: Living With Hepatic Encephalopathy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primcare_wres_mons`
--

CREATE TABLE `lhn_primcare_wres_mons` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_landings`
--

CREATE TABLE `main_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_btn_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_btn_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_link_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_link_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-11-08 08:41:07', '2021-11-08 08:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, 5, 3, '2021-11-08 08:41:07', '2021-11-09 05:29:55', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 5, '2021-11-08 08:41:08', '2021-11-09 05:29:55', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 5, 9, '2021-11-08 08:41:08', '2021-11-09 05:29:36', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 5, 7, '2021-11-08 08:41:08', '2021-11-09 05:29:36', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 5, '2021-11-08 08:41:08', '2021-11-29 06:31:14', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 6, '2021-11-08 08:41:08', '2021-11-09 05:29:55', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 4, '2021-11-08 08:41:08', '2021-11-09 05:29:55', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 2, '2021-11-08 08:41:08', '2021-11-09 05:29:42', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 1, '2021-11-08 08:41:08', '2021-11-09 05:29:38', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, 5, 8, '2021-11-08 08:41:08', '2021-11-09 05:29:36', 'voyager.settings.index', NULL),
(11, 1, 'Pathways Page', '', '_self', 'voyager-dot', '#000000', 13, 1, '2021-11-09 00:39:22', '2021-11-09 06:43:56', 'voyager.ace-pathways.index', 'null'),
(12, 1, 'Care Pathways', '', '_self', 'voyager-dot', '#000000', 13, 3, '2021-11-09 00:41:50', '2021-11-09 06:44:15', 'voyager.ace-care-pathways.index', 'null'),
(13, 1, 'Ace', '', '_self', 'voyager-window-list', '#000000', NULL, 2, '2021-11-09 00:42:29', '2021-11-09 07:53:34', NULL, ''),
(14, 1, 'Ehr Plugins Pages', '', '_self', 'voyager-dot', '#000000', 13, 4, '2021-11-09 00:57:26', '2021-11-09 06:44:22', 'voyager.ace-ehr-plugins-page.index', 'null'),
(15, 1, 'Health Systems', '', '_self', 'voyager-dot', '#000000', 13, 2, '2021-11-09 02:08:06', '2021-11-09 06:44:08', 'voyager.ace-health-systems.index', 'null'),
(16, 1, 'Long Term Care Pathways Page', '', '_self', 'voyager-dot', '#000000', 13, 6, '2021-11-09 04:30:51', '2021-11-09 07:53:34', 'voyager.ace-long-term-care-pathways.index', 'null'),
(17, 1, 'Long Term Care Pathways', '', '_self', 'voyager-dot', '#000000', 13, 7, '2021-11-09 04:33:21', '2021-11-09 07:53:34', 'voyager.ace-long-term-care-d-pathways.index', 'null'),
(18, 1, 'Ace Long Terms Ehr Plugins', '', '_self', 'voyager-dot', NULL, 13, 8, '2021-11-09 04:44:07', '2021-11-09 07:53:34', 'voyager.ace-long-terms-ehr-plugins.index', NULL),
(20, 1, 'Long Term Landings', '', '_self', 'voyager-dot', '#000000', 13, 5, '2021-11-09 05:02:24', '2021-11-09 07:53:34', 'voyager.ace-long-term-landings.index', 'null'),
(21, 1, 'Ace Guidelines', '', '_self', 'voyager-dot', NULL, 13, 9, '2021-11-09 05:13:21', '2021-11-09 07:53:34', 'voyager.ace-guidelines.index', NULL),
(22, 1, 'Ace Guidelines Links', '', '_self', 'voyager-dot', NULL, 13, 10, '2021-11-09 05:14:17', '2021-11-09 07:53:34', 'voyager.ace-guidelines-links.index', NULL),
(23, 1, 'Ace Quality Measures', '', '_self', 'voyager-dot', NULL, 13, 11, '2021-11-09 05:19:07', '2021-11-09 07:53:34', 'voyager.ace-quality-measures.index', NULL),
(24, 1, 'LHN', '', '_self', 'voyager-window-list', '#000000', NULL, 3, '2021-11-09 05:29:10', '2021-11-09 07:53:34', NULL, ''),
(25, 1, 'Annual Trend Reports', '', '_self', 'voyager-dot', NULL, 24, 3, '2021-11-09 05:48:04', '2021-11-09 06:04:11', 'voyager.lhn-annual-trend-reports.index', NULL),
(26, 1, 'Ambulatory Cares Landings', '', '_self', 'voyager-dot', '#000000', 24, 1, '2021-11-09 05:56:41', '2021-11-09 06:04:09', 'voyager.lhn-ambulatory-cares-landings.index', 'null'),
(27, 1, 'Ambulatory Care Banners', '', '_self', 'voyager-dot', '#000000', 24, 2, '2021-11-09 06:03:34', '2021-11-09 06:04:26', 'voyager.lhn-ambulatorycare-banners.index', 'null'),
(28, 1, 'Ambulatory Coord Cares', '', '_self', 'voyager-dot', NULL, 24, 4, '2021-11-09 06:10:48', '2021-11-09 06:11:00', 'voyager.lhn-ambulatory-coord-cares.index', NULL),
(29, 1, 'Ambulatory Define Episodes', '', '_self', 'voyager-dot', NULL, 24, 5, '2021-11-09 06:19:23', '2021-11-09 06:19:32', 'voyager.lhn-ambulatory-define-episodes.index', NULL),
(30, 1, 'Ambulatory Screen Patients', '', '_self', 'voyager-dot', '#000000', 24, 6, '2021-11-09 06:32:20', '2021-11-09 06:32:52', 'voyager.lhn-amb-screen-patients.index', 'null'),
(31, 1, 'Primary Care Landings', '', '_self', 'voyager-dot', NULL, 24, 7, '2021-11-09 06:41:37', '2021-11-09 06:41:44', 'voyager.lhn-primary-care-landings.index', NULL),
(32, 1, 'Primary Care Banners', '', '_self', 'voyager-dot', NULL, 24, 8, '2021-11-09 06:43:24', '2021-11-09 06:43:31', 'voyager.lhn-primary-care-banners.index', NULL),
(33, 1, 'Primary Care Raisebars', '', '_self', 'voyager-dot', NULL, 24, 9, '2021-11-09 07:18:00', '2021-11-09 07:18:45', 'voyager.lhn-primarycare-raisebars.index', NULL),
(34, 1, 'Primary Care Wrestling The Monsters', '', '_self', 'voyager-dot', NULL, 24, 10, '2021-11-09 07:25:37', '2021-11-09 07:25:45', 'voyager.lhn-primcare-wres-mons.index', NULL),
(35, 1, 'Main Landings', '', '_self', 'voyager-world', NULL, NULL, 1, '2021-11-09 07:51:41', '2021-11-09 07:53:34', 'voyager.main-landings.index', NULL),
(36, 1, 'Ace Health System Ehr Plugins', '', '_self', NULL, NULL, NULL, 6, '2021-11-24 02:12:24', '2021-11-29 06:31:14', 'voyager.ace-health-system-ehr-plugins.index', NULL),
(39, 1, 'Ace Popups', '', '_self', NULL, NULL, NULL, 7, '2021-11-24 07:10:38', '2021-11-29 06:31:14', 'voyager.ace-popup.index', NULL),
(40, 1, 'Ace Popups', '', '_self', NULL, NULL, NULL, 8, '2021-11-24 07:15:51', '2021-11-29 06:31:14', 'voyager.ace-popups.index', NULL),
(41, 1, 'Healthtools', '', '_self', 'voyager-dot', NULL, 46, 3, '2021-11-25 08:13:44', '2021-11-29 06:31:06', 'voyager.healthtools.index', NULL),
(43, 1, 'Website Sections', '', '_self', 'voyager-dot', NULL, NULL, 9, '2021-11-29 05:58:16', '2021-11-29 06:31:14', 'voyager.website-sections.index', NULL),
(44, 1, 'Targetaudiences', '', '_self', 'voyager-dot', NULL, 46, 1, '2021-11-29 06:03:59', '2021-11-29 06:31:02', 'voyager.targetaudiences.index', NULL),
(45, 1, 'Patient Types', '', '_self', 'voyager-dot', '#000000', 46, 2, '2021-11-29 06:29:38', '2021-11-29 06:31:02', 'voyager.patienttypes.index', 'null'),
(46, 1, 'Health Tool Management', '', '_self', 'voyager-dot', '#000000', NULL, 4, '2021-11-29 06:30:51', '2021-11-29 06:31:14', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organizationtypes`
--

CREATE TABLE `organizationtypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organizationtypes`
--

INSERT INTO `organizationtypes` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'National Health Plan', '1', '2019-05-08 05:00:48', '2019-05-08 05:00:48'),
(2, 'Health System', '1', '2019-05-08 05:00:57', '2019-05-08 05:00:57'),
(3, 'Employer', '1', '2019-05-08 05:01:02', '2019-05-08 05:01:02'),
(4, 'Hospital/Clinic', '1', '2019-05-08 05:01:09', '2019-05-08 05:01:09'),
(5, 'Regional Health Plan', '1', '2019-05-08 05:01:15', '2019-05-08 05:01:15'),
(6, 'Medical Group/Group Practice', '1', '2019-05-08 05:04:01', '2019-05-08 05:04:01'),
(7, 'IDN – Integrated Delivery Network', '1', '2019-05-08 05:04:09', '2019-05-08 05:04:09'),
(8, 'ACO – Accountable Care Organization', '1', '2019-05-08 05:04:15', '2019-05-08 05:04:15'),
(9, 'Veterans Health Organization', '1', '2019-05-08 05:04:22', '2019-05-08 05:04:22'),
(10, 'Other', '1', '2019-05-08 05:04:28', '2019-05-08 05:04:28');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patienttypes`
--

CREATE TABLE `patienttypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patienttypes`
--

INSERT INTO `patienttypes` (`id`, `type`, `status`, `sections`, `created_at`, `updated_at`) VALUES
(1, 'Screening', '1', '[\"1\",\"2\"]', '2019-05-13 03:53:00', '2021-11-29 06:37:19'),
(2, 'Define an Episode', '1', '[\"1\",\"2\"]', '2019-05-13 03:53:00', '2021-11-29 06:37:23'),
(3, 'Coordinate Care', '1', '[\"1\",\"2\"]', '2019-05-13 03:53:00', '2021-11-29 06:37:27'),
(4, 'OHE Appropriately Identified', '1', '[\"3\"]', '2021-11-29 06:38:03', '2021-11-29 06:38:03'),
(5, 'OHE Patient Management', '1', '[\"3\"]', '2021-11-29 06:38:21', '2021-11-29 06:38:21'),
(6, 'OHE Patient at Discharge', '1', '[\"3\"]', '2021-11-29 06:38:29', '2021-11-29 06:38:29'),
(7, 'OHE In Long-term Care', '1', '[\"4\"]', '2021-11-29 06:38:42', '2021-11-29 06:38:42');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(2, 'browse_bread', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(3, 'browse_database', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(4, 'browse_media', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(5, 'browse_compass', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(6, 'browse_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(7, 'read_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(8, 'edit_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(9, 'add_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(10, 'delete_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(11, 'browse_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(12, 'read_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(13, 'edit_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(14, 'add_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(15, 'delete_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(16, 'browse_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(17, 'read_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(18, 'edit_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(19, 'add_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(20, 'delete_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(21, 'browse_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(22, 'read_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(23, 'edit_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(24, 'add_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(25, 'delete_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(26, 'browse_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(27, 'read_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(28, 'edit_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(29, 'add_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(30, 'delete_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(31, 'browse_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(32, 'read_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(33, 'edit_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(34, 'add_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(35, 'delete_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(36, 'browse_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(37, 'read_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(38, 'edit_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(39, 'add_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(40, 'delete_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(41, 'browse_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(42, 'read_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(43, 'edit_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(44, 'add_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(45, 'delete_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(46, 'browse_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(47, 'read_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(48, 'edit_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(49, 'add_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(50, 'delete_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(51, 'browse_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(52, 'read_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(53, 'edit_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(54, 'add_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(55, 'delete_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(56, 'browse_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(57, 'read_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(58, 'edit_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(59, 'add_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(60, 'delete_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(61, 'browse_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(62, 'read_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(63, 'edit_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(64, 'add_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(65, 'delete_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(66, 'browse_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:23', '2021-11-09 05:02:23'),
(67, 'read_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:23', '2021-11-09 05:02:23'),
(68, 'edit_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:23', '2021-11-09 05:02:23'),
(69, 'add_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:24', '2021-11-09 05:02:24'),
(70, 'delete_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:24', '2021-11-09 05:02:24'),
(71, 'browse_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(72, 'read_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(73, 'edit_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(74, 'add_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(75, 'delete_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(76, 'browse_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(77, 'read_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(78, 'edit_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(79, 'add_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(80, 'delete_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(81, 'browse_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(82, 'read_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(83, 'edit_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(84, 'add_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(85, 'delete_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(86, 'browse_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(87, 'read_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(88, 'edit_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(89, 'add_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(90, 'delete_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(91, 'browse_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(92, 'read_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(93, 'edit_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(94, 'add_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(95, 'delete_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(96, 'browse_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(97, 'read_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(98, 'edit_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(99, 'add_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(100, 'delete_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(101, 'browse_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(102, 'read_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(103, 'edit_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(104, 'add_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(105, 'delete_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(106, 'browse_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(107, 'read_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(108, 'edit_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(109, 'add_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(110, 'delete_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(111, 'browse_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(112, 'read_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(113, 'edit_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(114, 'add_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(115, 'delete_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(116, 'browse_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(117, 'read_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(118, 'edit_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(119, 'add_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(120, 'delete_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(121, 'browse_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(122, 'read_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(123, 'edit_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(124, 'add_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(125, 'delete_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(126, 'browse_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(127, 'read_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(128, 'edit_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(129, 'add_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(130, 'delete_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(131, 'browse_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(132, 'read_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(133, 'edit_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(134, 'add_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(135, 'delete_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(136, 'browse_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(137, 'read_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(138, 'edit_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(139, 'add_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(140, 'delete_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(141, 'browse_ace_health_system_ehr_plugins', 'ace_health_system_ehr_plugins', '2021-11-24 02:12:24', '2021-11-24 02:12:24'),
(142, 'read_ace_health_system_ehr_plugins', 'ace_health_system_ehr_plugins', '2021-11-24 02:12:24', '2021-11-24 02:12:24'),
(143, 'edit_ace_health_system_ehr_plugins', 'ace_health_system_ehr_plugins', '2021-11-24 02:12:24', '2021-11-24 02:12:24'),
(144, 'add_ace_health_system_ehr_plugins', 'ace_health_system_ehr_plugins', '2021-11-24 02:12:24', '2021-11-24 02:12:24'),
(145, 'delete_ace_health_system_ehr_plugins', 'ace_health_system_ehr_plugins', '2021-11-24 02:12:24', '2021-11-24 02:12:24'),
(156, 'browse_ace_popup', 'ace_popup', '2021-11-24 07:10:38', '2021-11-24 07:10:38'),
(157, 'read_ace_popup', 'ace_popup', '2021-11-24 07:10:38', '2021-11-24 07:10:38'),
(158, 'edit_ace_popup', 'ace_popup', '2021-11-24 07:10:38', '2021-11-24 07:10:38'),
(159, 'add_ace_popup', 'ace_popup', '2021-11-24 07:10:38', '2021-11-24 07:10:38'),
(160, 'delete_ace_popup', 'ace_popup', '2021-11-24 07:10:38', '2021-11-24 07:10:38'),
(161, 'browse_ace_popups', 'ace_popups', '2021-11-24 07:15:51', '2021-11-24 07:15:51'),
(162, 'read_ace_popups', 'ace_popups', '2021-11-24 07:15:51', '2021-11-24 07:15:51'),
(163, 'edit_ace_popups', 'ace_popups', '2021-11-24 07:15:51', '2021-11-24 07:15:51'),
(164, 'add_ace_popups', 'ace_popups', '2021-11-24 07:15:51', '2021-11-24 07:15:51'),
(165, 'delete_ace_popups', 'ace_popups', '2021-11-24 07:15:51', '2021-11-24 07:15:51'),
(166, 'browse_healthtools', 'healthtools', '2021-11-25 08:13:44', '2021-11-25 08:13:44'),
(167, 'read_healthtools', 'healthtools', '2021-11-25 08:13:44', '2021-11-25 08:13:44'),
(168, 'edit_healthtools', 'healthtools', '2021-11-25 08:13:44', '2021-11-25 08:13:44'),
(169, 'add_healthtools', 'healthtools', '2021-11-25 08:13:44', '2021-11-25 08:13:44'),
(170, 'delete_healthtools', 'healthtools', '2021-11-25 08:13:44', '2021-11-25 08:13:44'),
(171, 'browse_website_section', 'website_section', '2021-11-29 05:55:46', '2021-11-29 05:55:46'),
(172, 'read_website_section', 'website_section', '2021-11-29 05:55:46', '2021-11-29 05:55:46'),
(173, 'edit_website_section', 'website_section', '2021-11-29 05:55:46', '2021-11-29 05:55:46'),
(174, 'add_website_section', 'website_section', '2021-11-29 05:55:46', '2021-11-29 05:55:46'),
(175, 'delete_website_section', 'website_section', '2021-11-29 05:55:46', '2021-11-29 05:55:46'),
(176, 'browse_website_sections', 'website_sections', '2021-11-29 05:58:16', '2021-11-29 05:58:16'),
(177, 'read_website_sections', 'website_sections', '2021-11-29 05:58:16', '2021-11-29 05:58:16'),
(178, 'edit_website_sections', 'website_sections', '2021-11-29 05:58:16', '2021-11-29 05:58:16'),
(179, 'add_website_sections', 'website_sections', '2021-11-29 05:58:16', '2021-11-29 05:58:16'),
(180, 'delete_website_sections', 'website_sections', '2021-11-29 05:58:16', '2021-11-29 05:58:16'),
(181, 'browse_targetaudiences', 'targetaudiences', '2021-11-29 06:03:59', '2021-11-29 06:03:59'),
(182, 'read_targetaudiences', 'targetaudiences', '2021-11-29 06:03:59', '2021-11-29 06:03:59'),
(183, 'edit_targetaudiences', 'targetaudiences', '2021-11-29 06:03:59', '2021-11-29 06:03:59'),
(184, 'add_targetaudiences', 'targetaudiences', '2021-11-29 06:03:59', '2021-11-29 06:03:59'),
(185, 'delete_targetaudiences', 'targetaudiences', '2021-11-29 06:03:59', '2021-11-29 06:03:59'),
(186, 'browse_patienttypes', 'patienttypes', '2021-11-29 06:29:37', '2021-11-29 06:29:37'),
(187, 'read_patienttypes', 'patienttypes', '2021-11-29 06:29:37', '2021-11-29 06:29:37'),
(188, 'edit_patienttypes', 'patienttypes', '2021-11-29 06:29:37', '2021-11-29 06:29:37'),
(189, 'add_patienttypes', 'patienttypes', '2021-11-29 06:29:37', '2021-11-29 06:29:37'),
(190, 'delete_patienttypes', 'patienttypes', '2021-11-29 06:29:38', '2021-11-29 06:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(2, 'user', 'Normal User', '2021-11-08 08:41:08', '2021-11-08 08:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE `sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `sources` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sr_no` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `sources`, `status`, `sr_no`, `created_at`, `updated_at`) VALUES
(1, 'Through my Salix Account Manager', '1', 1, '2019-05-08 05:22:59', '2019-05-08 05:22:59'),
(2, 'Through a colleague', '1', 2, '2019-05-08 05:23:05', '2019-05-08 05:23:05'),
(3, 'Through a web search', '1', 3, '2019-05-08 05:23:27', '2019-05-08 05:23:27'),
(4, 'I received an email from liverhealthnow.com', '0', 0, '2019-05-08 05:23:00', '2020-10-13 14:42:36'),
(5, 'Other', '1', 5, '2019-05-08 05:23:41', '2019-05-08 05:23:41'),
(6, 'I received a code', '1', 4, '2020-10-13 10:35:00', '2020-10-13 16:27:49');

-- --------------------------------------------------------

--
-- Table structure for table `targetaudiences`
--

CREATE TABLE `targetaudiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `targetaudiences`
--

INSERT INTO `targetaudiences` (`id`, `title`, `sections`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Patient', '[\"1\",\"2\",\"3\",\"4\"]', '1', '2019-05-13 03:47:00', '2021-11-29 06:26:23'),
(2, 'Caregiver', '[\"1\",\"2\",\"3\",\"4\"]', '1', '2019-05-13 03:48:00', '2021-11-29 06:25:57'),
(3, 'Health Care Provider', '[\"1\",\"2\",\"3\",\"4\"]', '1', '2019-05-13 03:48:00', '2021-11-29 06:26:16');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at_time_zone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at_timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salix_manager_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agreement_check` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_department` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `created_at_time_zone`, `updated_at_timezone`, `lname`, `organization`, `organization_id`, `job_title`, `contact`, `address`, `salix_manager_name`, `source_id`, `update`, `agreement_check`, `timezone`, `last_login`, `last_login_timezone`, `code`, `status`, `department_id`, `has_department`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$HCYpvtdsbs.fsRPzCsdIleX.SVxX3IOsxoS885zjQ31gkAQrRvcaO', NULL, NULL, '2021-11-08 08:43:11', '2021-11-29 04:09:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-29 09:39:13', NULL, NULL, '1', NULL, NULL),
(2, 2, 'Sanat changed', 'gawade.sanat@gmail.com', 'users/240.jpg', NULL, '$2y$10$iLu56wwc/h5H6LXkqw4AYOz3SL7uYu4J0nhVI/psj89DRs5qKrm.S', 'mMlQ3T6MoTzAAgj9rydzH8e0vfKl1S6HOwNvpgbh9I4L3jf1lR6b8WGRbAEV', NULL, '2021-11-25 04:44:52', '2021-11-30 08:17:03', NULL, NULL, 'Gawade changed', 'Aventria', '2', 'Php Developer', '9930526914', 'A102, Haritara Mhatre wadi Dahisar (w)', 'Sanat Sanjay Gawade', '2', '1', '1', NULL, '2021-11-30 05:29:15', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_sections`
--

CREATE TABLE `website_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `section` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `website_sections`
--

INSERT INTO `website_sections` (`id`, `section`, `created_at`, `updated_at`) VALUES
(1, 'Ambulatory Care', '2021-11-29 05:59:00', '2021-11-29 06:01:26'),
(2, 'Primary Care', '2021-11-29 05:59:30', '2021-11-29 05:59:30'),
(3, 'Health System', '2021-11-29 05:59:41', '2021-11-29 05:59:41'),
(4, 'Long Term Care', '2021-11-29 05:59:49', '2021-11-29 05:59:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ace_care_pathways`
--
ALTER TABLE `ace_care_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_ehr_plugins_page`
--
ALTER TABLE `ace_ehr_plugins_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_guidelines`
--
ALTER TABLE `ace_guidelines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_guidelines_links`
--
ALTER TABLE `ace_guidelines_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_health_systems`
--
ALTER TABLE `ace_health_systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_health_system_ehr_plugins`
--
ALTER TABLE `ace_health_system_ehr_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_terms_ehr_plugins`
--
ALTER TABLE `ace_long_terms_ehr_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_term_care_d_pathways`
--
ALTER TABLE `ace_long_term_care_d_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_term_care_pathways`
--
ALTER TABLE `ace_long_term_care_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_term_landings`
--
ALTER TABLE `ace_long_term_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_pathways`
--
ALTER TABLE `ace_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_popups`
--
ALTER TABLE `ace_popups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_quality_measures`
--
ALTER TABLE `ace_quality_measures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `healthtools`
--
ALTER TABLE `healthtools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthtool_paitienttype`
--
ALTER TABLE `healthtool_paitienttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthtool_targetaudiences`
--
ALTER TABLE `healthtool_targetaudiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health_tools_favs`
--
ALTER TABLE `health_tools_favs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_ambulatorycare_banners`
--
ALTER TABLE `lhn_ambulatorycare_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_ambulatory_cares_landings`
--
ALTER TABLE `lhn_ambulatory_cares_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_ambulatory_coord_cares`
--
ALTER TABLE `lhn_ambulatory_coord_cares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_ambulatory_define_episodes`
--
ALTER TABLE `lhn_ambulatory_define_episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_amb_screen_patients`
--
ALTER TABLE `lhn_amb_screen_patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_annual_trend_reports`
--
ALTER TABLE `lhn_annual_trend_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primarycare_raisebars`
--
ALTER TABLE `lhn_primarycare_raisebars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primary_care_banners`
--
ALTER TABLE `lhn_primary_care_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primary_care_landings`
--
ALTER TABLE `lhn_primary_care_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primcare_wres_mons`
--
ALTER TABLE `lhn_primcare_wres_mons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_landings`
--
ALTER TABLE `main_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizationtypes`
--
ALTER TABLE `organizationtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patienttypes`
--
ALTER TABLE `patienttypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sources`
--
ALTER TABLE `sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `targetaudiences`
--
ALTER TABLE `targetaudiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `website_sections`
--
ALTER TABLE `website_sections`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ace_care_pathways`
--
ALTER TABLE `ace_care_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_ehr_plugins_page`
--
ALTER TABLE `ace_ehr_plugins_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_guidelines`
--
ALTER TABLE `ace_guidelines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ace_guidelines_links`
--
ALTER TABLE `ace_guidelines_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_health_systems`
--
ALTER TABLE `ace_health_systems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ace_health_system_ehr_plugins`
--
ALTER TABLE `ace_health_system_ehr_plugins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ace_long_terms_ehr_plugins`
--
ALTER TABLE `ace_long_terms_ehr_plugins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ace_long_term_care_d_pathways`
--
ALTER TABLE `ace_long_term_care_d_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_long_term_care_pathways`
--
ALTER TABLE `ace_long_term_care_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ace_long_term_landings`
--
ALTER TABLE `ace_long_term_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ace_pathways`
--
ALTER TABLE `ace_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_popups`
--
ALTER TABLE `ace_popups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ace_quality_measures`
--
ALTER TABLE `ace_quality_measures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=328;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `healthtools`
--
ALTER TABLE `healthtools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `healthtool_paitienttype`
--
ALTER TABLE `healthtool_paitienttype`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `healthtool_targetaudiences`
--
ALTER TABLE `healthtool_targetaudiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `health_tools_favs`
--
ALTER TABLE `health_tools_favs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lhn_ambulatorycare_banners`
--
ALTER TABLE `lhn_ambulatorycare_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lhn_ambulatory_cares_landings`
--
ALTER TABLE `lhn_ambulatory_cares_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lhn_ambulatory_coord_cares`
--
ALTER TABLE `lhn_ambulatory_coord_cares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lhn_ambulatory_define_episodes`
--
ALTER TABLE `lhn_ambulatory_define_episodes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lhn_amb_screen_patients`
--
ALTER TABLE `lhn_amb_screen_patients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lhn_annual_trend_reports`
--
ALTER TABLE `lhn_annual_trend_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lhn_primarycare_raisebars`
--
ALTER TABLE `lhn_primarycare_raisebars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lhn_primary_care_banners`
--
ALTER TABLE `lhn_primary_care_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `lhn_primary_care_landings`
--
ALTER TABLE `lhn_primary_care_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lhn_primcare_wres_mons`
--
ALTER TABLE `lhn_primcare_wres_mons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_landings`
--
ALTER TABLE `main_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `organizationtypes`
--
ALTER TABLE `organizationtypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `patienttypes`
--
ALTER TABLE `patienttypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sources`
--
ALTER TABLE `sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `targetaudiences`
--
ALTER TABLE `targetaudiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `website_sections`
--
ALTER TABLE `website_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
