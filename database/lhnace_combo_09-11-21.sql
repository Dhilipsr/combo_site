-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2021 at 02:44 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.3.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lhnace_combo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ace_care_pathways`
--

CREATE TABLE `ace_care_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_ehr_plugins_page`
--

CREATE TABLE `ace_ehr_plugins_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_three_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gina_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gina_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_guidelines`
--

CREATE TABLE `ace_guidelines` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_guidelines_links`
--

CREATE TABLE `ace_guidelines_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_health_systems`
--

CREATE TABLE `ace_health_systems` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_line` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_terms_ehr_plugins`
--

CREATE TABLE `ace_long_terms_ehr_plugins` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_image_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_one_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_image_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plugin_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gina_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_term_care_d_pathways`
--

CREATE TABLE `ace_long_term_care_d_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_term_care_pathways`
--

CREATE TABLE `ace_long_term_care_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_long_term_landings`
--

CREATE TABLE `ace_long_term_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_pathways`
--

CREATE TABLE `ace_pathways` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_three` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ace_quality_measures`
--

CREATE TABLE `ace_quality_measures` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'voyager::seeders.data_rows.roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(24, 4, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(25, 4, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 12),
(26, 4, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(27, 4, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(28, 4, 'link_one_title', 'text', 'Link One Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(29, 4, 'link_one', 'text', 'Link One', 0, 1, 1, 1, 1, 1, '{}', 5),
(30, 4, 'link_two_title', 'text', 'Link Two Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(31, 4, 'link_two', 'text', 'Link Two', 0, 1, 1, 1, 1, 1, '{}', 7),
(32, 4, 'link_three_title', 'text', 'Link Three Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(33, 4, 'link_three', 'text', 'Link Three', 0, 1, 1, 1, 1, 1, '{}', 9),
(34, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(35, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(36, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(37, 5, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(38, 5, 'link_title', 'text', 'Link Title', 0, 1, 1, 1, 1, 1, '{}', 3),
(39, 5, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(42, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(43, 7, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(44, 7, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(45, 7, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 12),
(46, 7, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(47, 7, 'banner_image', 'image', 'Banner Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(48, 7, 'banner_description', 'text', 'Banner Description', 0, 1, 1, 1, 1, 1, '{}', 4),
(49, 7, 'ad_line', 'text', 'Ad Line', 0, 1, 1, 1, 1, 1, '{}', 5),
(50, 7, 'about_title', 'text', 'About Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(51, 7, 'about_description', 'text_area', 'About Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(52, 7, 'image_two', 'image', 'Image Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(53, 7, 'description_two', 'text_area', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 9),
(54, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(55, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(56, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 8, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(58, 8, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(59, 8, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 12),
(60, 8, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(61, 8, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(62, 8, 'link_one_title', 'text', 'Link One Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(63, 8, 'link_one', 'text', 'Link One', 0, 1, 1, 1, 1, 1, '{}', 5),
(64, 8, 'link_two_title', 'text', 'Link Two Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(65, 8, 'link_two', 'text', 'Link Two', 0, 1, 1, 1, 1, 1, '{}', 7),
(66, 8, 'link_three_title', 'text', 'Link Three Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(67, 8, 'link_three', 'text', 'Link Three', 0, 1, 1, 1, 1, 1, '{}', 9),
(68, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(69, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(70, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(71, 9, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(72, 9, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 3),
(73, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(74, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(75, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(76, 10, 'page_title', 'text', 'Page Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(77, 10, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(78, 10, 'plugin_one_title', 'text', 'Plugin One Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(79, 10, 'plugin_image_one', 'image', 'Plugin Image One', 0, 1, 1, 1, 1, 1, '{}', 5),
(80, 10, 'plugin_one_link', 'text', 'Plugin One Link', 0, 1, 1, 1, 1, 1, '{}', 6),
(81, 10, 'plugin_two_title', 'text', 'Plugin Two Title', 0, 1, 1, 1, 1, 1, '{}', 7),
(82, 10, 'plugin_image_two', 'image', 'Plugin Image Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(83, 10, 'plugin_two_link', 'text', 'Plugin Two Link', 0, 1, 1, 1, 1, 1, '{}', 9),
(84, 10, 'title_two', 'text', 'Title Two', 0, 1, 1, 1, 1, 1, '{}', 10),
(85, 10, 'title_two_link', 'text', 'Title Two Link', 0, 1, 1, 1, 1, 1, '{}', 11),
(86, 10, 'gina_text', 'text', 'Gina Text', 0, 1, 1, 1, 1, 1, '{}', 12),
(87, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 16),
(88, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 17),
(89, 10, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 13),
(90, 10, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 14),
(91, 10, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 15),
(92, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(93, 12, 'banner_image', 'image', 'Banner Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(94, 12, 'banner_description', 'rich_text_box', 'Banner Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(95, 12, 'description_one', 'text_area', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 4),
(96, 12, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(97, 12, 'sub_title', 'text', 'Sub Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(98, 12, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 7),
(99, 12, 'content_one', 'rich_text_box', 'Content One', 0, 1, 1, 1, 1, 1, '{}', 8),
(100, 12, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 9),
(101, 12, 'ref_text', 'text', 'Ref Text', 0, 1, 1, 1, 1, 1, '{}', 10),
(102, 12, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 11),
(103, 12, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 12),
(104, 12, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 13),
(105, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(106, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(107, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(108, 13, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(109, 13, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(110, 13, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 4),
(111, 13, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(112, 13, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 6),
(113, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(114, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(115, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(116, 14, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(117, 14, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 3),
(118, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(119, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(120, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(121, 15, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(122, 15, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(123, 15, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(124, 15, 'content_two', 'text', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(125, 15, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(126, 15, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(127, 15, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 8),
(128, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(129, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(130, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(131, 16, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(132, 16, 'description', 'rich_text_box', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(133, 16, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(134, 16, 'title_two', 'text', 'Title Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(135, 16, 'description_two', 'text', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 6),
(136, 16, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 7),
(137, 16, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(138, 16, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(139, 16, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 10),
(140, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(141, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(142, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(143, 17, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(144, 17, 'reference', 'rich_text_box', 'Reference', 0, 1, 1, 1, 1, 1, '{}', 3),
(145, 17, 'title_two', 'text', 'Title Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(146, 17, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 5),
(147, 17, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 6),
(148, 17, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 7),
(149, 17, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(150, 17, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(151, 17, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 10),
(152, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 11),
(153, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(154, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(155, 18, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(156, 18, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(157, 18, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(158, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(159, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(160, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(161, 19, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(162, 19, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 3),
(163, 19, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(164, 19, 'description_three', 'rich_text_box', 'Description Three', 0, 1, 1, 1, 1, 1, '{}', 5),
(165, 19, 'tool_one_title', 'text', 'Tool One Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(166, 19, 'tool_one_description', 'rich_text_box', 'Tool One Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(167, 19, 'tool_two_title', 'text', 'Tool Two Title', 0, 1, 1, 1, 1, 1, '{}', 8),
(168, 19, 'tool_two_description', 'rich_text_box', 'Tool Two Description', 0, 1, 1, 1, 1, 1, '{}', 9),
(169, 19, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 10),
(170, 19, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 11),
(171, 19, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 12),
(172, 19, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 13),
(173, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 14),
(174, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(175, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(176, 20, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(177, 20, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(178, 20, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(179, 20, 'content_three', 'rich_text_box', 'Content Three', 0, 1, 1, 1, 1, 1, '{}', 5),
(180, 20, 'provider_resource_content', 'rich_text_box', 'Provider Resource Content', 0, 1, 1, 1, 1, 1, '{}', 6),
(181, 20, 'patient_resource_content', 'rich_text_box', 'Patient Resource Content', 0, 1, 1, 1, 1, 1, '{}', 7),
(182, 20, 'ref', 'text', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 8),
(183, 20, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 9),
(184, 20, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 10),
(185, 20, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 11),
(186, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 12),
(187, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(188, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(189, 21, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(190, 21, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 3),
(191, 21, 'content_one', 'rich_text_box', 'Content One', 0, 1, 1, 1, 1, 1, '{}', 4),
(192, 21, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(193, 21, 'tool_one_description', 'rich_text_box', 'Tool One Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(194, 21, 'tool_two_description', 'rich_text_box', 'Tool Two Description', 0, 1, 1, 1, 1, 1, '{}', 7),
(195, 21, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 8),
(196, 21, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 9),
(197, 21, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 10),
(198, 21, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 11),
(199, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 12),
(200, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(201, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(202, 22, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(203, 22, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 3),
(204, 22, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 4),
(205, 22, 'description_three', 'rich_text_box', 'Description Three', 0, 1, 1, 1, 1, 1, '{}', 5),
(206, 22, 'description_four', 'rich_text_box', 'Description Four', 0, 1, 1, 1, 1, 1, '{}', 6),
(207, 22, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 7),
(208, 22, 'description_five', 'rich_text_box', 'Description Five', 0, 1, 1, 1, 1, 1, '{}', 8),
(209, 22, 'ref', 'rich_text_box', 'Ref', 0, 1, 1, 1, 1, 1, '{}', 9),
(210, 22, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 10),
(211, 22, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 11),
(212, 22, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 12),
(213, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(214, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(215, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(216, 23, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 2),
(217, 23, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(218, 23, 'link', 'text', 'Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(219, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(220, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(221, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(222, 24, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(223, 24, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(224, 24, 'video_link', 'text', 'Video Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(225, 24, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(226, 24, 'meta_description', 'text', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(227, 24, 'keywords', 'text', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 7),
(228, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(229, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(230, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(231, 25, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(232, 25, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(233, 25, 'video_link', 'text', 'Video Link', 0, 1, 1, 1, 1, 1, '{}', 4),
(234, 25, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 5),
(235, 25, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 6),
(236, 25, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 7),
(237, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(238, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(239, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(240, 26, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{}', 2),
(241, 26, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 3),
(242, 26, 'banner_image', 'image', 'Banner Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(243, 26, 'content_two', 'rich_text_box', 'Content Two', 0, 1, 1, 1, 1, 1, '{}', 5),
(244, 26, 'lhn_btn_one', 'text', 'Lhn Btn One', 0, 1, 1, 1, 1, 1, '{}', 6),
(245, 26, 'lhn_content_one', 'text_area', 'Lhn Content One', 0, 1, 1, 1, 1, 1, '{}', 7),
(246, 26, 'lhn_btn_two', 'text', 'Lhn Btn Two', 0, 1, 1, 1, 1, 1, '{}', 8),
(247, 26, 'lhn_content_two', 'text_area', 'Lhn Content Two', 0, 1, 1, 1, 1, 1, '{}', 9),
(248, 26, 'ace_link_one', 'text', 'Ace Link One', 0, 1, 1, 1, 1, 1, '{}', 10),
(249, 26, 'ace_content_one', 'text_area', 'Ace Content One', 0, 1, 1, 1, 1, 1, '{}', 11),
(250, 26, 'ace_link_two', 'text', 'Ace Link Two', 0, 1, 1, 1, 1, 1, '{}', 12),
(251, 26, 'ace_content_two', 'text_area', 'Ace Content Two', 0, 1, 1, 1, 1, 1, '{}', 13),
(252, 26, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 14),
(253, 26, 'description_one', 'rich_text_box', 'Description One', 0, 1, 1, 1, 1, 1, '{}', 15),
(254, 26, 'description_two', 'rich_text_box', 'Description Two', 0, 1, 1, 1, 1, 1, '{}', 16),
(255, 26, 'note', 'text', 'Note', 0, 1, 1, 1, 1, 1, '{}', 17),
(256, 26, 'meta_title', 'text', 'Meta Title', 0, 1, 1, 1, 1, 1, '{}', 18),
(257, 26, 'meta_description', 'text_area', 'Meta Description', 0, 1, 1, 1, 1, 1, '{}', 19),
(258, 26, 'keywords', 'text_area', 'Keywords', 0, 1, 1, 1, 1, 1, '{}', 20),
(259, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 21),
(260, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 22);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-11-08 08:41:06', '2021-11-08 08:41:06'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-11-08 08:41:06', '2021-11-08 08:41:06'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-11-08 08:41:06', '2021-11-08 08:41:06'),
(4, 'ace_pathways', 'ace-pathways', 'Ace Pathway', 'Ace Pathways', 'voyager-dot', 'App\\AcePathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(5, 'ace_care_pathways', 'ace-care-pathways', 'Ace Care Pathway', 'Ace Care Pathways', 'voyager-dot', 'App\\AceCarePathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(6, 'ace_ehr_plugins_page', 'ace-ehr-plugins-page', 'Ace Ehr Plugins Page', 'Ace Ehr Plugins Pages', 'voyager-dot', 'App\\AceEhrPluginsPage', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(7, 'ace_health_systems', 'ace-health-systems', 'Ace Health System', 'Ace Health Systems', 'voyager-dot', 'App\\AceHealthSystem', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 02:08:05', '2021-11-09 02:08:05'),
(8, 'ace_long_term_care_pathways', 'ace-long-term-care-pathways', 'Ace Long Term Care Pathway', 'Ace Long Term Care Pathways', 'voyager-dot', 'App\\AceLongTermCarePathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(9, 'ace_long_term_care_d_pathways', 'ace-long-term-care-d-pathways', 'Ace Long Term Care D Pathway', 'Ace Long Term Care D Pathways', 'voyager-dot', 'App\\AceLongTermCareDPathway', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 04:33:20', '2021-11-09 04:33:20'),
(10, 'ace_long_terms_ehr_plugins', 'ace-long-terms-ehr-plugins', 'Ace Long Terms Ehr Plugin', 'Ace Long Terms Ehr Plugins', 'voyager-dot', 'App\\AceLongTermsEhrPlugin', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 04:44:07', '2021-11-09 04:45:11'),
(11, 'ace_long_term_landing', 'ace-long-term-landing', 'Ace Long Term Landing', 'Ace Long Term Landings', 'voyager-dot', 'App\\AceLongTermLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-09 04:53:49', '2021-11-09 04:59:12'),
(12, 'ace_long_term_landings', 'ace-long-term-landings', 'Ace Long Term Landing', 'Ace Long Term Landings', 'voyager-dot', 'App\\AceLongTermLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:02:23', '2021-11-09 05:02:23'),
(13, 'ace_guidelines', 'ace-guidelines', 'Ace Guideline', 'Ace Guidelines', 'voyager-dot', 'App\\AceGuideline', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(14, 'ace_guidelines_links', 'ace-guidelines-links', 'Ace Guidelines Link', 'Ace Guidelines Links', 'voyager-dot', 'App\\AceGuidelinesLink', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(15, 'ace_quality_measures', 'ace-quality-measures', 'Ace Quality Measure', 'Ace Quality Measures', 'voyager-dot', 'App\\AceQualityMeasure', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(16, 'lhn_annual_trend_reports', 'lhn-annual-trend-reports', 'Lhn Annual Trend Report', 'Annual Trend Reports', 'voyager-dot', 'App\\LhnAnnualTrendReport', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:48:03', '2021-11-09 05:48:03'),
(17, 'lhn_ambulatory_cares_landings', 'lhn-ambulatory-cares-landings', 'Lhn Ambulatory Cares Landing', 'Lhn Ambulatory Cares Landings', 'voyager-dot', 'App\\LhnAmbulatoryCaresLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(18, 'lhn_ambulatorycare_banners', 'lhn-ambulatorycare-banners', 'Lhn Ambulatorycare Banner', 'Lhn Ambulatorycare Banners', 'voyager-dot', 'App\\LhnAmbulatorycareBanner', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(19, 'lhn_ambulatory_coord_cares', 'lhn-ambulatory-coord-cares', 'Lhn Ambulatory Coord Care', 'Ambulatory Coord Cares', 'voyager-dot', 'App\\LhnAmbulatoryCoordCare', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(20, 'lhn_ambulatory_define_episodes', 'lhn-ambulatory-define-episodes', 'Lhn Ambulatory Define Episode', 'Ambulatory Define Episodes', 'voyager-dot', 'App\\LhnAmbulatoryDefineEpisode', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(21, 'lhn_amb_screen_patients', 'lhn-amb-screen-patients', 'Lhn Amb Screen Patient', 'Lhn Amb Screen Patients', 'voyager-dot', 'App\\LhnAmbScreenPatient', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(22, 'lhn_primary_care_landings', 'lhn-primary-care-landings', 'Primary Care Landing', 'Primary Care Landings', 'voyager-dot', 'App\\LhnPrimaryCareLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:41:36', '2021-11-09 06:41:36'),
(23, 'lhn_primary_care_banners', 'lhn-primary-care-banners', 'Primary Care Banner', 'Primary Care Banners', 'voyager-dot', 'App\\LhnPrimaryCareBanner', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(24, 'lhn_primarycare_raisebars', 'lhn-primarycare-raisebars', 'Primary Care Raisebar', 'Primary Care Raisebars', 'voyager-dot', 'App\\LhnPrimarycareRaisebar', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(25, 'lhn_primcare_wres_mons', 'lhn-primcare-wres-mons', 'Primary Care Wrestling The Monster', 'Primary Care Wrestling The Monsters', 'voyager-dot', 'App\\LhnPrimcareWresMon', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(26, 'main_landings', 'main-landings', 'Main Landing', 'Main Landings', 'voyager-world', 'App\\MainLanding', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-11-09 07:51:41', '2021-11-09 07:51:41');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatorycare_banners`
--

CREATE TABLE `lhn_ambulatorycare_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatory_cares_landings`
--

CREATE TABLE `lhn_ambulatory_cares_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatory_coord_cares`
--

CREATE TABLE `lhn_ambulatory_coord_cares` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_three` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_one_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_one_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_two_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_two_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_ambulatory_define_episodes`
--

CREATE TABLE `lhn_ambulatory_define_episodes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_three` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_resource_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_resource_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_amb_screen_patients`
--

CREATE TABLE `lhn_amb_screen_patients` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_one_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tool_two_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_annual_trend_reports`
--

CREATE TABLE `lhn_annual_trend_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primarycare_raisebars`
--

CREATE TABLE `lhn_primarycare_raisebars` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primary_care_banners`
--

CREATE TABLE `lhn_primary_care_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primary_care_landings`
--

CREATE TABLE `lhn_primary_care_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_three` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_four` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_five` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lhn_primcare_wres_mons`
--

CREATE TABLE `lhn_primcare_wres_mons` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_landings`
--

CREATE TABLE `main_landings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_image` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_btn_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_btn_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lhn_content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_link_one` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_content_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_link_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ace_content_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_one` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_two` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-11-08 08:41:07', '2021-11-08 08:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, 5, 3, '2021-11-08 08:41:07', '2021-11-09 05:29:55', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 5, '2021-11-08 08:41:08', '2021-11-09 05:29:55', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, 5, 9, '2021-11-08 08:41:08', '2021-11-09 05:29:36', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, 5, 7, '2021-11-08 08:41:08', '2021-11-09 05:29:36', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 4, '2021-11-08 08:41:08', '2021-11-09 07:53:35', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 6, '2021-11-08 08:41:08', '2021-11-09 05:29:55', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 4, '2021-11-08 08:41:08', '2021-11-09 05:29:55', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 2, '2021-11-08 08:41:08', '2021-11-09 05:29:42', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 1, '2021-11-08 08:41:08', '2021-11-09 05:29:38', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, 5, 8, '2021-11-08 08:41:08', '2021-11-09 05:29:36', 'voyager.settings.index', NULL),
(11, 1, 'Pathways Page', '', '_self', 'voyager-dot', '#000000', 13, 1, '2021-11-09 00:39:22', '2021-11-09 06:43:56', 'voyager.ace-pathways.index', 'null'),
(12, 1, 'Care Pathways', '', '_self', 'voyager-dot', '#000000', 13, 3, '2021-11-09 00:41:50', '2021-11-09 06:44:15', 'voyager.ace-care-pathways.index', 'null'),
(13, 1, 'Ace', '', '_self', 'voyager-window-list', '#000000', NULL, 2, '2021-11-09 00:42:29', '2021-11-09 07:53:34', NULL, ''),
(14, 1, 'Ehr Plugins Pages', '', '_self', 'voyager-dot', '#000000', 13, 4, '2021-11-09 00:57:26', '2021-11-09 06:44:22', 'voyager.ace-ehr-plugins-page.index', 'null'),
(15, 1, 'Health Systems', '', '_self', 'voyager-dot', '#000000', 13, 2, '2021-11-09 02:08:06', '2021-11-09 06:44:08', 'voyager.ace-health-systems.index', 'null'),
(16, 1, 'Long Term Care Pathways Page', '', '_self', 'voyager-dot', '#000000', 13, 6, '2021-11-09 04:30:51', '2021-11-09 07:53:34', 'voyager.ace-long-term-care-pathways.index', 'null'),
(17, 1, 'Long Term Care Pathways', '', '_self', 'voyager-dot', '#000000', 13, 7, '2021-11-09 04:33:21', '2021-11-09 07:53:34', 'voyager.ace-long-term-care-d-pathways.index', 'null'),
(18, 1, 'Ace Long Terms Ehr Plugins', '', '_self', 'voyager-dot', NULL, 13, 8, '2021-11-09 04:44:07', '2021-11-09 07:53:34', 'voyager.ace-long-terms-ehr-plugins.index', NULL),
(20, 1, 'Long Term Landings', '', '_self', 'voyager-dot', '#000000', 13, 5, '2021-11-09 05:02:24', '2021-11-09 07:53:34', 'voyager.ace-long-term-landings.index', 'null'),
(21, 1, 'Ace Guidelines', '', '_self', 'voyager-dot', NULL, 13, 9, '2021-11-09 05:13:21', '2021-11-09 07:53:34', 'voyager.ace-guidelines.index', NULL),
(22, 1, 'Ace Guidelines Links', '', '_self', 'voyager-dot', NULL, 13, 10, '2021-11-09 05:14:17', '2021-11-09 07:53:34', 'voyager.ace-guidelines-links.index', NULL),
(23, 1, 'Ace Quality Measures', '', '_self', 'voyager-dot', NULL, 13, 11, '2021-11-09 05:19:07', '2021-11-09 07:53:34', 'voyager.ace-quality-measures.index', NULL),
(24, 1, 'LHN', '', '_self', 'voyager-window-list', '#000000', NULL, 3, '2021-11-09 05:29:10', '2021-11-09 07:53:34', NULL, ''),
(25, 1, 'Annual Trend Reports', '', '_self', 'voyager-dot', NULL, 24, 3, '2021-11-09 05:48:04', '2021-11-09 06:04:11', 'voyager.lhn-annual-trend-reports.index', NULL),
(26, 1, 'Ambulatory Cares Landings', '', '_self', 'voyager-dot', '#000000', 24, 1, '2021-11-09 05:56:41', '2021-11-09 06:04:09', 'voyager.lhn-ambulatory-cares-landings.index', 'null'),
(27, 1, 'Ambulatory Care Banners', '', '_self', 'voyager-dot', '#000000', 24, 2, '2021-11-09 06:03:34', '2021-11-09 06:04:26', 'voyager.lhn-ambulatorycare-banners.index', 'null'),
(28, 1, 'Ambulatory Coord Cares', '', '_self', 'voyager-dot', NULL, 24, 4, '2021-11-09 06:10:48', '2021-11-09 06:11:00', 'voyager.lhn-ambulatory-coord-cares.index', NULL),
(29, 1, 'Ambulatory Define Episodes', '', '_self', 'voyager-dot', NULL, 24, 5, '2021-11-09 06:19:23', '2021-11-09 06:19:32', 'voyager.lhn-ambulatory-define-episodes.index', NULL),
(30, 1, 'Ambulatory Screen Patients', '', '_self', 'voyager-dot', '#000000', 24, 6, '2021-11-09 06:32:20', '2021-11-09 06:32:52', 'voyager.lhn-amb-screen-patients.index', 'null'),
(31, 1, 'Primary Care Landings', '', '_self', 'voyager-dot', NULL, 24, 7, '2021-11-09 06:41:37', '2021-11-09 06:41:44', 'voyager.lhn-primary-care-landings.index', NULL),
(32, 1, 'Primary Care Banners', '', '_self', 'voyager-dot', NULL, 24, 8, '2021-11-09 06:43:24', '2021-11-09 06:43:31', 'voyager.lhn-primary-care-banners.index', NULL),
(33, 1, 'Primary Care Raisebars', '', '_self', 'voyager-dot', NULL, 24, 9, '2021-11-09 07:18:00', '2021-11-09 07:18:45', 'voyager.lhn-primarycare-raisebars.index', NULL),
(34, 1, 'Primary Care Wrestling The Monsters', '', '_self', 'voyager-dot', NULL, 24, 10, '2021-11-09 07:25:37', '2021-11-09 07:25:45', 'voyager.lhn-primcare-wres-mons.index', NULL),
(35, 1, 'Main Landings', '', '_self', 'voyager-world', NULL, NULL, 1, '2021-11-09 07:51:41', '2021-11-09 07:53:34', 'voyager.main-landings.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(2, 'browse_bread', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(3, 'browse_database', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(4, 'browse_media', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(5, 'browse_compass', NULL, '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(6, 'browse_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(7, 'read_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(8, 'edit_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(9, 'add_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(10, 'delete_menus', 'menus', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(11, 'browse_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(12, 'read_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(13, 'edit_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(14, 'add_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(15, 'delete_roles', 'roles', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(16, 'browse_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(17, 'read_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(18, 'edit_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(19, 'add_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(20, 'delete_users', 'users', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(21, 'browse_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(22, 'read_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(23, 'edit_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(24, 'add_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(25, 'delete_settings', 'settings', '2021-11-08 08:41:09', '2021-11-08 08:41:09'),
(26, 'browse_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(27, 'read_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(28, 'edit_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(29, 'add_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(30, 'delete_ace_pathways', 'ace_pathways', '2021-11-09 00:39:22', '2021-11-09 00:39:22'),
(31, 'browse_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(32, 'read_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(33, 'edit_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(34, 'add_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(35, 'delete_ace_care_pathways', 'ace_care_pathways', '2021-11-09 00:41:50', '2021-11-09 00:41:50'),
(36, 'browse_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(37, 'read_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(38, 'edit_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(39, 'add_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(40, 'delete_ace_ehr_plugins_page', 'ace_ehr_plugins_page', '2021-11-09 00:57:26', '2021-11-09 00:57:26'),
(41, 'browse_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(42, 'read_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(43, 'edit_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(44, 'add_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(45, 'delete_ace_health_systems', 'ace_health_systems', '2021-11-09 02:08:06', '2021-11-09 02:08:06'),
(46, 'browse_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(47, 'read_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(48, 'edit_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(49, 'add_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(50, 'delete_ace_long_term_care_pathways', 'ace_long_term_care_pathways', '2021-11-09 04:30:51', '2021-11-09 04:30:51'),
(51, 'browse_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(52, 'read_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(53, 'edit_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(54, 'add_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(55, 'delete_ace_long_term_care_d_pathways', 'ace_long_term_care_d_pathways', '2021-11-09 04:33:21', '2021-11-09 04:33:21'),
(56, 'browse_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(57, 'read_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(58, 'edit_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(59, 'add_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(60, 'delete_ace_long_terms_ehr_plugins', 'ace_long_terms_ehr_plugins', '2021-11-09 04:44:07', '2021-11-09 04:44:07'),
(61, 'browse_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(62, 'read_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(63, 'edit_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(64, 'add_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(65, 'delete_ace_long_term_landing', 'ace_long_term_landing', '2021-11-09 04:53:49', '2021-11-09 04:53:49'),
(66, 'browse_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:23', '2021-11-09 05:02:23'),
(67, 'read_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:23', '2021-11-09 05:02:23'),
(68, 'edit_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:23', '2021-11-09 05:02:23'),
(69, 'add_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:24', '2021-11-09 05:02:24'),
(70, 'delete_ace_long_term_landings', 'ace_long_term_landings', '2021-11-09 05:02:24', '2021-11-09 05:02:24'),
(71, 'browse_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(72, 'read_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(73, 'edit_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(74, 'add_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(75, 'delete_ace_guidelines', 'ace_guidelines', '2021-11-09 05:13:21', '2021-11-09 05:13:21'),
(76, 'browse_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(77, 'read_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(78, 'edit_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(79, 'add_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(80, 'delete_ace_guidelines_links', 'ace_guidelines_links', '2021-11-09 05:14:17', '2021-11-09 05:14:17'),
(81, 'browse_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(82, 'read_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(83, 'edit_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(84, 'add_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(85, 'delete_ace_quality_measures', 'ace_quality_measures', '2021-11-09 05:19:07', '2021-11-09 05:19:07'),
(86, 'browse_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(87, 'read_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(88, 'edit_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(89, 'add_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(90, 'delete_lhn_annual_trend_reports', 'lhn_annual_trend_reports', '2021-11-09 05:48:04', '2021-11-09 05:48:04'),
(91, 'browse_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(92, 'read_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(93, 'edit_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(94, 'add_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(95, 'delete_lhn_ambulatory_cares_landings', 'lhn_ambulatory_cares_landings', '2021-11-09 05:56:41', '2021-11-09 05:56:41'),
(96, 'browse_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(97, 'read_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(98, 'edit_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(99, 'add_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(100, 'delete_lhn_ambulatorycare_banners', 'lhn_ambulatorycare_banners', '2021-11-09 06:03:34', '2021-11-09 06:03:34'),
(101, 'browse_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(102, 'read_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(103, 'edit_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(104, 'add_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(105, 'delete_lhn_ambulatory_coord_cares', 'lhn_ambulatory_coord_cares', '2021-11-09 06:10:48', '2021-11-09 06:10:48'),
(106, 'browse_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(107, 'read_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(108, 'edit_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(109, 'add_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(110, 'delete_lhn_ambulatory_define_episodes', 'lhn_ambulatory_define_episodes', '2021-11-09 06:19:23', '2021-11-09 06:19:23'),
(111, 'browse_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(112, 'read_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(113, 'edit_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(114, 'add_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(115, 'delete_lhn_amb_screen_patients', 'lhn_amb_screen_patients', '2021-11-09 06:32:20', '2021-11-09 06:32:20'),
(116, 'browse_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(117, 'read_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(118, 'edit_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(119, 'add_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(120, 'delete_lhn_primary_care_landings', 'lhn_primary_care_landings', '2021-11-09 06:41:37', '2021-11-09 06:41:37'),
(121, 'browse_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(122, 'read_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(123, 'edit_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(124, 'add_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(125, 'delete_lhn_primary_care_banners', 'lhn_primary_care_banners', '2021-11-09 06:43:24', '2021-11-09 06:43:24'),
(126, 'browse_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(127, 'read_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(128, 'edit_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(129, 'add_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(130, 'delete_lhn_primarycare_raisebars', 'lhn_primarycare_raisebars', '2021-11-09 07:18:00', '2021-11-09 07:18:00'),
(131, 'browse_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(132, 'read_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(133, 'edit_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(134, 'add_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(135, 'delete_lhn_primcare_wres_mons', 'lhn_primcare_wres_mons', '2021-11-09 07:25:37', '2021-11-09 07:25:37'),
(136, 'browse_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(137, 'read_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(138, 'edit_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(139, 'add_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41'),
(140, 'delete_main_landings', 'main_landings', '2021-11-09 07:51:41', '2021-11-09 07:51:41');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-11-08 08:41:08', '2021-11-08 08:41:08'),
(2, 'user', 'Normal User', '2021-11-08 08:41:08', '2021-11-08 08:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$HCYpvtdsbs.fsRPzCsdIleX.SVxX3IOsxoS885zjQ31gkAQrRvcaO', NULL, NULL, '2021-11-08 08:43:11', '2021-11-08 08:43:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ace_care_pathways`
--
ALTER TABLE `ace_care_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_ehr_plugins_page`
--
ALTER TABLE `ace_ehr_plugins_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_guidelines`
--
ALTER TABLE `ace_guidelines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_guidelines_links`
--
ALTER TABLE `ace_guidelines_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_health_systems`
--
ALTER TABLE `ace_health_systems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_terms_ehr_plugins`
--
ALTER TABLE `ace_long_terms_ehr_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_term_care_d_pathways`
--
ALTER TABLE `ace_long_term_care_d_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_term_care_pathways`
--
ALTER TABLE `ace_long_term_care_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_long_term_landings`
--
ALTER TABLE `ace_long_term_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_pathways`
--
ALTER TABLE `ace_pathways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ace_quality_measures`
--
ALTER TABLE `ace_quality_measures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `lhn_ambulatorycare_banners`
--
ALTER TABLE `lhn_ambulatorycare_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_ambulatory_cares_landings`
--
ALTER TABLE `lhn_ambulatory_cares_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_ambulatory_coord_cares`
--
ALTER TABLE `lhn_ambulatory_coord_cares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_ambulatory_define_episodes`
--
ALTER TABLE `lhn_ambulatory_define_episodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_amb_screen_patients`
--
ALTER TABLE `lhn_amb_screen_patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_annual_trend_reports`
--
ALTER TABLE `lhn_annual_trend_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primarycare_raisebars`
--
ALTER TABLE `lhn_primarycare_raisebars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primary_care_banners`
--
ALTER TABLE `lhn_primary_care_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primary_care_landings`
--
ALTER TABLE `lhn_primary_care_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lhn_primcare_wres_mons`
--
ALTER TABLE `lhn_primcare_wres_mons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_landings`
--
ALTER TABLE `main_landings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ace_care_pathways`
--
ALTER TABLE `ace_care_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_ehr_plugins_page`
--
ALTER TABLE `ace_ehr_plugins_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_guidelines`
--
ALTER TABLE `ace_guidelines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_guidelines_links`
--
ALTER TABLE `ace_guidelines_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_health_systems`
--
ALTER TABLE `ace_health_systems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_long_terms_ehr_plugins`
--
ALTER TABLE `ace_long_terms_ehr_plugins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_long_term_care_d_pathways`
--
ALTER TABLE `ace_long_term_care_d_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_long_term_care_pathways`
--
ALTER TABLE `ace_long_term_care_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_long_term_landings`
--
ALTER TABLE `ace_long_term_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_pathways`
--
ALTER TABLE `ace_pathways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ace_quality_measures`
--
ALTER TABLE `ace_quality_measures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_ambulatorycare_banners`
--
ALTER TABLE `lhn_ambulatorycare_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_ambulatory_cares_landings`
--
ALTER TABLE `lhn_ambulatory_cares_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_ambulatory_coord_cares`
--
ALTER TABLE `lhn_ambulatory_coord_cares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_ambulatory_define_episodes`
--
ALTER TABLE `lhn_ambulatory_define_episodes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_amb_screen_patients`
--
ALTER TABLE `lhn_amb_screen_patients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_annual_trend_reports`
--
ALTER TABLE `lhn_annual_trend_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_primarycare_raisebars`
--
ALTER TABLE `lhn_primarycare_raisebars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_primary_care_banners`
--
ALTER TABLE `lhn_primary_care_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_primary_care_landings`
--
ALTER TABLE `lhn_primary_care_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lhn_primcare_wres_mons`
--
ALTER TABLE `lhn_primcare_wres_mons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_landings`
--
ALTER TABLE `main_landings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
